using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour
{
    /**
    * Code is based on this youtube tutorial https://www.youtube.com/watch?v=AKKpPmxx07w. 
    */

    public Transform startPosition;
    public LayerMask wallMask;
    public Vector2 gridWorldSize;
    public float nodeRadius;
    public float distanceBetweenNodes;

    Node[,] nodeArray;
    public List<Node> finalPath;


    float nodeDiameter;
    int gridSizeX, gridSizeY;


    private void Start()
    {
        nodeDiameter = nodeRadius * 2;
        gridSizeX = Mathf.RoundToInt(gridWorldSize.x / nodeDiameter);
        gridSizeY = Mathf.RoundToInt(gridWorldSize.y / nodeDiameter);
        CreateGrid();
    }

    /**
    * @Brief Creation of the grid field. 
    */
    void CreateGrid()
    {
        nodeArray = new Node[gridSizeX, gridSizeY];
        Vector3 bottomLeft = transform.position - Vector3.right * gridWorldSize.x / 2 - Vector3.forward * gridWorldSize.y / 2;
        for (int x = 0; x < gridSizeX; x++)
        {
            for (int y = 0; y < gridSizeY; y++)
            {
                Vector3 worldPoint = bottomLeft + Vector3.right * (x * nodeDiameter + nodeRadius) + Vector3.forward * (y * nodeDiameter + nodeRadius);
                bool wall = true;

                if (Physics.CheckSphere(worldPoint, nodeRadius, wallMask))
                {
                    wall = false;
                }

                nodeArray[x, y] = new Node(wall, worldPoint, x, y);
            }
        }
    }

    /**
    * @Brief Check to see what the neighbouring nodes of the current node are. 
    * @Param Current node on the path.
    */
    public List<Node> GetNeighboringNodes(Node neighborNode)
    {
        List<Node> neighborList = new List<Node>();
        int icheckX;
        int icheckY;

        icheckX = neighborNode.gridX + 1;
        icheckY = neighborNode.gridY;
        if (icheckX >= 0 && icheckX < gridSizeX)
        {
            if (icheckY >= 0 && icheckY < gridSizeY)
            {
                neighborList.Add(nodeArray[icheckX, icheckY]);
            }
        }

        icheckX = neighborNode.gridX - 1;
        icheckY = neighborNode.gridY;
        if (icheckX >= 0 && icheckX < gridSizeX)
        {
            if (icheckY >= 0 && icheckY < gridSizeY)
            {
                neighborList.Add(nodeArray[icheckX, icheckY]);
            }
        }

        icheckX = neighborNode.gridX;
        icheckY = neighborNode.gridY + 1;
        if (icheckX >= 0 && icheckX < gridSizeX)
        {
            if (icheckY >= 0 && icheckY < gridSizeY)
            {
                neighborList.Add(nodeArray[icheckX, icheckY]);
            }
        }

        icheckX = neighborNode.gridX;
        icheckY = neighborNode.gridY - 1;
        if (icheckX >= 0 && icheckX < gridSizeX)
        {
            if (icheckY >= 0 && icheckY < gridSizeY)
            {
                neighborList.Add(nodeArray[icheckX, icheckY]);
            }
        }
        return neighborList;
    }

    /**
    * @Brief Get the node from the world view, so checking where in the grid is the current node.
    * @Param Current position in the world.
    */
    public Node NodeFromWorldPoint(Vector3 worldPos)
    {
        float ixPos = ((worldPos.x + gridWorldSize.x / 2) / gridWorldSize.x);
        float iyPos = ((worldPos.z + gridWorldSize.y / 2) / gridWorldSize.y);

        ixPos = Mathf.Clamp01(ixPos);
        iyPos = Mathf.Clamp01(iyPos);

        int ix = Mathf.RoundToInt((gridSizeX - 1) * ixPos);
        int iy = Mathf.RoundToInt((gridSizeX - 1) * iyPos);

        return nodeArray[ix, iy];
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position, new Vector3(gridWorldSize.x, 1, gridWorldSize.y));

        if (nodeArray != null)
        {
            foreach (Node n in nodeArray)
            {
                if (n.isWall)
                {
                    Gizmos.color = Color.white;
                }
                else
                {
                    Gizmos.color = Color.yellow;
                }

                if (finalPath != null)
                {
                    if (finalPath.Contains(n))
                    {
                        Gizmos.color = Color.red;
                    }

                }
                Gizmos.DrawCube(n.position, Vector3.one * (nodeDiameter - distanceBetweenNodes));
            }
        }
    }

}
