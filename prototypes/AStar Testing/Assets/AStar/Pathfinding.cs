using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinding : MonoBehaviour
{
    /**
    * Code is based on this youtube tutorial https://www.youtube.com/watch?v=AKKpPmxx07w. 
    */

    Grid gridReference;
    public Transform startPosition;
    public Transform targetPosition;

    private void Awake()
    {
        gridReference = GetComponent<Grid>();
    }

    private void Update()
    {
        FindPath(startPosition.position, targetPosition.position);
    }

    /**
    * @Brief Function to find the path from start point to target point.
    * @Param Start position.
    * @Param Target position.
    */
    void FindPath(Vector3 vStartPos, Vector3 vTargetPos)
    {
        // Setting the starting position and target position.
        Node startNode = gridReference.NodeFromWorldPoint(vStartPos);
        Node targetNode = gridReference.NodeFromWorldPoint(vTargetPos);

        // Creating a list for the open nodes and a Hashset for the closed nodes.
        List<Node> openList = new List<Node>();
        HashSet<Node> closedList = new HashSet<Node>();

        // Add the start position to the node list.
        openList.Add(startNode);

        // While there are still nodes in the open list continue .
        while (openList.Count > 0)
        {
            // Receiving the current node from the list.
            Node currentNode = openList[0];
            // Looping through the List starting from the second object for comparison with currentNode.
            for (int i = 1; i < openList.Count; i++)
            {
                // Check to see if the F cost of the node is lower or the same than current node.
                if (openList[i].CalculateFCost() <= currentNode.CalculateFCost() && openList[i].hCost < currentNode.hCost)
                {
                    // Make the current node the new node from the list.
                    currentNode = openList[i];
                }
            }
            // Remove the Node from open list and add it to the closed list.
            openList.Remove(currentNode);
            closedList.Add(currentNode);

            // If the current Node is the same as the target Node then the final path is clear.
            if (currentNode == targetNode)
            {
                GetFinalPath(startNode, targetNode);
            }

            // Looping through all the neighbours of the current Node.
            foreach (Node neighbourNode in gridReference.GetNeighboringNodes(currentNode))
            {
                // Check if the Neighbour is a wall or is already in the closed list.
                if (!neighbourNode.isWall || closedList.Contains(neighbourNode))
                {
                    continue;
                }

                // Calculate the F cost of the neighbour.
                int moveCost = currentNode.gCost + GetManhattenDistance(currentNode, neighbourNode);

                // Check if the F cost is greater than the G cost or it is not in the open list.
                if (moveCost < neighbourNode.gCost || !openList.Contains(neighbourNode))
                {
                    // Set the G cost to the F cost.
                    neighbourNode.gCost = moveCost;
                    // Set the H cost.
                    neighbourNode.hCost = GetManhattenDistance(neighbourNode, targetNode);
                    // Set the parent of the node for retracing steps.
                    neighbourNode.parentNode = currentNode;

                    // If the neighbor is not in the openlist.
                    if (!openList.Contains(neighbourNode))
                    {
                        openList.Add(neighbourNode);
                    }
                }
            }
        }
    }

    /**
    * @Brief Function to set the final path in the grid.
    * @Param Start position.
    * @Param Target position.
    */
    void GetFinalPath(Node startingNode, Node endNode)
    {
        List<Node> finalPath = new List<Node>(); 
        Node currentNode = endNode;

        while (currentNode != startingNode)
        {
            finalPath.Add(currentNode);
            currentNode = currentNode.parentNode;
        }

        finalPath.Reverse();

        gridReference.finalPath = finalPath;

    }

    /**
    * @Brief Calculate the heuristic value. 
    * @Param Current node of the path.
    * @Param Neighbouring node on the path.
    */
    int GetManhattenDistance(Node nodeA, Node nodeB)
    {
        int ix = Mathf.Abs(nodeA.gridX - nodeB.gridX);
        int iy = Mathf.Abs(nodeA.gridY - nodeB.gridY);

        return ix + iy;
    }
}
