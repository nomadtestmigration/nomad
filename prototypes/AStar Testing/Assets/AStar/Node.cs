using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node 
{
    /**
    * Code is based on this youtube tutorial https://www.youtube.com/watch?v=AKKpPmxx07w. 
    */

    public int gridX;
    public int gridY;

    public bool isWall;
    public Vector3 position;

    public Node parentNode;
    /**
    * @Brief To calculate how far a node is from the target node an F cost is required. The F cost is calculated through adding the gCost and hCost together.
    */
    public int gCost;
    public int hCost;
    public int fCost;
    
    /**
    * @Brief Constructor of a Node. 
    * @Param Check to see if node is a wall.
    * @Param Position of the node.
    * @Param Grid position on the X axis.
    * @Param Grid position on the Y axis.
    */
    public Node(bool iIsWall, Vector3 vPos, int iGridX, int iGridY)
    {
        isWall = iIsWall;
        position = vPos;
        gridX = iGridX;
        gridY = iGridY;
    }

    /**
    * @Brief Calculate the F cost by adding up G and H cost. 
    */
    public int CalculateFCost()
    {
        fCost = gCost + hCost;
        return fCost;
    }
}
