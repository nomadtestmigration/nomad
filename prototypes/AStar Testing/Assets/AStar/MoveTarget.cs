using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTarget : MonoBehaviour
{
    public Vector3 position;
    void Update()
    {
        position.x += Input.GetAxis("Horizontal") * Time.deltaTime * 20;
        position.z += Input.GetAxis("Vertical") * Time.deltaTime * 20;
        transform.localPosition = position;
    }
}
