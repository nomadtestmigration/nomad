using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using UnityEditor;
using UnityEditor.UIElements;

using UnityEngine;
using UnityEngine.UIElements;

public class ItemDatabase : EditorWindow
{
    private static List<Item> m_itemDatabase = new List<Item>();

    private VisualElement m_itemsTab;
    private static VisualTreeAsset m_itemRowTemplate;
    private ScrollView m_detailSection;

    private ListView m_itemListView;
    private Sprite m_defaultItemIcon;
    private Item m_activeItem;
    private VisualElement m_largeDisplayIcon;

    private int m_itemHeight = 60;

    [MenuItem("Prototype/Item Database")]
    public static void Init()
    {
        ItemDatabase wnd = GetWindow<ItemDatabase>();
        wnd.titleContent = new GUIContent("Item Database");

        Vector2 size = new Vector2(1000, 475);
        wnd.minSize = size;
        wnd.maxSize = size;
    }

    /// <summary>
    /// Sets up the structure of the UI by loading and assigning the existing UXML, USS and Asset files.
    /// </summary>
    public void CreateGUI()
    {
        ImportFiles();

        //Get references for later
        m_detailSection = rootVisualElement.Q<ScrollView>("ScrollView_Details");
        m_largeDisplayIcon = m_detailSection.Q<VisualElement>("Icon");

        //Load all existing item assets 
        LoadAllItems();

        //Populate the listview
        m_itemsTab = rootVisualElement.Q<VisualElement>("ItemsTab");
        GenerateListView();

        AddButtonClickEvents();

        AddCallbacksForNewItems();

    }

    private void AddButtonClickEvents() {
        //Hook up button click events
        rootVisualElement.Q<Button>("Btn_AddItem").clicked += AddItemOnClick;
        rootVisualElement.Q<Button>("Btn_DeleteItem").clicked += DeleteItemOnClick;
    }

    private void AddCallbacksForNewItems() {
        //Register Value Changed Callbacks for new items added to the ListView
        m_detailSection.Q<TextField>("ItemName").RegisterValueChangedCallback(evt => 
        {
            m_activeItem.friendlyName = evt.newValue;
            m_itemListView.Rebuild();
        });

        m_detailSection.Q<ObjectField>("IconPicker").RegisterValueChangedCallback(evt =>
        {
            Sprite newSprite = evt.newValue as Sprite;
            m_activeItem.icon = newSprite == null ? m_defaultItemIcon : newSprite;
            m_largeDisplayIcon.style.backgroundImage = newSprite == null ? m_defaultItemIcon.texture : newSprite.texture;

            m_itemListView.Rebuild();
            
        });
    }

    private void ImportFiles() {
        // Import the UXML Window
        VisualTreeAsset visualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Assets/WUG/Editor/ItemDatabase.uxml");
        VisualElement rootFromUXML = visualTree.Instantiate();
        rootVisualElement.Add(rootFromUXML);

        // Import the stylesheet
        StyleSheet styleSheet = AssetDatabase.LoadAssetAtPath<StyleSheet>("Assets/WUG/Editor/ItemDatabase.uss");
        rootVisualElement.styleSheets.Add(styleSheet);

        //Import the ListView Item Template
        m_itemRowTemplate = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Assets/WUG/Editor/ItemRowTemplate.uxml");
        m_defaultItemIcon = (Sprite)AssetDatabase.LoadAssetAtPath("Assets/WUG/Sprites/UnknownIcon.png", typeof(Sprite));
    }

    /// <summary>
    /// Delete the active Item asset from the Asset/Data folder
    /// </summary>
    private void DeleteItemOnClick()
    {
        //Get the path of the fie and delete it through AssetDatabase
        string path = AssetDatabase.GetAssetPath(m_activeItem);
        AssetDatabase.DeleteAsset(path);

        //Purge the reference from the list and refresh the ListView
        m_itemDatabase.Remove(m_activeItem);
        m_itemListView.Rebuild();

        //Nothing is selected, so hide the details section
        m_detailSection.style.visibility = Visibility.Hidden;

    }

    /// <summary>
    /// Add a new Item asset to the Asset/Data folder
    /// </summary>
    private void AddItemOnClick()
    {
        //Create an instance of the scriptable object
        Item newItem = CreateInstance<Item>();
        newItem.friendlyName = $"New Item";
        newItem.icon = m_defaultItemIcon;

        //Create the asset 
        AssetDatabase.CreateAsset(newItem, $"Assets/Data/{newItem.ID}.asset");

        //Add it to the item list
        m_itemDatabase.Add(newItem);

        //Refresh the ListView so everything is redrawn again
        m_itemListView.Rebuild();
        m_itemListView.style.height = m_itemDatabase.Count * m_itemHeight + 5;

        m_itemListView.SetSelection(m_itemDatabase.Count - 1);
    }

    /// <summary>
    /// Look through all items located in Assets/Data and load them into memory
    /// </summary>
    private void LoadAllItems()
    {
        m_itemDatabase.Clear();

        string[] allPaths = Directory.GetFiles("Assets/Data", "*.asset", SearchOption.AllDirectories);

        foreach (string path in allPaths)
        {
            string cleanedPath = path.Replace("\\", "/");
            m_itemDatabase.Add((Item)AssetDatabase.LoadAssetAtPath(cleanedPath, typeof(Item)));
        }
    }   
    
    /// <summary>
    /// Create the list view based on the asset data
    /// </summary>
    private void GenerateListView()
    {
        //Defining what each item will visually look like. In this case, the makeItem function is creating a clone of the ItemRowTemplate.
        Func<VisualElement> makeItem = () => m_itemRowTemplate.CloneTree();

        //Define the binding of each individual Item that is created. Specifically, 
        //it binds the Icon visual element to the scriptable object�s Icon property and the 
        //Name label to the friendlyName property.
        Action<VisualElement, int> bindItem = (e, i) =>
        {
            e.Q<VisualElement>("Icon").style.backgroundImage = m_itemDatabase[i] == null ? m_defaultItemIcon.texture :  m_itemDatabase[i].icon.texture;
            e.Q<Label>("Name").text = m_itemDatabase[i].friendlyName;
        };

        //Create the listview and set various properties
        m_itemListView = new ListView(m_itemDatabase, m_itemHeight, makeItem, bindItem);
        m_itemListView.selectionType = SelectionType.Single;
        m_itemListView.style.height = m_itemDatabase.Count * m_itemHeight + 5;
        m_itemsTab.Add(m_itemListView);

        m_itemListView.onSelectionChange += ListViewOnSelectionChange;
        m_itemListView.SetSelection(0);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="selectedItems"></param>
    private void ListViewOnSelectionChange(IEnumerable<object> selectedItems)
    {
        //Get the first item in the selectedItems list. 
        //There will only ever be one because SelectionType is set to Single
        m_activeItem = (Item)selectedItems.First();

        //Create a new SerializedObject and bind the Details VE to it. 
        //This cascades the binding to the children
        SerializedObject so = new SerializedObject(m_activeItem);
        m_detailSection.Bind(so);

        //Set the icon if it exists
        if (m_activeItem.icon != null)
        {
            m_largeDisplayIcon.style.backgroundImage = m_activeItem.icon.texture;
        }

        //Make sure the detail section is visible. This can turn off when you delete an item
        m_detailSection.style.visibility = Visibility.Visible;
    }

}
