using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName = "Data/New Item", order = 1)]
public class Item : ScriptableObject
{
    public string ID = Guid.NewGuid().ToString().ToUpper();
    public string friendlyName;
    public string description;
    public Categories category;
    public bool stackable;
    public int buyPrice;
    [Range(0,1)]  public float sellPercentage;
    public Sprite icon;

    public enum Categories
    {
        ARMOR,
        FOOD,
        POTION,
        WEAPON,
        JUNK
    }

}
