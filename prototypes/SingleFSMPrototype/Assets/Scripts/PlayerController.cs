using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    public Rigidbody rb;

    private float movementX;
    private float movementY;

    private float mouseX;
    private float mouseY;

    [Header("Movement Settings")]
    public float prevSpeed;
    public float speed = 10f;
    public float sprintSpeed = 20f;
    public float crouchSpeed = 5f;
    public float rotateSpeed = 3f;
    public float jumpHeight = 400f;
    public float dashSpeed = 5f;
    public float dashDuration = 1f;

    [Header("Action Booleans")]
    public bool grounded;
    public bool jumping;
    public bool moving;
    public bool sprinting;
    public bool dashing;
    public bool crouching;
    public bool blocking;
    public bool attacking;
    public bool heavyAttacking;

    [Header("Camera Settings")]
    public Transform Camera;
    public float cameraPitch = 25f;
    public float mouseSensitivity = 20f;

    void Start()
    {
        rb = GetComponent<Rigidbody>();

        /**
         * Locks the cursor to the center of the window
         */
        Cursor.lockState = CursorLockMode.Locked;
    }

    /**
     *  Move Action
     *  @Param movementValue, gives a Vector2 value based on user input
     */
    void OnMove(InputValue movementValue)
    {
        Vector2 movementVectorValue = movementValue.Get<Vector2>();

        movementX = movementVectorValue.x;
        movementY = movementVectorValue.y;

        /**
         *  As long as the player is giving movement input go to the movingState.
         */
        if (movementVectorValue != Vector2.zero)
        {
            moving = true;
        }
        else
        {
            moving = false;
        }
    }

    /**
     *  Method that moves the player with the input given in OnMove.
     */
    public void Move()
    {
        /**
         * transform Movement to the the direction of the character
         */
        Vector3 movement = new(movementX, 0.0f, movementY);
        movement = transform.TransformDirection(-movement);
        Vector3 direction = (transform.position - (transform.position + movement)).normalized;

        /**
         * move player in the direction
         */
        rb.MovePosition(transform.position + (direction * Time.deltaTime * speed));
    }

    /**
     *  Look Action
     *  @Param value, gives a Vector2 value based on mouse movent
     */
    void OnLook(InputValue value)
    {
        mouseX = value.Get<Vector2>().x * mouseSensitivity * Time.deltaTime;
        mouseY = value.Get<Vector2>().y * mouseSensitivity * Time.deltaTime;
    }

    /**
     *  Jump Action, sets jumping to true as long as the action key is being pressed and the player is grounded.
     *  @param value, gives a value when the key is being pressed, used as a bool.
     */
    void OnJump(InputValue value)
    {
        CheckGround();

        if (grounded)
        {
            jumping = value.isPressed;
            grounded = false;
        }
    }

    /**
     * Method to check if the player is grounded before the character jumps
     */
    public void CheckGround() 
    {
        Ray groundRay = new(rb.position, Vector3.down);
        if (Physics.Raycast(groundRay, out RaycastHit groundHit, 2f))
        {
            grounded = true;
        }
        else
        {
            grounded = false;
        }
    }

    /**
     *  Sprint Action, activates and deactivates the SprintingState by setting sprinting to true or false based on the key being pressed or not.
     *  @Param value, gives a value when the key is being pressed, used as a bool.
     */
    public void OnSprint(InputValue value)
    {
        sprinting = value.isPressed;
    }

    /**
     * Dash Action, activates the DashingState by setting dashing to true;
     */
    public void OnDash()
    {
        dashing = true;
    }

    /**
     * Crouch Action, activates and deactivates the CrouchingState by setting crouching to true or false based on the key being pressed or not.
     * @Param value, gives a value when the key is being pressed, used as a bool.
     */
    public void OnCrouch(InputValue value)
    {
        crouching = value.isPressed;
    }

    /**
     * Attack Action, activates the attack state after the attack button is tapped.
     */
    public void OnAttack()
    {   
        attacking = true;
    }

    /**
     * HeavyAttack Action, activates the heavy attack state after the attack button is held for 0.5 seconds.
     */
    public void OnHeavyAttack()
    {
        heavyAttacking = true;
    }

    /**
     *  Block Action, activates and deactivates the BlockingState by setting blocking to true as long as the action key is being pressed.
     *  @Param value, gives a value when the key is being pressed, used as a bool.
     */
    public void OnBlock(InputValue value)
    {
        blocking = value.isPressed;
    }

    /**
     * Method that checks if the player is grounded after it collides with something.
     */
    public void OnCollisionEnter(Collision collision)
    {
        CheckGround();
    }

    public void Update()
    {
        /**
         * Rotate camera on x-axis when looking up and down (inverted because camera angles are inverterd compared to mouse delta up and down).
         */
        cameraPitch -= mouseY;

        /**
         * set Limits for looking up and down.
         */
        cameraPitch = Mathf.Clamp(cameraPitch, -90f, 90f);

        Camera.localEulerAngles = Vector3.right * cameraPitch;

        /**
         * Rotate player on y axis when looking left and right.
         */
        Vector2 mouseDelta = new(mouseX, mouseY);

        transform.Rotate(Vector2.up * mouseDelta.x);
    }

}
