using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SprintingState : State
{

    public void Start()
    {
        transitions = new List<Transition>
        {
            new Transition(() => !player.sprinting, gameObject.GetComponent<MovingState>()),
            new Transition(() => player.dashing, gameObject.GetComponent<DashingState>()),
            new Transition(() => player.crouching, gameObject.GetComponent<CrouchingState>()),
            new Transition(() => player.jumping, gameObject.GetComponent<JumpingState>()),
            new Transition(() => player.attacking, gameObject.GetComponent<AttackingState>()),
            new Transition(() => player.heavyAttacking, gameObject.GetComponent<HeavyAttackState>()),
            new Transition(() => player.blocking, gameObject.GetComponent<BlockingState>())
        };
    }

    public override void OnEnable()
    {
        //Debug message to check if the state is active.
        Debug.Log("I am Speed");

        //Sets the previous speed of the player to change the new speed back in OnDisable.
        player.prevSpeed = player.speed;
        //Then sets the speed to the sprintSpeed.
        player.speed = player.sprintSpeed;
    }

    public override void OnDisable()
    {
        //Set the speed back to the speed before this state.
        player.speed = player.prevSpeed;
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();

        player.Move();
    }

}
