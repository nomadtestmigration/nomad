using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashingState : State
{
    [SerializeField]
    private float timeX;
    [SerializeField]
    private float time;
    [SerializeField]
    private Vector3 dashDirection;
    [SerializeField]
    private bool endRoutine;

    public void Start()
    {
        transitions = new List<Transition>
        {
            new Transition(() => endRoutine, gameObject.GetComponent<IdleState>())
        };
    }

    /**
    * Turn endRoutine false so the transition isn't true
    * timeX is the time the player remains in the dash coroutine.
    * 
    * dashDirection is the direction the player faces.
    * Start coroutine.
    */
    public override void OnEnable()
    {
        endRoutine = false;

        timeX = player.dashDuration;

        time = Time.time;

        dashDirection = gameObject.transform.forward;

        StartCoroutine(DashRoutine());
    }

    public override void OnDisable()
    {

        //Debug message to check if the state has stopped.
        Debug.Log("Stopped Dashing!");

        //Reset bools when leaving the state.
        player.dashing = false;
        endRoutine = false;
    }

    IEnumerator DashRoutine()
    {

        //Run this coroutine as long as the time since this State started is smaller than the time since the state started + timeX (the time we want te player to remain in the dash coroutine).
        Debug.Log("Started coroutine");
        while (Time.time < time + timeX)
        {
            player.rb.MovePosition(transform.position + (dashDirection * player.dashSpeed * Time.deltaTime));

            yield return null;
        }

        endRoutine = true;

        Debug.Log("Ended coroutine");
        yield return null;
    }
}
