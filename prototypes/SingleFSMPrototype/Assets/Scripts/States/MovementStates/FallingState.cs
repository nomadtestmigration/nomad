using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingState : State
{
    public void Start()
    {
        transitions = new List<Transition>
        {
           new Transition(() => player.grounded, gameObject.GetComponent<IdleState>())
        };
    }


    public override void OnEnable()
    {
        //Debug message to check if the state is active.
        Debug.Log("Wheeeeee");
    }


    public override void Update()
    {
        base.Update();

        //Add downward force on the player to simulate gravity.
        player.rb.AddForce(Vector3.down * 6);
    }


    public override void FixedUpdate()
    {
        base.FixedUpdate();

        player.Move();
    }

}