using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleState : State
{
    public void Start()
    {
        transitions = new List<Transition>
        {
            new Transition(() => player.moving, gameObject.GetComponent<MovingState>()),
            new Transition(() => player.sprinting, gameObject.GetComponent<SprintingState>()),
            new Transition(() => player.dashing, gameObject.GetComponent<DashingState>()),
            new Transition(() => player.crouching, gameObject.GetComponent<CrouchingState>()),
            new Transition(() => player.jumping, gameObject.GetComponent<JumpingState>()),
            new Transition(() => player.attacking, gameObject.GetComponent<AttackingState>()),
            new Transition(() => player.heavyAttacking, gameObject.GetComponent<HeavyAttackState>()),
            new Transition(() => player.blocking, gameObject.GetComponent<BlockingState>()),
        };
    }


    public override void OnEnable()
    {
        //Debug message to check if the state is active.
        Debug.Log("Just Chilling");
    }


    public override void FixedUpdate()
    {
        base.FixedUpdate();

        player.Move();
    }
}
