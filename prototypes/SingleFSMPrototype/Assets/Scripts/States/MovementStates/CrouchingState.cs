using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrouchingState : State
{
    public void Start()
    {
        transitions = new List<Transition>
        {
            new Transition(() => !player.crouching, gameObject.GetComponent<IdleState>()),
            new Transition(() => player.dashing, gameObject.GetComponent<DashingState>())
        };
    }

    public override void OnEnable()
    {
        //Debug message to check if the state is active.
        Debug.Log("Get Low!");

        // Sets the previous speed of the player to change the new speed back in OnDisable.
        player.prevSpeed = player.speed;
        //Then sets the speed to the sprintSpeed.
        player.speed = player.crouchSpeed;
    }

    public override void OnDisable()
    {
        //Set the speed back to the speed before this state.
        player.speed = player.prevSpeed;
    }


    public override void FixedUpdate()
    {
        base.FixedUpdate();

        player.Move();
    }

}
