using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpingState : State
{
    public void Start()
    {
        transitions = new List<Transition>
        {
            /**
             * Transitions to falling state when the velocity of the player on the y-axis drops below 0.
             * This is not how it should be in the final product, because sometimes in Unity the player will drop below 0 when just moving around.
             */
            new Transition(() => player.rb.velocity.y < 0, gameObject.GetComponent<FallingState>())
        };
    }

    public override void OnEnable()
    {
        //Debug message to check if the state is active.
        Debug.Log("To the moon!");

        //Adds upward force on the player.
        player.rb.AddForce(Vector3.up * player.jumpHeight);
    }

    public override void OnDisable()
    {
        base.OnDisable();

        player.jumping = false;
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();

        player.Move();
    }

}
