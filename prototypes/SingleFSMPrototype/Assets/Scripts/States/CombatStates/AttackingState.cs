using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackingState : State
{
    public void Start()
    {
        transitions = new List<Transition>
        {
            new Transition(() => !player.attacking, gameObject.GetComponent<IdleState>()),
            new Transition(() => player.blocking, gameObject.GetComponent<BlockingState>())
        };
    }


    public override void OnEnable()
    {
        //Debug message to check if the state is active.
        Debug.Log("Attack!");
  
        //This will make sure the state transitions untill animations are added, that can be used to determine when to transition out of the state.
        player.attacking = false;
    }

    public override void OnDisable()
    {

    }


    public override void FixedUpdate()
    {
        base.FixedUpdate();

        player.Move();
    }
}
