using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockingState : State
{
    public void Start()
    {
        transitions = new List<Transition>
        {
            new Transition(() => !player.blocking, gameObject.GetComponent<IdleState>()),
            new Transition(() => player.attacking, gameObject.GetComponent<AttackingState>()),
            new Transition(() => player.heavyAttacking, gameObject.GetComponent<HeavyAttackState>()),
            new Transition(() => player.crouching, gameObject.GetComponent<CrouchingState>())
        };
    }


    public override void OnEnable()
    {
        //Debug message to check if the state is active.
        Debug.Log("Blocking!");
    }

    public override void OnDisable()
    {

    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();

        player.Move();
    }

}
