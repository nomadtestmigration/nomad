using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transition
{
    public Func<bool> condition;
    public State target;

    public Transition(Func<bool> aCondition, State aTarget)
    {
        condition = aCondition;
        target = aTarget;
    }
}