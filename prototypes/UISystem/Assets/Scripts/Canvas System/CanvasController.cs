using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class CanvasController : MonoBehaviour
{
    [SerializeField] private CanvasType canvasType;
    public CanvasType CanvasType => canvasType;
}
