using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Temp : MonoBehaviour
{
    private CanvasManager canvasManager;
    private void Awake()
    {
        canvasManager = FindObjectOfType<CanvasManager>();
    }

    public void StartGame()
    {
        canvasManager.SwitchCanvas(CanvasType.GAMEHUD);
        SceneManager.LoadScene("Level1");
    }

    public void NextLevel()
    {
        canvasManager.SwitchCanvas(CanvasType.GAMEHUD);
        SceneManager.LoadScene("Level2");
    }
    
    public void PauseMenu()
    {
        canvasManager.SwitchCanvas(CanvasType.PAUSEMENU);
    }
    
    public void ResumeGame()
    {
        canvasManager.SwitchCanvas(CanvasType.GAMEHUD);
    }

    public void QuitGame()
    {
        canvasManager.SwitchCanvas(CanvasType.MAINMENU);
        SceneManager.LoadScene("MainMenu");
    }
}
