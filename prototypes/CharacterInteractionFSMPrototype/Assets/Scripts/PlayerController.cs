using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    [Header("Player compononets")]
    public Rigidbody rb;

    [Header("Player movement values")]
    public float movementX;
    public float movementY;
    public Vector2 movementVectorValue;
    public float mouseX;
    public float mouseY;

    [Header("CheckGround() variables")]
    public LayerMask ground;
    public bool grounded;
    [SerializeField]
    private RaycastHit groundHit;

    [Header("Default values")]
    public float speed = 10f;
    public float rotateSpeed = 3f;
    public float jumpHeight = 400f;
    public float mouseSensitivity = 20f;
    public float dashSpeed;
    public float dashDurationInSec = 1f;
    public float crouchSpeed = 5f;
    public float sprintSpeed = 20f;

    [Header("State boolean initiaters")]
    public bool jumping;
    public bool moving;
    public bool attacking;
    public bool blocking;
    public bool dashing;
    public bool crouching;
    public bool sprinting;

    [Header("Camera parts")]
    public Transform camera;
    [SerializeField]
    private float cameraPitch = 0f;

    /**
     * Get and set RigidBody from current GameObject to rb for every state to use.
     * 
     * Lock cursor in the middle of the screen to simulate first person view.
     */
    private void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();
        //lock mouse in the middle of the screen
        Cursor.lockState = CursorLockMode.Locked;
    }

    /**
     * Set movement values to variables to be used to move.
     * 
     * Check if player is moving to smoothen transitions between states.
     * 
     * @Param movementValue This is a Vector2 value inputted from the new input system when pressing binded keys and get translated to movement in game.
     */
    void OnMove(InputValue movementValue)
    {
        movementVectorValue = movementValue.Get<Vector2>();

        movementX = movementVectorValue.x;
        movementY = movementVectorValue.y;

        if (movementVectorValue != Vector2.zero)
        {
            moving = true;
        } else
        {
            moving = false;
        }
    }
    
    /**
     * Ran when player pressed binded jump button.
     * 
     * If the player stands on the ground jumping boolean is true to signify state transition to jump state.
     */
    private void OnJump()
    {
        if (CheckGround())
        {
            jumping = true;
        }
    }

    /**
     * Ran when mouse is moved by player.
     * 
     * Sets mouse movement values multiplied by sensitivity and Time.deltaTime.
     * 
     * @Param value This is a Vector2 value that tracks mouse movement.
     */
    public void OnLook(InputValue value)
    {
        mouseX = value.Get<Vector2>().x * mouseSensitivity * Time.deltaTime;
        mouseY = value.Get<Vector2>().y * mouseSensitivity * Time.deltaTime;

        //inputView = new Vector2(mouseX, mouseY);
    }

    /**
     * Ran when player presses attack button.
     * 
     * Boolean attacking set to true to signify transition to attackingstate.
     */
    public void OnAttack()
    {
        attacking = true;
    }

    /**
    * Ran when player presses block button.
    * 
    * Blocking boolean set to true to signify transition to blockingstate.
    * 
    * @Param value Boolean is checked true if held down and false if released and set to blocking to signify state change.
    */
    public void OnBlock(InputValue value)
    {
        blocking = value.isPressed;
    }

    /**
    * Ran when player presses crouch button.
    * 
    * Crouching boolean set to true to signify transition to crouchingstate.
    * 
    * @Param value Boolean is checked true if held down and false if released and set to crouching to signify state change.
    */
    public void OnCrouch(InputValue value)
    {
        crouching = value.isPressed;
    }

    /**
    * Ran when player presses sprint button.
    * 
    * Also check if moving for smooth transition between states.
    * Sprinting boolean set to true to signify transition to sprintingstate.
    * 
    * @Param value Boolean is checked true if held down and false if released and set to sprinting to signify state change.
    */
    public void OnSprint(InputValue value)
    {
        if (moving)
        {
            sprinting = value.isPressed;
        }
    }

    /**
    * Ran when player presses dash button.
    * 
    * Dashing boolean set to true to signify transition to dashingstate.
    */
    public void OnDash()
    {
        dashing = true;
    }

    /**
     * Code Mathf.clamp sets camera limit to 90degrees upwards and downwards to prevent overviews.
     * 
     * Code localEulerAnglers turns the camera to set mouse movement.
     * 
     * Code transform.rotate rotates player on y-axis on mouse movement.
     */
    private void Update()
    {
        //rotate camera on x-axis when lookin up and down (inverted because camera angles are inverterd compared mouse delta up and down).
        cameraPitch -= mouseY;
        //set borders for looking up and down.
        cameraPitch = Mathf.Clamp(cameraPitch, -90f, 90f);

        camera.localEulerAngles = Vector3.right * cameraPitch;

        //Rotate player on y axis when looking left / right.
        Vector2 mouseDelta = new Vector2(mouseX, mouseY);

        transform.Rotate(Vector2.up * mouseDelta.x);
    }

    /**
     * Checks if player is grounded.
     * 
     * Sends raycast downwards from player and checks if layer ground is hit.
     * 
     * Returns true if ground is hit.
     */
    public bool CheckGround()
    {
        //send raycast below player with variables: start, direction, distance, hit return, layermask.
        Ray groundRay = new Ray(rb.position, Vector3.down);
        grounded = Physics.Raycast(groundRay, out groundHit, 2f, ground);

        Debug.DrawRay(rb.position, Vector3.down, Color.white, 1f); 

        return grounded;
    }

    /**
     * Moves player in 8 main directions with moveposition which takes current position and direction multiplied by set speed.
     */
    public void Move()
    {
        Vector3 movement = new Vector3(movementX, 0.0f, movementY);
        movement = transform.TransformDirection(-movement);
        Vector3 direction = (transform.position - (transform.position + movement)).normalized;

        rb.MovePosition(transform.position + (direction * Time.deltaTime * speed));
    }
}
