using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementState : MonoBehaviour
{
    public List<Transition> transitions;
    public PlayerController player;

    public virtual void Awake()
    {
        transitions = new List<Transition>();

        player = GetComponent<PlayerController>();

        //Setup your transitions.
    }

    public virtual void OnEnable()
    {
        //Develop state's initializations.
    }

    public virtual void OnDisable()
    {
        //Develop state's finalization.
    }

    public virtual void Update()
    {
        //Develop behaviour.
    }

    public virtual void FixedUpdate()
    {
        //Develop behaviour.
    }

    /**
     * This function checks every end of frame if a condition for the current state machine is true and will transition to that state if true.
     */
    public void LateUpdate()
    {
        foreach (Transition t in transitions)
        {
            if (t.condition())
            {
                t.movementTarget.enabled = true;
                this.enabled = false;
                return;
            }
            continue;
        }
    }
}
