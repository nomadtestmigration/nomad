using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transition
{
    public Func<bool> condition;
    public MovementState movementTarget;
    public CombatState combatTarget;

    /**
     * Transition is made to make transitions which take conditions and send state the to given state.
     * Both functions are made for different State machines (MovementState and CombatState).
     * 
     * @param condition This is the condition which will be checked. If true the transition will complete, if false nothing happends.
     * @param target The state will be transitioned to the given target state.
     */
    public Transition(Func<bool> condition, MovementState target)
    {
        this.condition = condition;
        this.movementTarget = target;
    }

    public Transition(Func<bool> condition, CombatState target)
    {
        this.condition = condition;
        this.combatTarget = target;
    }
}