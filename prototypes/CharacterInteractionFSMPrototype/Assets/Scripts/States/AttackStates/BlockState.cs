using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockState : CombatState
{

    /**
    * Set transitions from current state to given state on gameobject.
    */
    public void Start()
    {
        transitions = new List<Transition>
        {
            new Transition(() => player.attacking, gameObject.GetComponent<AttackState>()),
            new Transition(() => !player.blocking, gameObject.GetComponent<DefencelessState>())
        };
    }

    /**
     * Blocking state currently without behavior is active when holding the block button.
     */

    public override void OnEnable()
    {
        Debug.Log("blocking");
    }

    public override void OnDisable()
    {
        Debug.Log("Stopped blocking!");
    }

    public override void Update()
    {
        base.Update();

        Debug.Log("Blocking");
    }
}
