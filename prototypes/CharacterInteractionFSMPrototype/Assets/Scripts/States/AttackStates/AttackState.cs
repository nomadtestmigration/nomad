using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackState : CombatState
{
    /**
    * Set transitions from current state to given state on gameobject.
    */
    public void Start()
    {
        transitions = new List<Transition>
        {
            new Transition(() => !player.attacking, gameObject.GetComponent<DefencelessState>())
        };
    }

    /**
     * Start coroutine which simulates waiting for an animation in this case.
     */
    public override void OnEnable()
    {
        Debug.Log("Attacking!");

        StartCoroutine(WaitRoutine());
    }

    public override void OnDisable()
    {
        Debug.Log("Stopped attacking!");
    }

    /**
     * Coroutine which will wait 5 seconds (5 can be replaced with animation timer).
     * Then sets attacking to false to transition out of attacking state.
     */
    IEnumerator WaitRoutine()
    {
        yield return new WaitForSeconds(5); //maybe wait for animation duration here.

        player.attacking = false;
    }
}
