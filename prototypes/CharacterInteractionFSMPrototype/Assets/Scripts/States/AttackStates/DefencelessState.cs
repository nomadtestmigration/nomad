using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefencelessState : CombatState
{
    /**
     * Set transitions from current state to given state on gameobject.
     */
    public void Start()
    {
        transitions = new List<Transition>
        {
            new Transition(() => player.blocking, gameObject.GetComponent<BlockState>()),
            new Transition(() => player.attacking, gameObject.GetComponent<AttackState>())
        };
    }

    /**
     * This state is only used as default state to fall back upon when not doing any combat actions.
     */
    public override void OnEnable()
    {
        Debug.Log("Defencless!");
    }

    public override void OnDisable()
    {
        Debug.Log("Stopped being defenceless!");
    }
}
