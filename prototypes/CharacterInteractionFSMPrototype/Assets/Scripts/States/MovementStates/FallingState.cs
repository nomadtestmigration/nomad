using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingState : MovementState
{
    /**
    * Set transitions from current state to given state on gameobject.
    */
    public void Start()
    {
        transitions = new List<Transition>
        {
            new Transition(() => player.CheckGround() && !player.moving, gameObject.GetComponent<IdleState>()),
            new Transition(() => player.CheckGround() && player.sprinting, gameObject.GetComponent<SprintingState>()),
            new Transition(() => player.CheckGround() && player.moving, gameObject.GetComponent<MovingState>()),
            new Transition(() => player.dashing, gameObject.GetComponent<DashState>())
        };
    }

    public override void OnEnable()
    {
        Debug.Log("Falling!");
    }

    public override void OnDisable()
    {
        Debug.Log("Stopped Falling!");
    }

    /**
     * Calling Move() function here allows the player to move during this state.
     * 
     * Checking for any inputs gives a clean transition to moveState if the player hits the ground.
     */
    public override void FixedUpdate()
    {
        base.FixedUpdate();


        player.Move();
    }
}
