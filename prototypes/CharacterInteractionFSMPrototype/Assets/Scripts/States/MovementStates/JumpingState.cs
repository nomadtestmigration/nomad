using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpingState : MovementState
{
    /**
    * Set transitions from current state to given state on gameobject.
    * 
    * Leave JumpingState when y velocity stars going negative or player dashes.
    */
    public void Start()
    {
        transitions = new List<Transition>
        {
            new Transition(() => player.rb.velocity.y < 0f, gameObject.GetComponent<FallingState>()),
            new Transition(() => player.dashing, gameObject.GetComponent<DashState>())
        };
    }

    /**
     * Player is sent upwards with set force.
     */
    public override void OnEnable()
    {
        Debug.Log("Jumping!");
        player.grounded = false;

        player.rb.AddForce(0.0f, player.jumpHeight, 0.0f);
    }

    /**
     * Set jumping boolean false to indicate leaving the state.
     */
    public override void OnDisable()
    {
        Debug.Log("Stopped Jumping!");
        player.jumping = false;
    }

    /**
    * Call move every physics update to move player in 8 main directions.
    */
    public override void FixedUpdate()
    {
        base.FixedUpdate();

        player.Move();
    }
}
