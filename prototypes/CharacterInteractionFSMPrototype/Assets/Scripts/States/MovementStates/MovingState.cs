using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class MovingState : MovementState
{
    /**
    * Set transitions from current state to given state on gameobject.
    */
    public void Start()
    {
        transitions = new List<Transition>
        {
            new Transition(() => player.movementVectorValue == Vector2.zero, gameObject.GetComponent<IdleState>()),
            new Transition(() => player.jumping, gameObject.GetComponent<JumpingState>()),
            new Transition(() => player.sprinting, gameObject.GetComponent<SprintingState>()),
            new Transition(() => player.crouching, gameObject.GetComponent<CrouchingState>()),
            new Transition(() => player.dashing, gameObject.GetComponent<DashState>())
        };
    }

    public override void OnEnable()
    {
        Debug.Log("Walking!");
    }

    public override void OnDisable()
    {
        Debug.Log("Stopped Walking!");
    }

    /**
     * Call move every physics update to move player in 8 main directions.
     */
    public override void FixedUpdate()
    {
        base.FixedUpdate();

        player.Move();
    }
}
