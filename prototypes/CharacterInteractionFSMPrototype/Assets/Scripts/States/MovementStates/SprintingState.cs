using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class SprintingState : MovementState
{
    private float savedSpeed;

    /**
    * Set transitions from current state to given state on gameobject.
    */
    public void Start()
    {
        transitions = new List<Transition>
        {
            new Transition(() => player.movementVectorValue == Vector2.zero, gameObject.GetComponent<IdleState>()),
            new Transition(() => player.jumping, gameObject.GetComponent<JumpingState>()),
            new Transition(() => !player.sprinting && player.moving, gameObject.GetComponent<MovingState>()),
            new Transition(() => player.crouching, gameObject.GetComponent<CrouchingState>()),
            new Transition(() => player.dashing, gameObject.GetComponent<DashState>())
        };
    }

    /**
    * Get and set current player speed multiplier.
    * 
    * Get and set new speed for current state.
    */
    public override void OnEnable()
    {
        Debug.Log("Sprinting!");
        savedSpeed = player.speed;
        player.speed = player.sprintSpeed;
    }

    /**
    * When leaving state set player speed back to original multiplier.
    * 
    * Set sprinting boolean false to indicate leaving the state.
    */
    public override void OnDisable()
    {
        Debug.Log("Stopped Sprinting!");

        player.speed = savedSpeed;
        player.sprinting = false;
    }

    /**
    * Call move every physics update to move player in 8 main directions.
    */
    public override void FixedUpdate()
    {
        base.FixedUpdate();

        player.Move();
    }
}
