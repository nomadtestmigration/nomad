using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashState : MovementState
{
    private float timeX;
    Vector3 dashDirection;
    private bool endRoutine;

    /**
    * Set transitions from current state to given state on gameobject.
    */
    public void Start()
    {
        transitions = new List<Transition>
        {
            new Transition(() => endRoutine, gameObject.GetComponent<IdleState>())
        };
    }

    /**
     * Turn endRoutine false so the transition isnt true.
     * 
     * Get the timer set by developer as dashDurationInSec.
     * 
     * Get the current facing direction to dash towards that direction.
     * 
     * Start coroutine which dashes in current direction based on given time.
     */
    public override void OnEnable()
    {
        endRoutine = false;

        timeX = player.dashDurationInSec;

        dashDirection = gameObject.transform.forward;

        StartCoroutine(DashRoutine());
    }
    
    /**
     * Dashing boolean to false when exiting the state.
     */
    public override void OnDisable()
    {
        Debug.Log("Stopped Dashing!");

        player.dashing = false;
    }

    /**
     * Coroutine which dashes in current direction based on given time.
     * 
     * Stay in while loop until frames exceed given time.
     * Dash player in current direction with set speed.
     * 
     * Boolean which announces state exit transition.
     * 
     * Stop routine.
     */
    IEnumerator DashRoutine()
    {
        Debug.Log("Started coroutine");
        while (timeX > 0f)
        {
            player.rb.MovePosition(transform.position + (dashDirection * player.dashSpeed * Time.deltaTime));

            timeX -= Time.deltaTime;
            yield return null;
        }

        endRoutine = true;

        Debug.Log("Ended coroutine");
        yield return null;
    }
}
