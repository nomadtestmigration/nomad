using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleState : MovementState
{
    /**
    * Set transitions from current state to given state on gameobject.
    */
    public void Start()
    {
        transitions = new List<Transition>
        {
            new Transition(() => player.moving, gameObject.GetComponent<MovingState>()),
            new Transition(() => player.jumping, gameObject.GetComponent<JumpingState>()),
            new Transition(() => player.crouching, gameObject.GetComponent<CrouchingState>()),
            new Transition(() => player.dashing, gameObject.GetComponent<DashState>())
        };
    }

    /**
     * This state is only used as default state to fall back upon when not doing any movement actions.
     */
    public override void OnEnable()
    {
        Debug.Log("Start idling!");
    }

    public override void OnDisable()
    {
        Debug.Log("Stopped idling!");
    }
}
