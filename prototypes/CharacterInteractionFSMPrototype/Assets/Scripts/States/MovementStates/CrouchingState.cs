using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrouchingState : MovementState
{
    private float savedSpeed;

    /**
    * Set transitions from current state to given state on gameobject.
    */
    public void Start()
    {
        transitions = new List<Transition>
        {
            new Transition(() => !player.crouching, gameObject.GetComponent<IdleState>()),
            new Transition(() => !player.crouching && player.moving, gameObject.GetComponent<MovingState>()),
            new Transition(() => player.jumping, gameObject.GetComponent<JumpingState>()),
            new Transition(() => player.dashing, gameObject.GetComponent<DashState>())
        };
    }


    /**
     * Get and set current player speed multiplier.
     * 
     * Get and set new speed for current state.
     */
    public override void OnEnable()
    {
        Debug.Log("Start crouching!");
        savedSpeed = player.speed;
        player.speed = player.crouchSpeed;
    }

    /**
     * When leaving state set player speed back to original multiplier.
     */
    public override void OnDisable()
    {
        Debug.Log("Stopped idling!");
        player.speed = savedSpeed;
    }

    /**
    * Call move every physics update to move player in 8 main directions.
    */
    public override void FixedUpdate()
    {
        base.FixedUpdate();

        player.Move();
    }
}
