using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class OptionTwoPositionTrack : MonoBehaviour
{
    [FormerlySerializedAs("mPlayer")] [SerializeField] private Transform player;
    [FormerlySerializedAs("mPositionOffset")] [SerializeField] private Vector3 positionOffset = new Vector3(0.0f, 2.0f, -2.5f);
    [FormerlySerializedAs("mAngleOffset")] [SerializeField] private Vector3 angleOffset = new Vector3(0.0f, 0.0f, 0.0f);
    [FormerlySerializedAs("mDamping")] [SerializeField] private float damping = 1.0f;

    void LateUpdate()
    {
        CameraMoveFollow();
    }

    private void CameraMoveFollow()
    {
        Quaternion initialRotation = Quaternion.Euler(angleOffset);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, initialRotation, damping * Time.deltaTime);

        Vector3 forward = transform.rotation * Vector3.forward;
        Vector3 right = transform.rotation * Vector3.right;
        Vector3 up = transform.rotation * Vector3.up;

        Vector3 targetPos = player.position;
        Vector3 desiredPosition = targetPos + forward * positionOffset.z + right * positionOffset.x + up * positionOffset.y;

        Vector3 position = Vector3.Lerp(transform.position, desiredPosition, Time.deltaTime * damping);
        transform.position = position;
    }
}