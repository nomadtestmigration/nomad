using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;

public class Movement : MonoBehaviour
{
    [SerializeField] private float forwardSpeed = 10f;
    [SerializeField] private float strafeSpeed = 3f;
    [SerializeField] private float rotateSpeed = 3f;
    [SerializeField] private bool cameraControlled = false;
    [SerializeField] private Transform cameraOrbitPoint;
    [SerializeField] private bool rotateOnWalk = false;

    private Vector2 movement;

    private float rotation;

    private void Update()
    {
        transform.Translate(movement.x * strafeSpeed * Time.deltaTime, 0, movement.y * forwardSpeed * Time.deltaTime);
        if (!cameraControlled)
        {
            transform.Rotate(transform.up, rotation * rotateSpeed * Time.deltaTime);
        }
        else
        {
            if (movement.y > 0 || !rotateOnWalk)
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, cameraOrbitPoint.rotation, Time.deltaTime * rotateSpeed);
            }
        }
    }

    void OnMove(InputValue value)
    {
        movement = value.Get<Vector2>();
    }

    void OnRotate(InputValue value)
    {
        rotation = value.Get<float>();
    }
}