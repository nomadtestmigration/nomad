using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class OptionFourPositionTrackAndRotationOnMovement : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private Transform orbitPoint;
    [SerializeField] private float rotateSpeed;
    private Vector2 movement;

    private void Awake()
    {
        Cursor.visible = false;
    }

    void LateUpdate()
    {
        orbitPoint.position = target.position;

        if (movement != Vector2.zero)
        {
            orbitPoint.forward = Vector3.Lerp(orbitPoint.forward, target.TransformDirection(new Vector3(movement.x, 0, movement.y)), Time.deltaTime * rotateSpeed);
        }
    }

    void OnLook(InputValue value)
    {
        if (movement == Vector2.zero) orbitPoint.Rotate(Vector3.up, value.Get<Vector2>().x);
    }

    void OnMove(InputValue value)
    {
        movement = value.Get<Vector2>();
    }
}