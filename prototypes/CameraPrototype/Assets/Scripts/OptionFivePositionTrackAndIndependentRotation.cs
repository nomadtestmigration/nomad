using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;

public class OptionFivePositionTrackAndIndependentRotation : MonoBehaviour
{
    [FormerlySerializedAs("mPlayer")] [SerializeField] private Transform player;
    [FormerlySerializedAs("mPositionOffset")] [SerializeField] private Vector3 positionOffset = new Vector3(0.0f, 2.0f, -2.5f);
    [FormerlySerializedAs("mAngleOffset")] [SerializeField] private Vector3 angleOffset = new Vector3(0.0f, 0.0f, 0.0f);
    [FormerlySerializedAs("mDamping")] [SerializeField] private float damping = 1.0f;
    [FormerlySerializedAs("mMinPitch")] [SerializeField] private float minPitch = -30.0f;
    [FormerlySerializedAs("mMaxPitch")] [SerializeField] private float maxPitch = 30.0f;
    [FormerlySerializedAs("mRotationSpeed")] [SerializeField] private float rotationSpeed = 5.0f;
    [SerializeField] private float angleX = 0.0f;
    [SerializeField] private Vector2 mouseMovement;

    void LateUpdate()
    {
        FollowIndependentRotation();
    }

    void FollowIndependentRotation()
    {
        Quaternion initialRotation = Quaternion.Euler(angleOffset);

        Vector3 eu = transform.rotation.eulerAngles;

        angleX -= mouseMovement.y * rotationSpeed;

        angleX = Mathf.Clamp(angleX, minPitch, maxPitch);

        eu.y += mouseMovement.x * rotationSpeed;
        Quaternion newRot = Quaternion.Euler(angleX, eu.y, 0.0f) * initialRotation;

        transform.rotation = newRot;

        Vector3 forward = transform.rotation * Vector3.forward;
        Vector3 right = transform.rotation * Vector3.right;
        Vector3 up = transform.rotation * Vector3.up;

        Vector3 targetPos = player.position;
        Vector3 desiredPosition = targetPos + forward * positionOffset.z + right * positionOffset.x + up * positionOffset.y;

        Vector3 position = Vector3.Lerp(transform.position, desiredPosition, Time.deltaTime * damping);
        transform.position = position;
    }

    void OnLook(InputValue value)
    {
        mouseMovement = value.Get<Vector2>();
    }
}