using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class OptionEightTopDownPositionTrack : MonoBehaviour
{
    [FormerlySerializedAs("mPlayer")] [SerializeField] private Transform player;
    [FormerlySerializedAs("mPositionOffset")] [SerializeField] private Vector3 positionOffset = new Vector3(0.0f, 2.0f, -2.5f);
    [FormerlySerializedAs("mDamping")] [SerializeField] private float damping = 1.0f;

    void LateUpdate()
    {
        CameraMoveTopDown();
    }

    void CameraMoveTopDown()
    {
        Vector3 targetPos = player.position;
        targetPos.y += positionOffset.y;
        Vector3 position = Vector3.Lerp(transform.position, targetPos, Time.deltaTime * damping);
        transform.position = position;
        transform.rotation = Quaternion.Euler(90.0f, 0.0f, 0.0f);
    }
}