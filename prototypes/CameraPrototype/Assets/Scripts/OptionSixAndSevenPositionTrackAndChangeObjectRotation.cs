using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class OptionSixAndSevenPositionTrackAndChangeObjectRotation : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private Transform orbitPoint;

    private void Awake()
    {
        Cursor.visible = false;
    }

    void Update()
    {
        orbitPoint.position = target.position;
    }

    void OnLook(InputValue value)
    {
        orbitPoint.Rotate(Vector3.up, value.Get<Vector2>().x);
    }
}