using System;
using BehaviorDesigner.Runtime;
using UnityEngine;

public class HealthController : MonoBehaviour
{
    public int health = 100;

    private Behavior behavior;
    private new Renderer renderer;

    private Color color;

    void Awake()
    {
        renderer = GetComponent<Renderer>();
        behavior = GetComponent<Behavior>();
        color = renderer.material.color;
    }

    /// <summary>
    /// Subtracts the given <paramref name="amount"/> of health from the current <see cref="health"/>.
    /// </summary>
    /// <param name="amount">The amount of damage that should be taken.</param>
    public void Damage(int amount)
    {
        health -= amount;
       
        renderer.material.color = Color.black;

        if (behavior != null) { 
            behavior.SetVariableValue("Health", health); 
        }

        Debug.Log($"Hit: {gameObject.name}");

        Invoke(nameof(ResetColor), 1f);
    }

    /// <summary>
    /// Adds the given <paramref name="amount"/> of health to the current <see cref="health"/>.
    /// </summary>
    /// <param name="amount">The amount that should be healed for.</param>
    public void Heal(int amount)
    {
        health += amount;

        if (behavior != null)
        {
            behavior.SetVariableValue("Health", health);
        }
    }

    private void ResetColor()
    {
        renderer.material.color = color;
    }
}
