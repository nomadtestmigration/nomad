using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    public float movementSpeed = 5f;
    public int attackDamage = 50;
    public HealthController enemyHealth;

    private Vector3 movement;

    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        rb.MovePosition(rb.transform.position + movement * movementSpeed * Time.fixedDeltaTime);
    }

    void OnMove(InputValue inputValue)
    {
        Vector2 input = inputValue.Get<Vector2>();
        movement = new Vector3(input.x, 0f, input.y);
    }

    void OnAttack()
    {
        enemyHealth.Damage(attackDamage);
    }
}
