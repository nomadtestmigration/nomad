using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

/// <summary>
/// Seek action for Behavior Designer.
/// </summary>
[TaskName("Seek")]
[TaskDescription("Run towards the target GameObject.")]
public class SeekAction : BaseNavigationAction
{
    public SharedTransform target;

    /// <summary>
    /// (Re)starts the NavMeshAgent. Also sets its initial destination.
    /// </summary>
    public override void OnStart()
    {
        base.OnStart();

        navAgent.SetDestination(target.Value.position);
    }

    /// <summary>
    /// Executes logic to determine if the Task has Failed, Succeeded, or is still Running.
    /// Also updates the destination of the NavMeshAgent if it is outdated.
    /// </summary>
    /// <returns>Failure if target is null; 
    /// Success if path is not pending AND the remaining distance is lesser than or equal to the stopping distance; 
    /// otherwise Running.
    /// </returns>
    public override TaskStatus OnUpdate()
    {
        if (target == null)
        {
            return TaskStatus.Failure;
        }
        if (!navAgent.pathPending && navAgent.remainingDistance <= navAgent.stoppingDistance)
        {
            return TaskStatus.Success;
        }
        if (navAgent.destination != target.Value.position)
        {
            navAgent.SetDestination(target.Value.position);
        }

        return TaskStatus.Running;
    }
}
