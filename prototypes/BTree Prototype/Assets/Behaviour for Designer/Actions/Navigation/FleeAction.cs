using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

/// <summary>
/// Flee action for Behavior Designer.
/// </summary>
[TaskName("Flee")]
[TaskDescription("Run away from the target GameObject.")]
public class FleeAction : BaseNavigationAction
{
    public SharedTransform target;
    public SharedFloat minDistance;

    /// <summary>
    /// (Re)starts the NavMeshAgent. Also sets its initial destination.
    /// </summary>
    public override void OnStart()
    {
        navAgent.isStopped = false;
        
        navAgent.SetDestination(GetNewPosition());
    }

    /// <summary>
    /// Executes logic to determine if the Task has Failed, Succeeded, or is still Running.
    /// Also updates the destination of the NavMeshAgent if it is outdated.
    /// </summary>
    /// <returns>Failure if target is null; 
    /// Success if path is not pending AND the remaining distance is lesser than or equal to the stopping distance; 
    /// otherwise Running.
    /// </returns>
    public override TaskStatus OnUpdate()
    {
        if (target == null)
        { 
            return TaskStatus.Failure; 
        }
        if (!navAgent.pathPending && navAgent.remainingDistance <= navAgent.stoppingDistance)
        { 
            return TaskStatus.Success; 
        }
        if (navAgent.destination != target.Value.position)
        { 
            navAgent.SetDestination(GetNewPosition()); 
        }

        return TaskStatus.Running;
    }

    private Vector3 GetNewPosition()
    {
        Vector3 dirFromPlayer = transform.position - target.Value.position;
        Vector3 movement = dirFromPlayer.normalized * minDistance.Value;

        return transform.position + movement;
    }
}
