using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

/// <summary>
/// Wander action for Behavior Designer.
/// </summary>
[TaskName("Wander")]
[TaskDescription("Walk around without purpose within the bounds of the specified Collider.")]
public class WanderAction : BaseNavigationAction
{
    public SharedCollider area;

    private Vector3 target;

    /// <summary>
    /// (Re)starts the NavMeshAgent. Also sets its initial destination.
    /// </summary>
    public override void OnStart()
    {
        navAgent.isStopped = false;

        SetNewRandomTarget();

        navAgent.SetDestination(target);
    }

    /// <summary>
    /// Executes logic to determine if the Task has Failed, Succeeded, or is still Running.
    /// </summary>
    /// <returns>Failure if target is null; 
    /// Success if path is not pending AND the remaining distance is lesser than or equal to the stopping distance; 
    /// otherwise Running.
    /// </returns>
    public override TaskStatus OnUpdate()
    {
        if (target == Vector3.zero)
        {
            return TaskStatus.Failure;
        }
        if (!navAgent.pathPending && navAgent.remainingDistance <= navAgent.stoppingDistance)
        { 
            return TaskStatus.Success; 
        }

        return TaskStatus.Running;
    }

    private void SetNewRandomTarget()
    {
        target = GetRandomPositionInArea();
    }

    private Vector3 GetRandomPositionInArea()
    {
        if(area.Value != null)
        {
            Collider area = this.area.Value;
            Bounds b = area.bounds;

            Vector3 randPos = new(
                Random.Range(b.min.x, b.max.x), 
                Random.Range(b.min.y, b.max.y), 
                Random.Range(b.min.z, b.max.z)
            );

            return area.ClosestPoint(randPos);
        }

        return Vector3.zero;
    }
}
