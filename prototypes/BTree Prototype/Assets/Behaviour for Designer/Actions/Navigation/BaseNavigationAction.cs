﻿using BehaviorDesigner.Runtime.Tasks;
using UnityEngine.AI;

[TaskCategory("Nomad/Navigation")]
abstract public class BaseNavigationAction : Action
{
    protected NavMeshAgent navAgent;

    /// <summary>
    /// Gets the <see cref="NavMeshAgent"/> 
    /// from the <see cref="GameObject"/> that is performing this <see cref="Action"/>, 
    /// and assigns it to <see cref="navAgent"/>.
    /// </summary>
    public override void OnAwake()
    {
        navAgent = GetComponent<NavMeshAgent>();
    }

    /// <summary>
    /// (Re)starts the NavMeshAgent.
    /// </summary>
    public override void OnStart()
    {
        navAgent.isStopped = false;
    }

    /// <summary>
    /// Calls <see cref="StopAgent"/>.
    /// </summary>
    public override void OnEnd()
    {
        StopAgent();
    }

    /// <summary>
    /// Calls <see cref="StopAgent"/>.
    /// </summary>
    public override void OnConditionalAbort()
    {
        StopAgent();
    }

    /// <summary>
    /// Stops the NavMeshAgent.
    /// </summary>
    protected void StopAgent()
    {
        navAgent.isStopped = true;
    }
}

