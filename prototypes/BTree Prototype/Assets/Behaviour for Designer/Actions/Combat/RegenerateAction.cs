using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

[TaskCategory("Nomad/Combat")]
public class RegenerateAction : Action
{
    public SharedInt health;

    public SharedGameObject healthTarget;

    private HealthController healthController;

    /// <summary>
    /// Gets the <see cref="HealthController"/> 
    /// from the <see cref="healthTarget"/>, 
    /// and assigns it to <see cref="healthController"/>.
    /// </summary>
    public override void OnAwake()
    {
        healthController = healthTarget.Value.GetComponent<HealthController>();
    }

    /// <summary>
    /// Heals the target <see cref="HealthController"/> assigned to <see cref="healthController"/> for the amount set int <see cref="health"/>.
    /// </summary>
    public override void OnStart()
    {
        healthController.Heal(health?.Value ?? 0);
    }

    /// <summary>
    /// Because the regenerate action is instantaneous, and all logic is executed in the <see cref="OnStart"/> method, there is no need for logic here.
    /// </summary>
    /// <returns>Success</returns>
    public override TaskStatus OnUpdate()
    {
        return TaskStatus.Success;
    }
}
