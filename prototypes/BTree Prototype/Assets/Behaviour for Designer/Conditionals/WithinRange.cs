using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

[TaskCategory("Nomad")]
public class WithinRange : Conditional
{
    public SharedFloat range = 5;

    public SharedTransform target;

    /// <summary>
    /// Checks if the specified target is within range of the <see cref="GameObject"/> performing this <see cref="Action"/>.
    /// </summary>
    /// <returns>Failure if target is null; 
    /// Success if the target is within range; 
    /// otherwise Failure.
    /// </returns>
    public override TaskStatus OnUpdate()
    {
        if (target.Value == null) { 
            return TaskStatus.Failure; 
        }

        if (Vector3.Distance(transform.position, target.Value.position) <= (range?.Value ?? 0))
        {
            return TaskStatus.Success;
        }

        return TaskStatus.Failure;
    }
}
