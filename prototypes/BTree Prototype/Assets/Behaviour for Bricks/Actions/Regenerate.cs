using BBUnity.Actions;
using Pada1.BBCore;
using Pada1.BBCore.Tasks;

[Action("EnemyActions/Regenerate")]
[Help("Heal for a certain amount")]
public class Regenerate : GOAction
{
    [InParam("health")]
    public int health;

    [InParam("healthTarget")]
    public HealthController healthTarget;

    /// <summary>
    /// Heals the target <see cref="HealthController"/> assigned to <see cref="healthTarget"/> for the amount set int <see cref="health"/>.
    /// </summary>
    public override void OnStart()
    {
        healthTarget.Heal(health);
    }

    /// <summary>
    /// Because the regenerate action is instantaneous, and all logic is executed in the <see cref="OnStart"/> method, there is no need for logic here.
    /// </summary>
    /// <returns>Success</returns>
    public override TaskStatus OnUpdate()
    {
        return TaskStatus.COMPLETED;
    }
}
