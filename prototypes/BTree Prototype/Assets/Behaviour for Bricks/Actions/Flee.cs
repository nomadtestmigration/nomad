using BBUnity.Actions;
using Pada1.BBCore;
using Pada1.BBCore.Tasks;
using UnityEngine;
using UnityEngine.AI;

[Action("EnemyActions/Flee")]
[Help("Run away from target")]
public class Flee : GOAction
{
    [InParam("target")]
    public GameObject target;

    [InParam("runDistance")]
    public float runDistance;

    private NavMeshAgent navAgent;

    /// <summary>
    /// (Re)starts the NavMeshAgent. Also sets its initial destination.
    /// </summary>
    public override void OnStart()
    {
        navAgent = gameObject.GetComponent<NavMeshAgent>();

        Vector3 dirToPlayer = gameObject.transform.position - target.transform.position;

        Vector3 newPos = gameObject.transform.position + dirToPlayer.normalized * runDistance;

        navAgent.SetDestination(newPos);

        navAgent.isStopped = false;
    }

    /// <summary>
    /// Executes logic to determine if the Task has Failed, Succeeded, or is still Running.
    /// Also updates the destination of the NavMeshAgent if it is outdated.
    /// </summary>
    /// <returns>Failure if target is null; 
    /// Success if path is not pending AND the remaining distance is lesser than or equal to the stopping distance; 
    /// otherwise Running.
    /// </returns>
    public override TaskStatus OnUpdate()
    {
        if (target == null)
        {
            return TaskStatus.FAILED;
        }
        if (!navAgent.pathPending && navAgent.remainingDistance <= navAgent.stoppingDistance)
        {
            return TaskStatus.COMPLETED;
        }
            
        return TaskStatus.RUNNING;
    }

    /// <summary>
    /// Stops the NavMeshAgent.
    /// </summary>
    public override void OnAbort()
    {
        if(navAgent != null)
        {
            navAgent.isStopped = true;
        }
    }
}
