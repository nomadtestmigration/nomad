using BBUnity.Conditions;
using Pada1.BBCore;

[Condition("Enemy/CheckHealth")]
[Help("Compares the targets health with a given health level")]
public class CheckHealth : GOCondition
{
    [InParam("healthTarget")]
    public HealthController healthTarget;

    [InParam("comparator")]
    public Comparator comparator;

    [InParam("healthLevel")]
    public int healthLevel;

    /// <summary>
    /// Compares the health of the <see cref="healthTarget"/> target with the <see cref="healthLevel"/> based on the specified <see cref="Comparator"/>.
    /// </summary>
    /// <returns>True if the comparison results in true; otherwise False.</returns>
    /// <exception cref="System.NotImplementedException"></exception>
    public override bool Check()
    {
        return comparator switch
        {
            Comparator.LESSER => healthTarget.health < healthLevel,
            Comparator.GREATER => healthTarget.health > healthLevel,
            Comparator.EQUAL => healthTarget.health == healthLevel,
            Comparator.LESSEROREQUAL => healthTarget.health <= healthLevel,
            Comparator.GREATEREROREQUAL => healthTarget.health >= healthLevel,
            _ => throw new System.NotImplementedException()
        };
    }
}
