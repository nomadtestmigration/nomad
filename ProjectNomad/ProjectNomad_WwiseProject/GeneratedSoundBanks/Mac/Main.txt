Event	ID	Name			Wwise Object Path	Notes
	21198552	Play_new_footsteps			\Default Work Unit\Character\CC\Walking\Play_new_footsteps	
	149375838	Stop_Tension			\Default Work Unit\Music\Stop_Tension	
	261900158	Play_Presure_plate_compress			\Default Work Unit\Presure_plate\Play_Presure_plate_compress	
	837991554	Stop_new			\Default Work Unit\Music\Stop_new	
	844699242	Play_robotBomb_Explosion_v1			\Default Work Unit\Items\Bomb\Play_robotBomb_Explosion_v1	
	1115286468	Play_Menu_Click_01			\Default Work Unit\UI\Play_Menu_Click_01	
	1357778998	Play_Menu_Click			\Default Work Unit\UI\Play_Menu_Click	
	1674911419	Play_EnemyFleshHit			\Default Work Unit\Character\Ebo\Attack\Enemy hit\Play_EnemyFleshHit	
	1932796752	Play_Noise			\Default Work Unit\Ambient\Play_Noise	
	2039851942	Play_Solving_a_Puzzle__Sief_Arrangement__1			\Default Work Unit\Music\Play_Solving_a_Puzzle__Sief_Arrangement__1	
	2296124049	Play_JumpBegin			\Default Work Unit\Character\Ebo\Jump\Play_JumpBegin	
	2323405115	Play_Landing			\Default Work Unit\Character\Ebo\Landing\Play_Landing	
	2388497063	Play_Lonely			\Default Work Unit\Music\Play_Lonely	
	2481039929	Play_switchSoundCC			\Default Work Unit\Character\CC\Noises\Play_switchSoundCC	
	2557017376	Play_new			\Default Work Unit\Music\Play_new	
	2662844813	Play_TempleBunker_2			\Default Work Unit\Music\Play_TempleBunker_2	
	2921490336	Play_Walking_CC			\Default Work Unit\Character\CC\Walking\Play_Walking_CC	
	3021116958	Stop_Noise			\Default Work Unit\Ambient\Stop_Noise	
	3330584343	Play_Presure_plate_decompress			\Default Work Unit\Presure_plate\Play_Presure_plate_decompress	
	3359251851	Stop_TempleBunker_2			\Default Work Unit\Music\Stop_TempleBunker_2	
	3431095223	Play_Bunker_3_1			\Default Work Unit\Music\Play_Bunker_3_1	
	3625097539	Play_CC_Jump			\Default Work Unit\Character\CC\JumpBegin\Play_CC_Jump	
	3766847488	Play_Weapon_Swing_Staf			\Default Work Unit\Character\Ebo\Attack\Swing\Play_Weapon_Swing_Staf	
	3847695549	Play_Solving_a_puzzle			\Default Work Unit\Music\Play_Solving_a_puzzle	
	3854155799	Play_Footsteps			\Default Work Unit\Character\Ebo\Walking\Play_Footsteps	
	3974977655	Play_airlockDoor_Close1			\Default Work Unit\Doors\Play_airlockDoor_Close1	
	4114669316	Play_Tension			\Default Work Unit\Music\Play_Tension	

Switch Group	ID	Name			Wwise Object Path	Notes
	2385628198	Footsteps			\Default Work Unit\Footsteps	

Switch	ID	Name	Switch Group			Notes
	803837735	Sand	Footsteps			
	1216965916	Stone	Footsteps			
	2058049674	Wood	Footsteps			
	2195636714	Dirt	Footsteps			
	2654748154	Water	Footsteps			
	4248645337	Grass	Footsteps			

Effect plug-ins	ID	Name	Type				Notes
	34733224	Wwise Parametric EQ (Custom)	Wwise Parametric EQ			
	73874663	Wwise Parametric EQ (Custom)	Wwise Parametric EQ			
	76295689	Wwise RoomVerb (Custom)	Wwise RoomVerb			
	124972655	Wwise Parametric EQ (Custom)	Wwise Parametric EQ			
	157246972	Wwise Parametric EQ (Custom)	Wwise Parametric EQ			
	240966419	Wwise Parametric EQ (Custom)	Wwise Parametric EQ			
	401825909	Wwise Parametric EQ (Custom)	Wwise Parametric EQ			
	460388539	Wwise Parametric EQ (Custom)	Wwise Parametric EQ			
	503329112	Wwise RoomVerb (Custom)	Wwise RoomVerb			
	509573914	Wwise RoomVerb (Custom)	Wwise RoomVerb			
	560596120	Wwise Compressor (Custom)	Wwise Compressor			
	567223112	Wwise Parametric EQ (Custom)	Wwise Parametric EQ			
	642716492	Wwise Compressor (Custom)	Wwise Compressor			
	652878601	Wwise Parametric EQ (Custom)	Wwise Parametric EQ			
	654504333	Wwise Parametric EQ (Custom)	Wwise Parametric EQ			
	658452323	Wwise Parametric EQ (Custom)	Wwise Parametric EQ			
	692102117	Wwise Parametric EQ (Custom)	Wwise Parametric EQ			
	716171177	Wwise Parametric EQ (Custom)	Wwise Parametric EQ			
	762122439	Wwise Parametric EQ (Custom)	Wwise Parametric EQ			
	775480168	Wwise Compressor (Custom)	Wwise Compressor			
	825664871	Wwise Parametric EQ (Custom)	Wwise Parametric EQ			
	847780169	Wwise Parametric EQ (Custom)	Wwise Parametric EQ			
	884750585	Wwise Parametric EQ (Custom)	Wwise Parametric EQ			
	909492228	Wwise Parametric EQ (Custom)	Wwise Parametric EQ			
	919160211	Wwise Parametric EQ (Custom)	Wwise Parametric EQ			
	929153803	Wwise Compressor (Custom)	Wwise Compressor			
	986953865	Wwise Parametric EQ (Custom)	Wwise Parametric EQ			
	998178512	Wwise RoomVerb (Custom)	Wwise RoomVerb			
	1013072521	Wwise Parametric EQ (Custom)	Wwise Parametric EQ			
	1042887375	Wwise Parametric EQ (Custom)	Wwise Parametric EQ			
	1047792488	Wwise Compressor (Custom)	Wwise Compressor			

In Memory Audio	ID	Name	Audio source file		Wwise Object Path	Notes	Data Size
	4612625	robotBomb_Explosion_v1	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\robotBomb_Explosion_v1_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\Items\Bomb\robotBomb_Explosion_v1		2541564
	21314822	Noise	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\noiseeeee_D79FB2B7.wem		\Actor-Mixer Hierarchy\Default Work Unit\Ambient\Noise		20659700
	43144290	Ebo_footstep_2	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Ebo_footstep_2_571D96DC.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\Footsteps\Footsteps stone\Ebo_footstep_2		43168
	44298515	CC_1	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\CC_1_852792D8.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\CC\Noises\switchSoundCC\CC_1		93300
	46356551	Ebo_footstep_16	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Ebo_footstep_16_2D94B470.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\Footsteps\Footsteps stone\Ebo_footstep_16		34264
	52290557	Ebo_footstep_8	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Ebo_footstep_8_D54179A2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\Footsteps\Footsteps stone\Ebo_footstep_8		47148
	53791480	zandloop_03	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\zandloop_03_42021BD2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\landing\step2\zandloop_03		76850
	69516150	PM_SDGS_186 Footstep Step Dry Grass Shrubs Pine Needles Meadow	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\PMSFX - STEPS Dry Grass & Shrubs\PM_SDGS_186 Footstep Step Dry Grass Shrubs Pine Needles Meadow _A98966F8.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\Footsteps\footsteps Dirt\PM_SDGS_186 Footstep Step Dry Grass Shrubs Pine Needles Meadow		25860
	80412620	water step4	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Water\water step4_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\landing\step1\water step4		51292
	97583820	CC step5	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\CC step5_306C0D97.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\CC\Walking\stone\CC step5		66176
	106327764	Ebo_jump4	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Ebo_jump4_61211436.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\landing\step1\StoneLanding1\Ebo_jump4		58184
	110171407	Ebo_footstep_3	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Ebo_footstep_3_646633FD.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\Footsteps\Footsteps stone\Ebo_footstep_3		46916
	121198613	lopen op hout_05	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\lopen op hout_05_A60FF654.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\landing\step2\lopen op hout_05		33676
	137175263	Ebo_footstep_12	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Ebo_footstep_12_2C1FFDBB.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\Footsteps\Footsteps stone\Ebo_footstep_12		44572
	145836393	Presure plate2	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Presure plate2_4A9BFBD9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Plates\Presure plate2		119614
	147472232	PM_SDGS_113 Footstep Step Dry Grass Shrubs Pine Needles Meadow	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\PMSFX - STEPS Dry Grass & Shrubs\PM_SDGS_113 Footstep Step Dry Grass Shrubs Pine Needles Meadow _49B139CB.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\Footsteps\footsteps Dirt\PM_SDGS_113 Footstep Step Dry Grass Shrubs Pine Needles Meadow		36602
	155348267	new	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Musical Concept_D79FB2B7.wem		\Actor-Mixer Hierarchy\Default Work Unit\Music\new		80908908
	174656164	Tension	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Tension_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\Music\Tension		6513660
	178899459	Presure plate1	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Presure plate1_D155986B.wem		\Actor-Mixer Hierarchy\Default Work Unit\Plates\Presure plate1		99734
	180394500	Menu Click	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\UI_Click_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\Items\UI\Menu Click		126852
	187179988	Ebo_footstep_15	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Ebo_footstep_15_DF006C64.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\Footsteps\Footsteps stone\Ebo_footstep_15		39184
	196157112	Ebo_footstep_10	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Ebo_footstep_10_2EEC90B4.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\Footsteps\Footsteps stone\Ebo_footstep_10		47152
	196686994	Solving a Puzzle (Sief Arrangement)_1	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Solving a Puzzle (Sief Arrangement)_1_D79FB2B7.wem		\Actor-Mixer Hierarchy\Default Work Unit\Music\Solving a Puzzle (Sief Arrangement)_1		18365436
	199519636	Ebo_footstep_4	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Ebo_footstep_4_D72E1E92.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\Footsteps\Footsteps stone\Ebo_footstep_4		46680
	214952857	Ebo_footstep_5	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Ebo_footstep_5_5ABE765F.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\Footsteps\Footsteps stone\Ebo_footstep_5		40592
	230689832	Ebo Jump3	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Ebo Jump3_A9009F1B.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\JumpBegin\Ebo Jump3		38320
	253165710	Ebo Jump7	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Ebo Jump7_F51DC5E1.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\JumpBegin\Ebo Jump7		37256
	282891463	grasloop_02	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\grasloop_02_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\landing\step1\grasloop_02		45628
	289688904	PM_SDGS_186 Footstep Step Dry Grass Shrubs Pine Needles Meadow	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\PMSFX - STEPS Dry Grass & Shrubs\PM_SDGS_186 Footstep Step Dry Grass Shrubs Pine Needles Meadow _D0E69551.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\landing\step2\PM_SDGS_186 Footstep Step Dry Grass Shrubs Pine Needles Meadow		27836
	291313456	Ebo_footstep_14	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Ebo_footstep_14_F9137FA2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\Footsteps\Footsteps stone\Ebo_footstep_14		39184
	301709278	water step 1	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Water\water step 1_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\Footsteps\Footstep Water\water step 1		48732
	318901007	ccstep3	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\cc_ 4-robot walk kist_04_B76358BA.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\CC\Walking\new footsteps\ccstep3		113856
	334383187	zandloop_03	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\zandloop_03_B11C9B57.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\Footsteps\Footsteps sand\zandloop_03		85432
	336037490	EnemyFleshHit	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\PM_LB_SOURCE_FLESH_DROPS_ORANGES_MKH8060_6_A2E1E2F1.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\Attack\Enemy Hit\EnemyFleshHit		49658
	338882032	Ebo_jump2	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Ebo_jump2_06EC6D0E.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\landing\step1\StoneLanding1\Ebo_jump2		71184
	355703800	zandloop_01	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\zandloop_01_ED2CD4E5.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\landing\step1\zandloop_01		67480
	356364866	ccstep6	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\cc_ 7-robot walk kist_04_DDBAEC8D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\CC\Walking\new footsteps\ccstep6		132944
	356720153	Ebo_jump5	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Ebo_jump5_7F0DF206.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\landing\step1\StoneLanding1\Ebo_jump5		73860
	379206311	Ebo_footstep_13	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Ebo_footstep_13_1A150D6C.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\Footsteps\Footsteps stone\Ebo_footstep_13		44572
	394555763	Ebo Jump6	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Ebo Jump6_F976D542.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\JumpBegin\Ebo Jump6		42060
	409616183	Solving-a-puzzle	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Solving-a-puzzle_D79FB2B7.wem		\Actor-Mixer Hierarchy\Default Work Unit\Music\Solving-a-puzzle		16865344
	434089419	lopen op hout_03	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\lopen op hout_03_A222ED40.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\Footsteps\Footsteps Wood\lopen op hout_03		37542
	434593709	Ebo_jump6	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Ebo_jump6_DB16C191.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\landing\step1\StoneLanding1\Ebo_jump6		66216
	461181637	TempleBunker_2	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\TempleBunker_2_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\Music\TempleBunker_2		17273340
	472837230	PM_SDGS_113 Footstep Step Dry Grass Shrubs Pine Needles Meadow	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\PMSFX - STEPS Dry Grass & Shrubs\PM_SDGS_113 Footstep Step Dry Grass Shrubs Pine Needles Meadow _7D051DF0.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\landing\step1\PM_SDGS_113 Footstep Step Dry Grass Shrubs Pine Needles Meadow		34636
	481610931	Bunker 3_1	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Bunker 3_1_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\Music\Bunker 3_1		23056380
	515794795	water step3	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Water\water step3_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\Footsteps\Footstep Water\water step3		47944
	521254169	CC step3	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\CC step3_39B973EB.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\CC\Walking\stone\CC step3		16828
	523502145	Ebo_footstep_6	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Ebo_footstep_6_055369DF.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\Footsteps\Footsteps stone\Ebo_footstep_6		43872
	619942140	Ebo_footstep_7	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Ebo_footstep_7_98DF6402.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\Footsteps\Footsteps stone\Ebo_footstep_7		49260
	644335690	water step5	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Water\water step5_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\Footsteps\Footstep Water\water step5		61536
	660242240	ccstep7	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\cc_ 8-robot walk kist_04_EA77E3E7.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\CC\Walking\new footsteps\ccstep7		119360
	668267127	CC step4	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\CC step4_8DEDB211.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\CC\Walking\stone\CC step4		66500
	671785146	Lonely	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Lonely_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\Music\Lonely		10241532
	684550957	Ebo_footstep_1	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Ebo_footstep_1_EC476F17.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\Footsteps\Footsteps stone\Ebo_footstep_1		42232
	720102748	Jump	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\FF_ES_foley_arrow_swoosh_white_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\CC\JumpBegin\Jump		66948
	730733301	Ebo_jump1	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Ebo_jump1_3083F932.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\landing\step1\StoneLanding1\Ebo_jump1		98336
	736348735	lopen op hout_02	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\lopen op hout_02_15E0AE8D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\Footsteps\Footsteps Wood\lopen op hout_02		23526
	737939361	Ebo Jump8	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Ebo Jump8_40084D10.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\JumpBegin\Ebo Jump8		42240
	744747810	ccstep5	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\cc_ 6-robot walk kist_04_60757848.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\CC\Walking\new footsteps\ccstep5		145792
	785779869	Ebo Jump5	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Ebo Jump5_FA8037A2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\JumpBegin\Ebo Jump5		38320
	789768650	Ebo Jump4	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Ebo Jump4_F87F0863.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\JumpBegin\Ebo Jump4		44904
	846495192	Ebo Jump2	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Ebo Jump2_0CE02F74.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\JumpBegin\Ebo Jump2		39568
	850359463	lopen op hout_04	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\lopen op hout_04_4346FB29.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\landing\step1\lopen op hout_04		43808
	873693094	zandloop_02	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\zandloop_02_874A2522.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\Footsteps\Footsteps sand\zandloop_02		70544
	876976061	PM_SDGS_14 Footstep Step Dry Grass Shrubs Pine Needles Meadow	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\PMSFX - STEPS Dry Grass & Shrubs\PM_SDGS_14 Footstep Step Dry Grass Shrubs Pine Needles Meadow _D484FDCA.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\Footsteps\footsteps Dirt\PM_SDGS_14 Footstep Step Dry Grass Shrubs Pine Needles Meadow		51710
	882275102	lopen op hout_01	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\lopen op hout_01_B7F69DB6.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\Footsteps\Footsteps Wood\lopen op hout_01		29572
	892517920	Ebo_footstep_9	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Ebo_footstep_9_E8147502.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\Footsteps\Footsteps stone\Ebo_footstep_9		46212
	892912427	grasloop_01	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\grasloop_01_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\Footsteps\Footsteps Grass\grasloop_01		44488
	913057826	grasloop_03	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\grasloop_03_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\landing\step2\grasloop_03		50184
	932297726	CC step 2	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\CC step 2_9ECAA427.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\CC\Walking\stone\CC step 2		55920
	954253419	ccstep1	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\cc_ 2-robot walk kist_04_0F3E4239.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\CC\Walking\new footsteps\ccstep1		135144
	960585968	water step2	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Water\water step2_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\landing\step2\water step2		36712
	974450902	airlockDoor_Close1	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\airlockDoor_Close1_829D9573.wem		\Actor-Mixer Hierarchy\Default Work Unit\Doors\airlockDoor_Close1		1297032
	993925912	CC step 1	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\CC step 1_B5447990.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\CC\Walking\stone\CC step 1		58756
	994809604	ccstep4	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\cc_ 5-robot walk kist_04_CF9EF0BA.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\CC\Walking\new footsteps\ccstep4		130740
	1011805965	Ebo Jump1	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Ebo Jump1_B5E9A882.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\JumpBegin\Ebo Jump1		40848
	1022318847	Weapon Swing Staf	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\MeleeSwingsPack_96khz_Stereo_HighSwings15_B5622231.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\Attack\Weapon Swings\Weapon Swing Staf		55984
	1044945293	ccstep2	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\cc_ 3-robot walk kist_04_CC281265.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\CC\Walking\new footsteps\ccstep2		107248
	1051617136	Ebo_jump3	E:\nomad\ProjectNomad\ProjectNomad_WwiseProject\.cache\Mac\SFX\Ebo_jump3_E24E3C51.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character\Ebo\landing\step1\StoneLanding1\Ebo_jump3		68124

