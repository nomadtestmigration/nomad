using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class disableOnTriggerExit : MonoBehaviour
{
    // Start is called before the first frame update
    void OnTriggerExit()
    {
        gameObject.SetActive(false);
    }
}
