using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class disableOnTriggerEnter : MonoBehaviour
{
    // Start is called before the first frame update
    void OnTriggerEnter()
    {
        gameObject.SetActive(false);
    }
}
