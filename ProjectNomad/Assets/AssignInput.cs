using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Users;

public class AssignInput : MonoBehaviour
{

    private void Start()
    {
        PlayerInput[] inputs = FindObjectsOfType<PlayerInput>();

        foreach (PlayerInput input in inputs)
        {
            InputUser.PerformPairingWithDevice(device: Keyboard.current, input.user);
            InputUser.PerformPairingWithDevice(device: Mouse.current, input.user);
        }
    }

}
