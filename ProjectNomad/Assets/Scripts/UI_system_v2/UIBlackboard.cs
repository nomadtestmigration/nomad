using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI_system_v2
{
    public abstract class UIBlackboard : MonoBehaviour
    {
        [SerializeField] private SceneAsset mainMenuScene;
        
        [SerializeField] private SceneType type;

        public SceneAsset MainMenuScene => mainMenuScene;

        public SceneType Type => type;
    }

    /**
     * Enum that decides what layers can be active.
     */
    public enum SceneType
    {
        InGame,
        Menu,
    }
}
