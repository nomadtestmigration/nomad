using System.Collections;
using System.Collections.Generic;
using UI_system_v2;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI_system_v2.Blackboards
{
    public class MainMenuBlackboard : UIBlackboard
    {
        [SerializeField] private SceneAsset nextScene;

        public SceneAsset NextScene => nextScene;
    }
}
