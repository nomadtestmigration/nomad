using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.SceneManagement;

namespace UI_system_v2.Blackboards
{
    public class InGameBlackboard : UIBlackboard
    {
        [SerializeField] private GameObject ebo;
        
        [SerializeField] private GameObject cc;

        [SerializeField] private CharacterSwitch characterSwitch;
        
        [SerializeField] private Inventory eboInventory;

        [SerializeField] private CameraManager cameraManager;
        
        [SerializeField] private TimeController timeController;

        [SerializeField] private PlayerInteractor interactor;
        
        public Inventory EboInventory => eboInventory;

        public GameObject Ebo => ebo;

        public GameObject Cc => cc;
        
        public CharacterSwitch CharacterSwitch => characterSwitch;

        public CameraManager CameraManager => cameraManager;

        public TimeController TimeController => timeController;
        
        public PlayerInteractor Interactor => interactor;

        public bool PauseToggle { get; set; }

        public bool QuickMenu { get; set; }
    }
}