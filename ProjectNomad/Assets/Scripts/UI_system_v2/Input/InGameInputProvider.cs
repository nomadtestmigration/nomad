using System.Collections;
using System.Collections.Generic;
using UI_system_v2.Blackboards;
using UnityEngine;
using UnityEngine.InputSystem;

namespace UI_system_v2.Input
{
    public class InGameInputProvider : UIInputProvider
    {
        private void OnInGameMenuToggle(InputValue value)
        {
                if (controller.Blackboard is InGameBlackboard)
                {
                    InGameBlackboard blackboard = controller.Blackboard as InGameBlackboard;
                    if (value.isPressed && !blackboard.CameraManager.Brain.IsBlending)
                    {
                        blackboard!.PauseToggle = !blackboard.PauseToggle;
                    }
                }
        }
        
        private void OnQuickMenuOpen(InputValue value)
        {
            if (controller.Blackboard is InGameBlackboard)
            {
                InGameBlackboard blackboard = controller.Blackboard as InGameBlackboard;
                blackboard!.QuickMenu = value.isPressed;
            }
        }
    } 
}


