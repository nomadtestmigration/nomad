using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UI_system_v2
{
    public abstract class UILayer : MonoBehaviour
    {
        [SerializeField] protected bool exclusive;
        [SerializeField] protected int priority;
        
        protected UIController parentController;
        protected List<UIElement> elements;
        private List<Func<bool>> conditions;

        public bool Exclusive => exclusive;
        
        public int Priority => priority;
        
        public List<Func<bool>> Conditions => conditions;
        
        public UIController ParentController => parentController;
        
        protected void OnValidate()
        {
            foreach (var layer in transform.parent.GetComponentsInChildren<UILayer>(true))
            {
                if (layer.Priority == priority && layer != this) Debug.LogWarning("UI layers " + name + " and " + layer.name + " currently share the same priority number.");
            }
        }

        protected virtual void OnEnable()
        {
            SetupElements();
        }

        private void LateUpdate()
        {
            CheckElementsConditions();
        }
        
        /// <summary>
        /// Prepares all data needed for condition checks before awake.
        /// </summary>
        /// <exception cref="SystemException"></exception>
        public void Setup()
        {
            parentController = GetComponentInParent<UIController>();
            if(parentController == null) throw new SystemException("UILayer " + name + " does not have a parent UIController");
            elements = GetComponentsInChildren<UIElement>(true).ToList();
            elements.Sort((elementA, elementB) => elementA.Priority.CompareTo(elementB.Priority));
            conditions = SetupConditions();
        }
        
        /// <summary>
        /// Checks all conditions for the layer.
        /// </summary>
        /// <returns>True when all conditions are true, false when one condition is false</returns>
        public bool CheckConditions()
        {
            if (conditions == null) return true;
            foreach (var condition in conditions)
            {
                if (!condition())
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Calls the setup method on all child elements.
        /// </summary>
        public void SetupElements()
        {
            if(!gameObject.activeSelf) return;
            foreach (UIElement element in elements)
            {
                element.SetupElement();
            }
        }

        private void CheckElementsConditions()
        {
            bool exclusiveActive = false;
            foreach (var element in elements)
            {
                if (!exclusiveActive && element.CheckConditions())
                {
                    element.gameObject.SetActive(true);
                    exclusiveActive = element.Exclusive;
                }
                else
                {
                    element.gameObject.SetActive(false);
                }
            }
        }
        
        protected abstract List<Func<bool>> SetupConditions();
    }
}
