using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI_system_v2
{
    public class UIController : MonoBehaviour
    {
        private List<UILayer> layers;

        public List<UILayer> Layers => layers;

        public UIBlackboard Blackboard { get; private set; }

        private void Awake()
        {
            if(AlreadyExists()) 
            {
                DestroyImmediate(gameObject); 
                return;
            }
            DontDestroyOnLoad(gameObject);
            DisableAllUIChildren();
            layers = GetComponentsInChildren<UILayer>(true).ToList();
            layers.Sort((layerA, layerB) => layerA.Priority.CompareTo(layerB.Priority));
            SearhForUIBlackboard();
        }

        private bool AlreadyExists()
        {
            return FindObjectsOfType<UIController>().Length > 1;
        }
        
        private void DisableAllUIChildren()
        {
            GetComponentsInChildren<UILayer>(true).ToList().ForEach(layer => layer.gameObject.SetActive(false));
            GetComponentsInChildren<UIElement>(true).ToList().ForEach(element => element.gameObject.SetActive(false));
        }
        
        private void Start()
        {
            SetupLayers();
        }

        private void SetupLayers()
        {
            foreach (var layer in layers)
            {
                layer.Setup();
            }
        }
        
        private void LateUpdate()
        {
            CheckLayerConditions();
        }

        private void CheckLayerConditions()
        {
            bool exclusiveActive = false;
            foreach (var layer in layers)
            {
                if (!exclusiveActive && layer.CheckConditions())
                {
                    layer.gameObject.SetActive(true);
                    exclusiveActive = layer.Exclusive;
                }
                else
                {
                    layer.gameObject.SetActive(false);
                }
            }
        }
        
        private void SearhForUIBlackboard()
        {
            Blackboard = FindObjectOfType<UIBlackboard>();
            if(Blackboard == null) throw new SystemException("UIController was unable to find blackboard in scene");
        }
        

        private void OnEnable()
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        private void OnDisable()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }

        private void OnDestroy()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }
        
        private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            SearhForUIBlackboard();
            foreach (var layer in layers)
            {
                layer.Setup();
                layer.SetupElements();
            }
        }
    }
}
