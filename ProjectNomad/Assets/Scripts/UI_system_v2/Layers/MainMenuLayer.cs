using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UI_system_v2.Layers
{
    public class MainMenuLayer : UILayer
    {
        [SerializeField] private AK.Wwise.Event MenuHover;

        protected override List<Func<bool>> SetupConditions()
        {
            List<Func<bool>> conditions = new List<Func<bool>>();
            conditions.Add(() => parentController.Blackboard.Type == SceneType.Menu);
            return conditions;
        }
        public void playMenuHover()
        {
            MenuHover?.Post(gameObject);
        }
    }
    
}

