using System;
using System.Collections;
using System.Collections.Generic;
using UI_system_v2;
using UI_system_v2.Blackboards;
using UnityEngine;
using UnityEngine.InputSystem;

namespace UI_system_v2.Layers
{
    public class InGameMenuLayer : UILayer
    {
        private float timer;
        private float elapsedTime;

        private InGameBlackboard blackboard;
        protected override void OnEnable()
        {
            base.OnEnable();
            blackboard = (parentController.Blackboard as InGameBlackboard);
            blackboard?.TimeController.StopTheGame();
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            blackboard?.CameraManager.SetCameraMovementActive(false);
            timer = Time.time;
        }

        private void OnDisable()
        {
            blackboard?.TimeController.ResumeTheGame();
            blackboard?.CameraManager.SetCameraMovementActive(true);
        }
        
        private void OnDestroy()
        {
            blackboard?.TimeController.ResumeTheGame();
            blackboard?.CameraManager.SetCameraMovementActive(true);
        }

        protected override List<Func<bool>> SetupConditions()
        {
            List<Func<bool>> conditions = new List<Func<bool>>();
            conditions.Add(() => parentController.Blackboard.Type == SceneType.InGame);
            conditions.Add(() => (parentController.Blackboard as InGameBlackboard)!.PauseToggle);
            return conditions;
        }
    }
}

