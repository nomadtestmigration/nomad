using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace UI_system_v2.Layers
{
    public class InGameLayer : UILayer
    {
        protected override void OnEnable()
        {
            base.OnEnable();
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        
        protected override List<Func<bool>> SetupConditions()
        {
            List<Func<bool>> conditions = new List<Func<bool>>();
            conditions.Add(() => parentController.Blackboard.Type == SceneType.InGame);
            return conditions;
        }
    }
}