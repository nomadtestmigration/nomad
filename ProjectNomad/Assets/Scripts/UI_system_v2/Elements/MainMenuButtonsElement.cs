using System;
using System.Collections;
using System.Collections.Generic;
using UI_system_v2;
using UI_system_v2.Blackboards;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI_system_v2.Elements
{
    public class MainMenuButtonsElement : UIElement
    {

        /// <summary>
        /// This function will close the game.
        /// </summary>
        public void ExitButton()
        {
            Application.Quit();
        }

        /// <summary>
        /// This function loads the next scene.
        /// </summary> 
        public void StartGame()
        {
            SceneManager.LoadScene((GetComponentInParent<UILayer>().ParentController.Blackboard as MainMenuBlackboard)!.NextScene.name);
            Time.timeScale = 1;
        }

        
        protected override void Setup()
        {
            
        }

        protected override List<Func<bool>> SetupConditions()
        {
            return null;
        }
    }
}