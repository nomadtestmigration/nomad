using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UI_system_v2;
using UI_system_v2.Blackboards;
using UnityEngine;
using UnityEngine.InputSystem;

public class InteractionMarkerElement : UIElement
{
    private InGameBlackboard blackboard;

    private TextMeshProUGUI textDisplay;

    [SerializeField] private Vector3 offset;
    
    [SerializeField] private InputActionReference inputActionReference;
    
    private void OnEnable()
    {
        UpdateMarkerPosition();
        UpdateMarkerText();
    }

    protected override void Awake()
    {
        base.Awake();
        textDisplay = GetComponentInChildren<TextMeshProUGUI>();
    }

    private void LateUpdate()
    {
        UpdateMarkerPosition();
        UpdateMarkerText();
    }

    private void UpdateMarkerPosition()
    {
        transform.position = blackboard.CameraManager.Brain.OutputCamera.WorldToScreenPoint(blackboard.Interactor.InteractableProximity.CurrentInteractable.transform.position + offset);
    }

    private void UpdateMarkerText()
    {
        textDisplay.text = inputActionReference.action.GetBindingDisplayString();
    }
    
    protected override void Setup()
    {
        blackboard = ParentLayer.ParentController.Blackboard as InGameBlackboard;
        if (blackboard == null) throw new SystemException("InteractionMarkerElement " + name + " cannot access correct type of blackboard");
    }

    protected override List<Func<bool>> SetupConditions()
    {
        List<Func<bool>> conditions = new List<Func<bool>>();
        conditions.Add(() => blackboard.CharacterSwitch.CurrentCharacter == CharacterSwitch.ActiveCharacter.Ebo);
        conditions.Add(() => blackboard.Interactor.InteractableProximity.CurrentInteractable != null);
        return conditions;
    }
}
