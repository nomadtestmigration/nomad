using System;
using System.Collections;
using System.Collections.Generic;
using UI_system_v2;
using UI_system_v2.Blackboards;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class SelectedCharacterElement : UIElement
{
    [SerializeField] private Sprite eboSprite;
    [SerializeField] private Sprite ccSprite;
    
    private InGameBlackboard blackboard;
    private Image m_image;

    protected override void Awake()
    {
        base.Awake();
        m_image = GetComponent<Image>();
    }

    protected override void Setup()
    {
        blackboard = ParentLayer.ParentController.Blackboard as InGameBlackboard;
        if (blackboard == null) throw new SystemException("HealthElement " + name + " cannot access correct type of blackboard");
    }

    private void Update()
    {
        m_image.sprite = blackboard.CharacterSwitch.CurrentCharacter == CharacterSwitch.ActiveCharacter.Ebo ? eboSprite : ccSprite;
    }

    protected override List<Func<bool>> SetupConditions()
    {
        return null;
    }
}
