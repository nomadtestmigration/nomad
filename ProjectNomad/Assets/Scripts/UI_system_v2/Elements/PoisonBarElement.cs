using System;
using System.Collections;
using System.Collections.Generic;
using UI_system_v2;
using UnityEngine;
using UnityEngine.UI;

namespace UI_system_v2.Elements
{
    public class PoisonBarElement : UIElement
    {
        [SerializeField] private Slider slider;

        [SerializeField] private CharacterStats characterStats;

        private void SetMaxPoison(float poison)
        {
            slider.maxValue = poison;
        }

        private void UpdatePoisonBar()
        {
            slider.value = characterStats.CurrentPoison;
        }

        protected override void Setup()
        {
            SetMaxPoison(characterStats.MaxPoison);
            characterStats.updateCurrentPoisonDelegate += UpdatePoisonBar;
        }

        protected override List<Func<bool>> SetupConditions()
        {
            return null;
        }
    }
}