using System;
using System.Collections.Generic;
using System.Linq;
using UI_system_v2;
using UI_system_v2.Blackboards;
using UI_system_v2.Layers;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

namespace UI_system_v2.Elements.QuickMenu
{
    public class QuickMenuElement : UIElement
    {
        [SerializeField] private List<Transform> slots;
        [SerializeField] private Transform emptyHandSlot;
        [SerializeField] private AK.Wwise.Event MenuClick;


        private float centerY;
        private float centerX;
        private InGameBlackboard blackboard;
        private HeldItemHandler heldItemHandler;
        private int previousSelectedSlot;

        protected override void Awake()
        {
            base.Awake();
            RectTransform m_rectTransform = GetComponent<RectTransform>();
            centerY = m_rectTransform.TransformPoint(Vector3.zero).y;
            centerX = m_rectTransform.TransformPoint(Vector3.zero).x;
        }

        protected override void Setup()
        {
            blackboard = ParentLayer.ParentController.Blackboard as InGameBlackboard;
            if (blackboard == null) throw new SystemException("QuickMenuElement " + name + " cannot access correct type of blackboard");
            heldItemHandler = blackboard!.Ebo.GetComponent<HeldItemHandler>();
            if (heldItemHandler == null) throw new SystemException("QuickMenuElement " + name + " cannot can't find heldItemHandler in: " + blackboard.Ebo);
        }

        private void Update()
        {
            SelectSlot(CheckSelectedSlot());
        }

        private void OnEnable()
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            blackboard.CameraManager!.SetCameraMovementActive(false);
        }

        private void OnDisable()
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            blackboard.CameraManager!.SetCameraMovementActive(true);
        }

        private void OnDestroy()
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = false;
        }

        private double CalculateCursorAngleFromCenter()
        {
            Vector3 currentMousePos = Mouse.current.position.ReadValue();
            float deltaY = currentMousePos.y - centerY;
            float deltaX = currentMousePos.x - centerX;
            double angle = Math.Atan2(deltaY, deltaX);
            return angle * (180 / Math.PI);
        }

        private int CheckSelectedSlot()
        {
            double angle = CalculateCursorAngleFromCenter();
            if (angle < -120 && angle >= -180)
            {
                return 0;
            }

            if (angle < 180 && angle >= 120)
            {
                return 1;
            }

            if (angle < 120 && angle >= 60)
            {
                return 2;
            }

            if (angle < 60 && angle >= 0)
            {
                return 3;
            }

            if (angle < 0 && angle >= -60)
            {
                return 4;
            }

            return -1;
        }

        private void SelectSlot(int slotIndex)
        {
            Transform selectedSlot = slots.ElementAtOrDefault(slotIndex) ?? emptyHandSlot;
            HighlightSlot(selectedSlot);
            if (slotIndex!= previousSelectedSlot)
            {
                MenuClick?.Post(gameObject);

            }
            if (slotIndex == -1)
            {
                SelectEmptyHand();
            }
            else
            {
                SelectItem(slotIndex);
            }
            previousSelectedSlot = slotIndex;
        }

        private void SelectEmptyHand()
        {
            heldItemHandler.Swap(null);
        }

        private void SelectItem(int index)
        {
            BaseItem item = blackboard.EboInventory.itemSlots[index].Item;
            if (item == null)
            {
                SelectEmptyHand();
                return;
            }

            heldItemHandler.Swap(item);
        }

        private void HighlightSlot(Transform slot)
        {
            transform.GetComponent<Image>().sprite = slot.GetComponent<Image>().sprite;
        }

        protected override List<Func<bool>> SetupConditions()
        {
            List<Func<bool>> conditions = new List<Func<bool>>();
            conditions.Add(() => blackboard.CharacterSwitch.CurrentCharacter == CharacterSwitch.ActiveCharacter.Ebo);
            conditions.Add(() => blackboard.QuickMenu);
            return conditions;
        }
    }
}