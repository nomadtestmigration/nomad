using UnityEngine;
using UnityEngine.UI;

public class SlotItem : MonoBehaviour
{
    [SerializeField] private BaseItem item;
    private Image itemSprite;

    private void Awake() 
    {
        itemSprite = transform.Find("Item").GetComponent<Image>();
        SetItemSprite();
    }

    public BaseItem Item 
    {
        get { return item; }
    }
    
    /// <summary>
    ///     Sets the sprite of the item inside gameobject inside this gameobject.
    /// </summary>
    public void SetItemSprite() 
    {
        if (item != null) 
        {
            itemSprite.sprite = item.Sprite;
        }
    }
}
