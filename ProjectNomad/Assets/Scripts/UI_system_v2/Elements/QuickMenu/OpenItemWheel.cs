using UnityEngine;
using UnityEngine.InputSystem;

public class OpenItemWheel : MonoBehaviour
{
    [SerializeField] private GameObject itemWheel;
    
    private void Awake()
    {
        itemWheel.SetActive(false);
    }
    
    private void OnOpenItemWheel(InputValue value)
    {
        itemWheel.SetActive(value.isPressed);
    }
}
