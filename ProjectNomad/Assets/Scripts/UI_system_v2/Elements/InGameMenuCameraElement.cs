using System;
using System.Collections;
using System.Collections.Generic;
using UI_system_v2;
using UI_system_v2.Blackboards;
using UnityEngine;
using UnityEngine.InputSystem;

public class InGameMenuCameraElement : UIElement
{
    private InGameBlackboard blackboard;

    private void OnEnable()
    {
        blackboard.Cc.GetComponent<PlayerInput>().actions.Disable();
        blackboard.Ebo.GetComponent<PlayerInput>().actions.Disable();
        blackboard.CharacterSwitch.GetComponent<PlayerInput>().actions.Disable();
        blackboard.CameraManager.SetCameraMovementActive(false);
        blackboard.CameraManager.SwitchCameraPriorityToVirtualCamera(
            blackboard.CharacterSwitch.CurrentCharacter == CharacterSwitch.ActiveCharacter.Ebo
                ? blackboard.CameraManager.EboPauseCam
                : blackboard.CameraManager.CcPauseCam);
    }

    private void OnDisable()
    {
        blackboard.Cc.GetComponent<PlayerInput>().actions.Enable();
        blackboard.Ebo.GetComponent<PlayerInput>().actions.Enable();
        blackboard.CharacterSwitch.GetComponent<PlayerInput>().actions.Enable();
        blackboard.CameraManager.SwitchCameraPriorityToVirtualCamera(
            blackboard.CharacterSwitch.CurrentCharacter == CharacterSwitch.ActiveCharacter.Ebo
                ? blackboard.CameraManager.EboVFreeLookCam
                : blackboard.CameraManager.CcVFreeLookCam);
        blackboard.CameraManager.SetCameraMovementActive(true);
    }


    protected override void Setup()
    {
        blackboard = ParentLayer.ParentController.Blackboard as InGameBlackboard;
        if (blackboard == null)
            throw new SystemException("InGameMenuCameraElement " + name + " cannot access correct type of blackboard");
    }

    protected override List<Func<bool>> SetupConditions()
    {
        return null;
    }
}