using System;
using System.Collections.Generic;
using UI_system_v2;
using UI_system_v2.Blackboards;
using UnityEngine;
using UnityEngine.UI;

namespace UI_system_v2.Elements
{
    public class HeldItemElement : UIElement
    {
        [SerializeField] private Sprite handSprite;
        [SerializeField] private Image heldItemImage;
        private InGameBlackboard blackboard;
        private HeldItemHandler heldItemHandler;

        protected override void Awake()
        {
            base.Awake();
            SetCurrentItem(null);
        }

        private void LateUpdate()
        {
            BaseItem item = heldItemHandler.ItemInHand;
            SetCurrentItem(item == null ? null : item.Sprite);
        }

        /// <summary>
        /// Sets the current Item in slot to item image, if sprite is null then the handsprite should be set.
        /// </summary>
        /// <param name="sprite"></param>
        public void SetCurrentItem(Sprite sprite)
        {
            heldItemImage.sprite = sprite == null ? handSprite : sprite;
        }

        protected override void Setup()
        {
            blackboard = ParentLayer.ParentController.Blackboard as InGameBlackboard;
            if (blackboard == null) throw new SystemException("CurrentItemElement " + name + " cannot access correct type of blackboard");
            heldItemHandler = blackboard!.Ebo.GetComponent<HeldItemHandler>();
            if (heldItemHandler == null) throw new SystemException("CurrentItemElement " + name + " cannot can't find heldItemHandler in: " + blackboard.Ebo);
        }

        protected override List<Func<bool>> SetupConditions()
        {
            return null;
        }
    }
}