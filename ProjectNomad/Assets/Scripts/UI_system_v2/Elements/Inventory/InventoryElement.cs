using System;
using System.Collections.Generic;
using UI_system_v2;
using UI_system_v2.Blackboards;
using UnityEngine;

public class InventoryElement : UIElement
{
    private InGameBlackboard blackboard;
    
    protected override void Setup()
    {
        blackboard = ParentLayer.ParentController.Blackboard as InGameBlackboard;
        if (blackboard == null) throw new SystemException("CCInventoryElement " + name + " cannot access correct type of blackboard");
    }

    private void OnEnable()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        blackboard.CameraManager!.SetCameraMovementActive(false);
    }

    private void OnDisable()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        blackboard.CameraManager!.SetCameraMovementActive(true);
    }

    private void OnDestroy()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = false;
    }
    
    protected override List<Func<bool>> SetupConditions()
    {
        List<Func<bool>> conditions = new List<Func<bool>>();
        conditions.Add(() => blackboard.Interactor.InteractionInventory != null);
        return conditions;
    }
}
