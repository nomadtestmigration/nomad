using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UI_system_v2;
using UI_system_v2.Blackboards;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI_system_v2.Elements
{
    public class InGameMenuButtonsElement : UIElement
    {
        private InGameBlackboard blackboard;
        /// <summary>
        /// This function will close the game.
        /// </summary>
        public void ExitButton()
        {
            Application.Quit();
        }
        
        /// <summary>
        /// This function loads the main menu.
        /// </summary> 
        public void MainMenuButton()
        {
            SceneManager.LoadScene(blackboard.MainMenuScene.name);
            blackboard.TimeController.ResumeTheGame();

        }

        /// <summary>
        /// This function resume the game.
        /// </summary> 
        public void ResumeGameButton()
        {
            blackboard!.PauseToggle = false;
            blackboard.TimeController.ResumeTheGame();
        }

        protected override void Setup()
        {
            blackboard = ParentLayer.ParentController.Blackboard as InGameBlackboard;
            if (blackboard == null) throw new SystemException("InGameMenuButtonsElement " + name + " cannot access correct type of blackboard");
        }

        protected override List<Func<bool>> SetupConditions()
        {
            return null;
        }
    }
}