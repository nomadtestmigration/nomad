using System;
using System.Collections;
using System.Collections.Generic;
using UI_system_v2;
using UI_system_v2.Blackboards;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

namespace UI_system_v2.Elements
{
    public class HeartsElement : UIElement
    {
        [SerializeField] private CharacterStats characterStats;

        [Header("Image Offset")] 
        [SerializeField] private int offset = 70;

        [Header("Image Prefab")] 
        [SerializeField] private GameObject heartPrefab;

        [Header("Image Sprites")] 
        [SerializeField] private Sprite fullHeartSprite;
        [SerializeField] private Sprite halfHeartSprite;
        [SerializeField] private Sprite emptyHeartSprite;

        private List<GameObject> hearts;
        private int numOfHearts;

        private InGameBlackboard blackboard;
        
        private void OnEnable()
        {
            characterStats.updateHealthDelegate += UpdateHealth;
            UpdateHealth();
        }

        private void OnDisable()
        {
            characterStats.updateHealthDelegate -= UpdateHealth;
        }

        private void OnDestroy()
        {
            characterStats.updateHealthDelegate -= UpdateHealth;
        }

        /// <summary>
        /// It updates health connected to the the Unity Event
        /// </summary>
        public void UpdateHealth()
        {
            if (hearts == null) hearts = CreateHearts();
            float tempHealth = characterStats.Health / 2;
            for (int i = 0; i < hearts.Count; i++)
            {
                if (i < tempHealth)
                {
                    hearts[i].GetComponent<Image>().sprite = fullHeartSprite;
                }
                else
                {
                    hearts[i].GetComponent<Image>().sprite = emptyHeartSprite;
                }
            }

            if (tempHealth % 1 != 0)
            {
                int index = (int) Mathf.Floor(characterStats.Health / 2);
                hearts[index].GetComponent<Image>().sprite = halfHeartSprite;
            }
        }

        private List<GameObject> CreateHearts()
        {
            if (transform.childCount > 0)
            {
                foreach (Transform child in transform)
                {
                    DestroyImmediate(child.gameObject);
                }
            }
            
            List<GameObject> newHearts = new List<GameObject>();
            for (int i = 0; i < numOfHearts; i++)
            {
                newHearts.Add(AddHeart(i * offset, 0));
            }
            return newHearts;
        }

        private GameObject AddHeart(int x, int y)
        {
            GameObject heart = Instantiate(heartPrefab, transform, true);
            heart.GetComponent<RectTransform>().anchoredPosition = new Vector2(x, y);
            return heart;
        }

        protected override void Setup()
        {
            blackboard = ParentLayer.ParentController.Blackboard as InGameBlackboard;
            if (blackboard == null) throw new SystemException("HealthElement " + name + " cannot access correct type of blackboard");
            numOfHearts = characterStats.MaxHealth / 2;
            hearts = CreateHearts();
            UpdateHealth();
        }

        protected override List<Func<bool>> SetupConditions()
        {
            return null;
        }
    }
}