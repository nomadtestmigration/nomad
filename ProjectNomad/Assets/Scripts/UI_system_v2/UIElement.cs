using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UI_system_v2
{
    public abstract class UIElement : MonoBehaviour
    {
        [SerializeField] protected bool exclusive;
        [SerializeField] protected int priority;

        public bool Exclusive => exclusive;
        
        public int Priority => priority;
        
        public UILayer ParentLayer { get; set; }

        public List<Func<bool>> Conditions { get; set; }

        protected virtual void Awake()
        {
            
        }
        
        protected void OnValidate()
        {
            foreach (var element in transform.parent.GetComponentsInChildren<UIElement>(true))
            {
                if (element.Priority == priority && element != this) Debug.LogWarning("UI elements " + name + " and " + element.name + " currently share the same priority number.");
            }
        }
        
        /// <summary>
        /// Prepares all data needed for condition checks before awake.
        /// </summary>
        /// <exception cref="SystemException"></exception>
        public void SetupElement()
        {
            ParentLayer = GetComponentInParent<UILayer>();
            if(ParentLayer == null) throw new SystemException("UIElement " + name + " does not have a parent UILayer");
            Setup();
            Conditions = SetupConditions();
        }

        protected abstract void Setup();

        /// <summary>
        /// Checks all conditions for the element.
        /// </summary>
        /// <returns>True when all conditions are true, false when one condition is false</returns>
        public bool CheckConditions()
        {
            if (Conditions == null) return true;
            foreach (var condition in Conditions)
            {
                if (!condition())
                {
                    return false;
                }
            }
            return true;
        }

        protected abstract List<Func<bool>> SetupConditions();
    }
}