using System;
using System.Collections;
using System.Collections.Generic;
using UI_system_v2;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(UIController), typeof(PlayerInput))]
public class UIInputProvider : MonoBehaviour
{
    protected UIController controller;

    private void Awake()
    {
        controller = GetComponent<UIController>();
    }
}
