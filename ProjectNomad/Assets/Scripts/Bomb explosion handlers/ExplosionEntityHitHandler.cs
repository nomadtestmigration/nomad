using UnityEngine;

public class ExplosionEntityHitHandler : MonoBehaviour
{
    [SerializeField] private float knockbackForce = 10f;
    [SerializeField] private float upforceModifier = 2f;
    [SerializeField] private float explosionDamage = 2f;

    [SerializeField] private CharacterStats characterStats;

    private Rigidbody m_RigidBody;

    private void Start()
    {
        m_RigidBody = gameObject.GetComponent<Rigidbody>();
    }
    
    /// <summary>
    /// Function that knocks back entity from bomb position in TakeKnockback()
    /// Then deal damage to entity based on bomb damage
    /// </summary>
    /// <param name="bombPosition">The Position of the exploding bomb as Vector3</param>
    /// <param name="explosionRadius">The range of the bomb explosion as float</param>
    public void ExplosionHit(Vector3 bombPosition, float explosionRadius)
    {
        TakeKnockback(bombPosition, explosionRadius);

        TakeDamage();
    }

    private void TakeKnockback(Vector3 bombPosition, float explosionRadius)
    {
        if (m_RigidBody == null)
        {
            return;
        }
        
        m_RigidBody.AddExplosionForce(knockbackForce, bombPosition, explosionRadius, upforceModifier, ForceMode.Impulse);
    }

    private void TakeDamage()
    {
        characterStats.Health -= explosionDamage; 
        
        //Add enemy taking damage when it's implemented
    }
}
