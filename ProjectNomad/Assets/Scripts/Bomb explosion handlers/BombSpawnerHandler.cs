using UnityEngine;

public class BombSpawnerHandler : MonoBehaviour
{
    [SerializeField] private float spawnDuration;
    [SerializeField] private GameObject walkingBomb;

    [SerializeField] private float spawnDistance = 3.0f;
    [SerializeField] private float spawnHeight = 0.5f;


    private void Start()
    {
        GameObject bombObject = Instantiate(walkingBomb, new Vector3(transform.position.x + spawnDistance, transform.position.y + spawnHeight, transform.position.z), Quaternion.Euler(new Vector3(0, 0, transform.rotation.z)));
        BombController bombController = bombObject.GetComponent<BombController>();
        bombController.Spawnpoint = transform;
        bombController.bombPickedUp.AddListener(StartTimer);
    }
    
    /// <summary>
    /// Function that starts Coroutine which waits for set time before spawning a new bomb 
    /// </summary>
    public void StartTimer()
    {
        Invoke("BombSpawnTimer", spawnDuration);
    }
    
    private void BombSpawnTimer()
    {
        GameObject bombObject = Instantiate(walkingBomb, new Vector3(transform.position.x + spawnDistance, transform.position.y + spawnHeight, transform.position.z), Quaternion.Euler(new Vector3(0, 0, transform.rotation.z)));
        BombController bombController = bombObject.GetComponent<BombController>();
        bombController.Spawnpoint = transform;
        bombController.bombPickedUp.AddListener(StartTimer);
    }
}
