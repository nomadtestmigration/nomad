using UnityEngine;

public class ExplodeWallHandler : MonoBehaviour
{
    /// <summary>
    /// Function that runs logic for destroying wall (room for expantion if animations and/or force is added)
    /// </summary>
    public void ExplodeWall()
    {
        Destroy(gameObject);
    }
}