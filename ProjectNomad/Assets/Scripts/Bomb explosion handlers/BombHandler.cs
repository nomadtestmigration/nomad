using UnityEngine;

public class BombHandler : MonoBehaviour
{
    [SerializeField] private Material explodePrepMaterial;
    [SerializeField] private float explosionRadius;
    [SerializeField] private LayerMask damagableObjectsMask;
    [SerializeField] private ParticleSystem explosionEffect;
    [SerializeField] private GameObject bombHead;
    
    private Material originalBombMaterial;
    private MeshRenderer headRenderer;
    
    private void Start(){
        headRenderer = bombHead.GetComponent<MeshRenderer>();
        originalBombMaterial = headRenderer.material; 
    }
    
    /// <summary>
    /// Change bomb head material to red material
    /// </summary>
    public void ChangeMaterialToRed()
    {
        headRenderer.material = explodePrepMaterial;
    }

    /// <summary>
    /// Change bomb head material to original material
    /// </summary>
    public void ChangeMaterialToDefault()
    {
        headRenderer.material = originalBombMaterial;
    }

    /// <summary>
    /// Function that controls explosion functionality.
    /// CheckCollisionsHit(): Checks for all colliders in range of the explosion with and Player, Destructable or Enemy.
    /// explosionEffect.play: Plays the explosion effect.
    /// Destroy(): Destroys the current exploding bomb.
    /// </summary>
    public void Explode()
    {
        CheckCollisionsHit();
        
        explosionEffect.Play(true);

        Destroy (gameObject.GetComponent<Transform>().GetChild(0).gameObject);
    }

    /// <summary>
    /// Destroys bomb object from the scene
    /// </summary>
    public void DestroyBomb()
    {
        Destroy(gameObject);
    }

    private void CheckCollisionsHit()
    {
        Collider[] objects = Physics.OverlapSphere(transform.position, explosionRadius ,damagableObjectsMask);
        
        foreach (Collider collider in objects)
        {
            if (collider.TryGetComponent(out ExplodeWallHandler explodeHandler))
            {
                explodeHandler.ExplodeWall();
            } 
            else if(collider.TryGetComponent(out ExplosionEntityHitHandler explodeEntity))
            {
                explodeEntity.ExplosionHit(gameObject.transform.position, explosionRadius);
            } 
            //Uncomment when enemies are implemented
            //Add ExplosionEnemyHitHandler script with logic
            /*else if(collider.TryGetComponent(out ExplosionEnemyHitHandler explodeEnemy))
            {
                explodeEnemy.ExplosionHit(gameObject.transform.position, explosionRadius);
            }*/
        }
    }
}
