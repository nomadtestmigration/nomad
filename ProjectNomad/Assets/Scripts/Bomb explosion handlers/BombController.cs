using UnityEngine;
using UnityEngine.Events;

public class BombController : MonoBehaviour
{
    [SerializeField] private float moveSpeed = 20f;

    private Transform spawnpoint;
    public Transform Spawnpoint
    {
        get => spawnpoint;
        set => spawnpoint = value;
    }

    public UnityEvent bombPickedUp;

    private void Update () {
        transform.RotateAround(spawnpoint.position, Vector3.up, -moveSpeed * Time.deltaTime);
    }

    private void OnDisable()
    {
        bombPickedUp.Invoke();
    }
}