using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerInteractor : MonoBehaviour
{
    [SerializeField] private InteractableProximity interactableProximity;
    [SerializeField] private Inventory inventory;
    [SerializeField] private AK.Wwise.Event MenuClick;

    private Inventory interactionInventory;
    private ActiveInteractable previousInteractable;
    public Inventory Inventory => inventory;

    public Inventory InteractionInventory
    {
        get => interactionInventory;
        set => interactionInventory = value;
    }

    public InteractableProximity InteractableProximity => interactableProximity;

    private void Update()
    {
        if (previousInteractable != null && previousInteractable != interactableProximity.CurrentInteractable)
        {
            previousInteractable.ExitInteract(this);
        }
    }

    /// <summary>
    /// Finds interactables in the proximity of the player and calls the 
    /// <see cref="ActiveInteractable.Interact"/> method of the <see cref="ActiveInteractable"/>.
    /// </summary>
    /// <param name="context">The <see cref="InputValue"/> provided by the <see cref="InputSystem"/>.</param>
    public void OnInteract(InputValue value)
    {
        if (!value.isPressed)
        {
            return;
        }
        ActiveInteractable interactable = interactableProximity.CurrentInteractable;
        if (interactable == null || !interactable.CanInteract())
        {
            return;
        }
        previousInteractable = interactable;
        interactable.Interact(this);
        MenuClick?.Post(gameObject);
    }
}