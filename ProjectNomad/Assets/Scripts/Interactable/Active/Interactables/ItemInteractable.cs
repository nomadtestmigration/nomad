using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class ItemInteractable : ActiveInteractable
{
    [SerializeField] private BaseItem item;
    [Header("Collection animation settings")]
    [SerializeField] private float collectAnimationSpeed = 5;
    [SerializeField] private float collectMinimumScale = 0.1f;
    
    private Collider m_collider;
    private Rigidbody m_rigidbody;
    private bool available = true;
    
    
    private void Awake()
    {
        m_collider = GetComponent<Collider>();
        m_rigidbody = GetComponent<Rigidbody>();
    }

    /// <summary>
    /// Handles player interaction.
    /// </summary>
    /// <param name="interactor">PlayerInteractor class.</param>
    public override void Interact(PlayerInteractor interactor)
    {
        if (interactor.Inventory.HasFreeSlot() || (interactor.Inventory.ContainsItem(item) && interactor.Inventory.IsStackable && item.IsStackable))
        {
            interactor.Inventory.PickUp(item, 1);
            StartCoroutine(GoToInteractor(interactor));
        }
    }

    private IEnumerator GoToInteractor(PlayerInteractor interactor)
    {
        available = false;
        if(m_collider != null) m_collider.enabled = false;
        if(m_rigidbody != null) m_rigidbody.isKinematic = true;
        while (true)
        {
            if(transform.position == interactor.transform.position || Vector3.Distance(transform.localScale, Vector3.zero) <= collectMinimumScale) break;
            transform.position = (Vector3.Lerp(transform.position, interactor.transform.position, collectAnimationSpeed * Time.deltaTime));
            transform.localScale = Vector3.Lerp(transform.localScale, Vector3.zero, collectAnimationSpeed * Time.deltaTime);
            yield return null;
        }
        Destroy(gameObject);
    }
    
    public override bool CanInteract() => available;
    
}
