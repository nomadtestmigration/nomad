using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryInteractable : ActiveInteractable
{
    [SerializeField] private Inventory inventory;

    public Inventory Inventory => inventory;

    /// <summary>
    /// Handles player interaction.
    /// </summary>
    /// <param name="interactor">PlayerInteractor class.</param>
    public override void Interact(PlayerInteractor interactor)
    {
        interactor.GetComponent<HeldItemHandler>()!.Swap(null);
        interactor.InteractionInventory = interactor.InteractionInventory == inventory ? null : inventory;
    }

    /// <summary>
    /// Handles exit of player interaction
    /// </summary>
    /// <param name="interactor">PlayerInteractor class.</param>
    public override void ExitInteract(PlayerInteractor interactor)
    {
        if (interactor.InteractionInventory == inventory)
        {
            interactor.InteractionInventory = null;
        }
    }
}
