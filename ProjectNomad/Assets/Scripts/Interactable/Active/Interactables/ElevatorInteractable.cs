using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static WeightedPlatform;

public class ElevatorInteractable : MonoBehaviour {
	[Header("Elevator settings")]
	[SerializeField]
	private float elevationSpeed = 5f;
	[SerializeField]
	[Tooltip("The amount of units the elevator will go up.")]
	private float elevationHeight = 6f;
	[SerializeField]
	[Tooltip("Idle time before elevator is lowered.")]
	private float idleSeconds = 3f;

	private PlatformState platformState = PlatformState.IDLE;
	private MeshRenderer m_renderer;
	private bool isWaitingToDescend = false;
	private bool moving;
	private Vector3 elevatorDirection = Vector3.up;

	private Vector3? originalPosition;
	public Vector3 OriginPosition => originalPosition ?? transform.position;


	private void Start() {
		originalPosition = transform.position;
	}

	/// <summary>
	/// Toggles elevator between moving and idle
	/// </summary>
	/// <param name="isPuzzleCorrect">Boolean that indicates if the puzzle is solved or not</param>
	public void Activate(bool isPuzzleCorrect) {
		moving = isPuzzleCorrect;

		if (moving) {
			platformState = PlatformState.ASCENDING;
		}
	}

	private void Update() {
		if (platformState == PlatformState.IDLE) {
			return;
		}

		if (platformState == PlatformState.BOTTOM) {
			StartCoroutine(AscendElevator());
			return;
		}

		transform.position += elevatorDirection * elevationSpeed * Time.deltaTime;
		float clampedY = Mathf.Clamp(transform.position.y, OriginPosition.y, OriginPosition.y + elevationHeight);
		transform.position = new Vector3(transform.position.x, clampedY, transform.position.z);

		// Bottom
		if (clampedY == OriginPosition.y) {
			platformState = moving ? PlatformState.BOTTOM : PlatformState.IDLE;
			ReverseDirection();
		}

		// Top
		if (!isWaitingToDescend) {
			if (clampedY - OriginPosition.y >= elevationHeight) {
				platformState = PlatformState.TOP;
				StartCoroutine(LowerElevator());
			}
		}
	}

	private IEnumerator LowerElevator() {
		isWaitingToDescend = true;
		yield return new WaitForSeconds(idleSeconds);
		platformState = PlatformState.DESCENDING;
		isWaitingToDescend = false;
		ReverseDirection();
	}

	private IEnumerator AscendElevator() {
		yield return new WaitForSeconds(idleSeconds);
		platformState = PlatformState.ASCENDING;
	}

	private void ReverseDirection() {
		elevatorDirection = elevatorDirection == Vector3.up ? Vector3.down : Vector3.up;
	}

	private void OnDrawGizmos() {
		m_renderer = GetComponent<MeshRenderer>();
		Vector3 direction = Vector3.up * elevationHeight;

		GizmosExtensions.DrawArrow(OriginPosition, new Vector3(direction.x, direction.y + m_renderer.bounds.size.y / 2, direction.z), Color.red);
	}
}
