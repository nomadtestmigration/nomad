using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InteractableElementNode : ActiveInteractable {
	[SerializeField]
	private float flowDelay = 0f;
	[SerializeField]
	protected ActiveInteractable[] nodes;

	/// <summary>
	///	Activates the current node as well as the nested nodes for recursive purposes
	/// </summary>
	public void Activate() {
		OnActivate();

		foreach (ActiveInteractable node in nodes) {
			node.Interact(null);
		}
	}

	/// <summary>
	/// Activation logic for a single node
	/// </summary>
	public abstract void OnActivate();

	/// <summary>
	/// Determines whether a node can be activated or not
	/// </summary>
	/// <returns>A bool whether you can interact with the object or not</returns>
	public abstract bool CanActivate();
	
	private void OnDrawGizmosSelected() {
		if (nodes == null) {
			return;
		}

		foreach (ActiveInteractable node in nodes) {
			if (node == null) {
				continue;
			}

			Vector3 nodeDirection = node.transform.position - transform.position;
			GizmosExtensions.DrawArrow(transform.position, nodeDirection, Color.red);
		}
	}
	
	private IEnumerator ContinueFlow() {
		yield return new WaitForSeconds(flowDelay);

		foreach (ActiveInteractable node in nodes) {
			node.Interact(null);
		}
	}

	public override void Interact(PlayerInteractor interactor)
	{
		StartCoroutine(ContinueFlow());
	}
}
