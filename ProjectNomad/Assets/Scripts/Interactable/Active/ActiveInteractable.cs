using System.Collections;
using UnityEngine;

public abstract class ActiveInteractable : MonoBehaviour {
	
	/// <summary>
	/// Executes the logic implemented by the class that derives from <see cref="ActiveInteractable"/>.
	/// </summary>
	
	/// <summary>
	/// Executes behaviour based on the type of interactable.
	/// </summary>
	/// <param name="interactor">the object interacting with the interactable</param>
	public abstract void Interact(PlayerInteractor interactor);

	/// <summary>
	/// Called after the interactable was interacted with and is no longer in range.
	/// </summary>
	/// <param name="interactor">the object interacting with the interactable</param>
	public virtual void ExitInteract(PlayerInteractor interactor)
	{
		
	}
	
	/// <summary>
	/// Checks whether or not an interactable is ready to interact.
	/// </summary>
	/// <returns>boolean ready</returns>
	public virtual bool CanInteract() => true;
}