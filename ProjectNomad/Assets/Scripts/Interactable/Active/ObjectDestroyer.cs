using UnityEngine;
using UnityEngine.InputSystem;

public class ObjectDestroyer : MonoBehaviour {

	/// <summary>
	///	Destroys objects with the tag "block" on click by casting a raycast
	/// </summary>
	/// <param name="context">Input actions context</param>
	public void OnClick(InputAction.CallbackContext context) {
		Ray ray = Camera.main.ScreenPointToRay(Mouse.current.position.ReadValue());
		if (Physics.Raycast(ray, out RaycastHit hit)) {
			if (hit.transform.tag == "Block") {
				Destroy(hit.transform.gameObject);
			}
		}
	}
}
