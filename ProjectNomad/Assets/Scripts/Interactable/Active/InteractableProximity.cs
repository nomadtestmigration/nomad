using System;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class InteractableProximity : MonoBehaviour
{
    private List<ActiveInteractable> interactables;

    private ActiveInteractable currentInteractable;
    public ActiveInteractable CurrentInteractable => currentInteractable;
    
    public ActiveInteractable[] Interactables
    {
        get
        {
            RefreshInteractables();
            return interactables.ToArray();
        }
    }

    private void Awake()
    {
        interactables = new List<ActiveInteractable>();
    }

    private void Update()
    {
        currentInteractable = CalculateCurrentInteractable();
    }

    /// <summary>
    /// Checks if any items in the interactables list have been deleted.
    /// </summary>
    public void RefreshInteractables()
    {
        foreach (var interactable in interactables)
        {
            if (interactable == null)
            {
                interactables.Remove(interactable);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.TryGetComponent(out ActiveInteractable interactable))
        {
            return;
        }

        interactables.Add(interactable);
    }

    private void OnTriggerExit(Collider other)
    {
        if (!other.TryGetComponent(out ActiveInteractable interactable))
        {
            return;
        }

        interactables.Remove(interactable);
    }

    private ActiveInteractable CalculateCurrentInteractable()
    {
        return interactables.FindAll((interactable => interactable.CanInteract())).MinBy(interactable => Vector3.Angle(interactable.transform.position - transform.position, transform.forward));
    }
}