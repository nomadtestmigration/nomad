using UnityEngine;

public class DoorController : MonoBehaviour
{
    private static readonly int OpenDoor = Animator.StringToHash("OpenDoor");

    private Animator m_Animator;
    private bool doorNotOpen;

    [Header("Wwise Events")]
    [Tooltip("This is the sound for Opening Airlock")]
    [SerializeField] private AK.Wwise.Event AirlockOpenSound;

    private void Start()
    {
        m_Animator = GetComponent<Animator>();
        doorNotOpen = true;
    }

    /// <summary>
    /// Triggers the animator to open the door.
    /// </summary>
    public void Open()
    {
        print("open");
        m_Animator.SetTrigger(OpenDoor);
        if (doorNotOpen)
        {   
            AirlockOpenSound.Post(gameObject);
            doorNotOpen = false;
        }
    }
}
