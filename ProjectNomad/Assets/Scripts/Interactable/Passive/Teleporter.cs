using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour {
	[SerializeField]
	private List<GameObject> teleportables;

	[SerializeField]
	private Transform destination;

	private void OnTriggerEnter(Collider collider) {
		if (teleportables.Contains(collider.gameObject)) {
			collider.gameObject.transform.position = destination.position;
		}
	}

	private void OnDrawGizmos() {
		if (destination) {
			Gizmos.color = Color.red;
			Gizmos.DrawSphere(destination.position, 0.3f);
		}
	}
}
