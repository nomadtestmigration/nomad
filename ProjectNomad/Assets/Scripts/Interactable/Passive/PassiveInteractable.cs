using UnityEngine;

public abstract class PassiveInteractable : MonoBehaviour
{
    /// <summary>
    /// Event style method. Called by classes that derive from <see cref="PassiveInteractable"/>.
    /// </summary>
    protected abstract void Interact();
}