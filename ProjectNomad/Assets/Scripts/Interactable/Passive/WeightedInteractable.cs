using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class WeightedInteractable : PassiveInteractable {
	[Header("Interaction settings")]
	[SerializeField]
	[TagSelector]
	private string applicableTag;
	[SerializeField]
	private int minimumPresentColliders = 1;

	[SerializeField]
	private List<Collider> applicableColliders;

	[Header("Event settings")]
	[SerializeField]
	private UnityEvent onEnter;
	[SerializeField]
	private UnityEvent onExit;

	private List<Collider> presentColliders;

	protected bool HasRequiredPresentColliders => presentColliders.Count >= minimumPresentColliders;

	protected virtual void Awake() {
		presentColliders = new List<Collider>();
	}

	private void Enter(Collider collider) {
		OnEnter(collider);
		onEnter.Invoke();
	}

	private void Exit(Collider collider) {
		OnExit(collider);
		onExit.Invoke();
	}

	/// <summary>
	/// Gets called when another <see cref="Collider"/> enters the trigger.
	/// </summary>
	/// <param name="collider">The <see cref="Collider"/> that exited the trigger.</param>
	protected abstract void OnEnter(Collider collider);

	/// <summary>
	/// Gets called when another <see cref="Collider"/> exits the trigger.
	/// </summary>
	/// <param name="collider">The <see cref="Collider"/> that exited the trigger.</param>
	protected abstract void OnExit(Collider collider);

	private void OnTriggerEnter(Collider collider) {
		if (!applicableColliders.Contains(collider)) {
			return;
		}

		presentColliders.Add(collider);
		Enter(collider);

		if (HasRequiredPresentColliders) {
			Interact();
		}
	}

	private void OnTriggerExit(Collider other) {
		if (!string.IsNullOrWhiteSpace(applicableTag) && !other.CompareTag(applicableTag)) {
			return;
		}

		presentColliders.Remove(other);
		Exit(other);
	}
}