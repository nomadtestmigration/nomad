using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class AnimatedDoor : MonoBehaviour
{
    #region Animator Cached Properties

    private static readonly int SpeedMultiplier = Animator.StringToHash("SpeedMultiplier");
    private static readonly int State = Animator.StringToHash("State");

    #endregion

    #region Serialized Fields

    [Header("Duration settings")] 
    [SerializeField] private float durationInSeconds = 4f;
    [SerializeField] private float openDurationMultiplier = 0.25f;

    [Header("Delay settings")] 
    [SerializeField] private float openDelayInSeconds = 0;
    [SerializeField] private float closeDelayInSeconds = 0;

    [Header("Advanced settings")]
    [SerializeField] 
    [Tooltip("Controls the step size of the animation blend tree")] 
    [Range(0.0005f, 0.1f)]
    private float increment = 0.0125f;

    #endregion
    
    private float state = 0f;

    private Coroutine currentCoroutine;

    private Animator m_Animator;

    private void Awake()
    {
        m_Animator = GetComponent<Animator>();
    }

    private void Start()
    {
        m_Animator.SetFloat(SpeedMultiplier, 1 / durationInSeconds);
        m_Animator.SetFloat(State, state);
    }

    /// <summary>
    /// Starts the <see cref="Coroutine"/> that opens the door:
    /// <see cref="StartOpening"/>.
    /// </summary>
    public void Open()
    {
        StartNewCoroutine(nameof(StartOpening));
    }

    /// <summary>
    /// Starts the <see cref="Coroutine"/> that closes the door:
    /// <see cref="StartClosing"/>.
    /// </summary>
    public void Close()
    {
        StartNewCoroutine(nameof(StartClosing));
    }

    private IEnumerator StartOpening()
    {
        if (openDelayInSeconds > 0)
        {
            yield return new WaitForSeconds(closeDelayInSeconds);
        }

        while (state < 1)
        {
            m_Animator.SetFloat(State, state += increment);
            yield return new WaitForSeconds(durationInSeconds * increment * openDurationMultiplier);
        }
    }

    private IEnumerator StartClosing()
    {
        if (closeDelayInSeconds > 0)
        {
            yield return new WaitForSeconds(closeDelayInSeconds);
        }

        while (state > 0)
        {
            m_Animator.SetFloat(State, state -= increment);
            yield return new WaitForSeconds(durationInSeconds * increment);
        }
    }

    private void StartNewCoroutine(string coroutineName)
    {
        StopCurrentCoroutine();

        currentCoroutine = StartCoroutine(coroutineName);
    }

    private void StopCurrentCoroutine()
    {
        if (currentCoroutine != null)
        {
            StopCoroutine(currentCoroutine);
        }
    }
}
