using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TogglingPressurePlate : PressurePlate {
	[Header("Light settings")]
	[SerializeField] private Light pointLight;
	[SerializeField] private Light spotLight;

	[Header("Puzzle settings")]
	[SerializeField] private UnityEvent<TogglingPressurePlate> onInteract;
	
	private bool isPressed;
	public bool IsPressed => isPressed;

	protected override void Awake() {
		base.Awake();
		pointLight.enabled = spotLight.enabled = false;
	}

	protected override void OnEnter(Collider collider) {
		base.OnEnter(collider);

		isPressed = !isPressed;
		pointLight.enabled = isPressed;
		spotLight.enabled = isPressed;

		onInteract.Invoke(this);
	}
	private void OnDrawGizmos() {
		if (pointLight) {
			Gizmos.color = Color.red;
			Vector3 direction = pointLight.transform.position - transform.position;
			Gizmos.DrawRay(transform.position, direction);
		}
	}
}
