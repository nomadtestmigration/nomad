using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class PressurePlatePuzzleManager : MonoBehaviour {
	[SerializeField] PressurePlateLink[] links;

	[SerializeField] private UnityEvent<bool> onExecute;

	[SerializeField] private AK.Wwise.Event PlayElevator;

	private List<TogglingPressurePlate> turnedOnPressurePlates;

	[Serializable]
	private struct PressurePlateLink {
		public TogglingPressurePlate pressurePlate;
		public bool isPartOfPuzzle;
		public Light light;
	}

	private void Awake() {
		turnedOnPressurePlates = new List<TogglingPressurePlate>();
	}

	private void Start() {
		ToggleLights(false);
	}


	/// <summary>
	/// Toggle lights that show the correct code
	/// </summary>
	/// <param name="turnedOn">True if the lights should turn on, false if the lights should turn off</param>
	public void ToggleLights(bool turnedOn) {
		foreach (PressurePlateLink pressurePlateLink in links) {
			pressurePlateLink.light.enabled = turnedOn && pressurePlateLink.isPartOfPuzzle;
		}
	}

	/// <summary>
	/// Toggle a pressure plate on/off and check if the puzzle is correct
	/// </summary>
	/// <param name="togglingPressurePlate">Pressure plate instance that is used to check for the solution <see cref="TogglingPressurePlate"/></param>
	public void HandlePressurePlate(TogglingPressurePlate togglingPressurePlate) {
		if (togglingPressurePlate.IsPressed) {
			turnedOnPressurePlates.Add(togglingPressurePlate);
		}
		else {
			turnedOnPressurePlates.Remove(togglingPressurePlate);
		}

		onExecute.Invoke(CheckPuzzleSolution());
	}

	private bool CheckPuzzleSolution() {
		List<TogglingPressurePlate> list = links
			.Where(link => link.isPartOfPuzzle)
			.Select(link => link.pressurePlate)
			.ToList();

		if (turnedOnPressurePlates.Count == list.Count) {
			return turnedOnPressurePlates.Count == list.Count && list.All(pressurePlate => turnedOnPressurePlates.Contains(pressurePlate));
		}

		return false;
	}
	public void PlayElevatorNoise()
    {
		PlayElevator?.Post(gameObject);
    }
}
