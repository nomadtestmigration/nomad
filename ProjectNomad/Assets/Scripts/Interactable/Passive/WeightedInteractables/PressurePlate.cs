using System;
using UnityEngine;
using UnityEngine.Events;
using AKEvent = AK.Wwise.Event;

[RequireComponent(typeof(Animator), typeof(Collider))]
public class PressurePlate : WeightedInteractable {
	private static readonly int Pressed = Animator.StringToHash("Pressed");

	[SerializeField]
	private UnityEvent onPressed;
	[SerializeField]
	private UnityEvent onReleased;

	[Header("Sound settings")]
	[SerializeField]
	private AKEvent compressSound;
	[SerializeField]
	private AKEvent decompressSound;

	private Animator m_animator;

	protected virtual void Awake() {
		base.Awake();
		m_animator = GetComponent<Animator>();
	}

	/// <summary>
	/// Triggers the pressure plate animation to be pressed 
	/// and invokes <see cref="onPressed"/>.
	/// </summary>
	protected override void Interact() {
		m_animator.SetBool("Pressed", true);
		onPressed.Invoke();
		compressSound.Post(gameObject);
	}

	/// <inheritdoc/>
	protected override void OnEnter(Collider col) {
	}

	/// <inheritdoc/>
	protected override void OnExit(Collider col) {
		if (HasRequiredPresentColliders) {
			return;
		}

		m_animator.SetBool(Pressed, false);
		onReleased.Invoke();
		decompressSound.Post(gameObject);
	}
}
