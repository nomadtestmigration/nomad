using System.Collections.Generic;
using UnityEngine;

public class WeightedPlatform : MonoBehaviour {
	[Header("Linked platforms")]
	[SerializeField]
	[Tooltip("A list of platforms that will ascend when stepped on this platform")]
	private List<WeightedPlatform> nodes;
	private Vector3 originalPosition;

	private bool active = true;
	public bool Active {
		get => active;
		set {
			platformState = PlatformState.ASCENDING;
			active = value;
		}
	}

	[Header("Platform settings")]
	[SerializeField]
	[Tooltip("The speed at which a platform will go up and down")]
	private float heightSpeed = 2f;
	[SerializeField]
	[Tooltip("Negative number to indicate the lowest point a platform will go")]
	private float minimumHeight = -7f;
	public PlatformState platformState = PlatformState.ASCENDING;

	private void Start() {
		originalPosition = transform.position;
	}

	private void Update() {
		if (platformState == PlatformState.ASCENDING) {
			transform.position += Vector3.up * heightSpeed * Time.deltaTime;
		}
		else if (platformState == PlatformState.DESCENDING) {
			transform.position += Vector3.down * heightSpeed * Time.deltaTime;
		}

		float clampedY = Mathf.Clamp(transform.position.y, originalPosition.y + minimumHeight, originalPosition.y);
		transform.position = new Vector3(transform.position.x, clampedY, transform.position.z);
	}

	private void OnTriggerEnter(Collider other) {
		if (!active) {
			return;
		}

		foreach (WeightedPlatform node in nodes) {
			if (!node) {
				continue;
			}

			node.platformState = PlatformState.ASCENDING;
		}

		platformState = PlatformState.DESCENDING;
	}

	private void OnTriggerExit(Collider other) {
		platformState = PlatformState.ASCENDING;
	}

	public enum PlatformState {
		ASCENDING,
		DESCENDING,
		BOTTOM,
		TOP,
		IDLE
	}
}
