using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetPlatforms : MonoBehaviour {
	[SerializeField]
	private List<WeightedPlatform> platforms;

	private void OnTriggerEnter(Collider other) {
		foreach (WeightedPlatform platform in platforms) {
			platform.Active = false;
		}
	}
}
