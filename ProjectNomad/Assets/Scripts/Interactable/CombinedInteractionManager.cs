using UnityEngine;
using UnityEngine.Events;

public class CombinedInteractionManager : MonoBehaviour {
	[SerializeField] private int requiredInteractions = 2;
	private int interactionCount;

	[Header("Event settings")]
	[SerializeField]
	private UnityEvent onPressed;
	[SerializeField]
	private UnityEvent onReleased;

	/// <summary>
	/// Increments <see cref="interactionCount"/>. 
	/// Invokes <see cref="onPressed"/> when the interaction count is 
	/// greater than or equal to the amount of required interactions.
	/// </summary>
	public void Press() {
		++interactionCount;

		if (interactionCount >= requiredInteractions) {
			onPressed.Invoke();
		}
	}

	/// <summary>
	/// Decrements <see cref="interactionCount"/>. 
	/// Invokes <see cref="onReleased"/> when the interaction count is 
	/// less than the amount of required interactions.
	/// </summary>
	public void Release() {
		--interactionCount;

		if (interactionCount < requiredInteractions) {
			onReleased.Invoke();
		}
	}
}
