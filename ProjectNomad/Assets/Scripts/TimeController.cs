using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeController : MonoBehaviour
{

    [SerializeField] private CameraManager cameraManager;

    public CameraManager CameraManager => cameraManager;

    /// <summary>
    /// Set the game to pause (time scale to 0).
    /// </summary>
    public void StopTheGame()
    {
        Time.timeScale = 0;
    }

    /// <summary>
    /// Start the coroutine to set the timescale back to one based on the camera blending time.
    /// </summary>
    public void ResumeTheGame()
    {
        StartCoroutine(ResumeTheGameCoroutine());
    }

    private IEnumerator ResumeTheGameCoroutine()
    {
        yield return new WaitForSecondsRealtime(cameraManager.Brain.m_DefaultBlend.m_Time);
        Time.timeScale = 1;
    }

}
