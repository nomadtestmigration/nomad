using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class CharacterSwitcher : MonoBehaviour
{
    public enum Character { EBO, CC };
    public Character currentCharacter = Character.EBO;
    private PlayableDirector m_Director;

    private void Start()
    {
        m_Director = GetComponent<PlayableDirector>();
    }

    private void OnSwitch()
    {
        m_Director.Play();
        if (currentCharacter == Character.EBO)
        {
            currentCharacter = Character.CC;
        }
        else
        {
            currentCharacter = Character.EBO;
        }
    }

}
