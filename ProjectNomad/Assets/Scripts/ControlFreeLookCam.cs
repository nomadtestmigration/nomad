using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class ControlFreeLookCam : MonoBehaviour
{
    private CinemachineFreeLook m_freeLookCam;
    private string xName;
    private string yName;

    // Start is called before the first frame update
    private void Start()
    {
        m_freeLookCam = GetComponent<CinemachineFreeLook>();
        xName = m_freeLookCam.m_XAxis.m_InputAxisName;
        yName = m_freeLookCam.m_YAxis.m_InputAxisName;
    }

    /// <summary>
    /// @Turn the free motion movement on.
    /// </summary>
    public void TurnOn()
    {
        m_freeLookCam.m_XAxis.m_InputAxisName = xName;
        m_freeLookCam.m_YAxis.m_InputAxisName = yName;
    }

    /// <summary>
    /// @Turn the free motion movement off.
    /// </summary>
    public void TurnOff()
    {
        m_freeLookCam.m_XAxis.m_InputAxisName = "";
        m_freeLookCam.m_YAxis.m_InputAxisName = "";
        m_freeLookCam.m_YAxis.Reset();
        m_freeLookCam.m_XAxis.Reset();
    }
}
