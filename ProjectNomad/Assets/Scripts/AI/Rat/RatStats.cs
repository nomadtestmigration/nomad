using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New RatStats", menuName = "RatStats")]
public class RatStats : ScriptableObject
{
    [Header("Current health of the rat")]
    [SerializeField] private float health;
    public delegate void UpdateHealth();
    public UpdateHealth updateHealthDelegate;
    public float Health { get => health; set { health = value; updateHealthDelegate(); } }

    [Header("max health, even numbers only")]
    [SerializeField] private int maxHealth;
    public int MaxHealth { get => maxHealth; set => maxHealth = value; }

    [Header("Attack Damage of the Rat")]
    [SerializeField] private int damage;
    public int Damage => damage;
}
