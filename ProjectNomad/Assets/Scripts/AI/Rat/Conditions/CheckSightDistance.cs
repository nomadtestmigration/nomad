using Pada1.BBCore;
using UnityEngine;

namespace BBUnity.Conditions
{
    /// <summary>
    /// It is a condition of perception to check if the target is in sight within a certain distance and angle.
    /// </summary>
    [Condition("CheckSightDistance")]
    [Help("Checks whether a target is in sight depending on a given angle")]
    public class CheckSightDistance : GOCondition
    {
        private float playerDistance;
        private float ccDistance;

        ///<value>Input Target Ebo to checks it's distance.</value>
        [InParam("Target Ebo")]
        [Help("Target Ebo to check the distance")]
        private GameObject targetEbo;

        ///<value>Input Target CC to checks it's distance.</value>
        [InParam("Target CC")]
        [Help("Target CC to check the distance")]
        private GameObject targetCC;

        ///<value>Input Sight distance that the rat can see in front of him.</value>
        [InParam("Sight Distance")]
        [Help("The maximun sight distance to consider that the rat can see the target")]
        private float sightingDistance;

        ///<value>Input the closest target </value>
        [OutParam("Closest Target")]
        [Help("The closest target from the rat")]
        private GameObject closestTarget;

        ///<value>Input view angle parameter to consider that the target is in sight.</value>
        [InParam("angle")]
        [Help("The view angle to consider that the target is in sight")]
        private float angle;

        /// <summary>
        /// Checks first whether Ebo or CC is closer and then it checks whether the target is in sight depending on a given angle, casting a raycast to the target and then compare the angle of forward vector
        /// with de raycast direction.
        /// </summary>
        /// <returns>True if the angle of forward vector with the  raycast direction is lower than the given angle. And false if both Ebo and CC are not in sight.</returns>
        public override bool Check()
        {
            Vector3 eboDirection = (targetEbo.transform.position - gameObject.transform.position);
            Vector3 ccDirection = (targetCC.transform.position - gameObject.transform.position);

            playerDistance = Vector3.Distance(gameObject.transform.position, targetEbo.transform.position);
            ccDistance = Vector3.Distance(gameObject.transform.position, targetCC.transform.position);
            RaycastHit hit;

            if (playerDistance <= ccDistance)
            {
                if (Physics.Raycast(gameObject.transform.position + new Vector3(0, 0.1f, 0), eboDirection, out hit))
                {
                    return hit.collider.gameObject == targetEbo && Vector3.Angle(eboDirection, gameObject.transform.forward) < angle * 0.5f;
                } 
            }
            else
            {
                if (Physics.Raycast(gameObject.transform.position + new Vector3(0, 0.1f, 0), ccDirection, out hit))
                {
                    return hit.collider.gameObject == targetCC && Vector3.Angle(ccDirection, gameObject.transform.forward) < angle * 0.5f;
                }
            }
            return false;
        }
    }
}