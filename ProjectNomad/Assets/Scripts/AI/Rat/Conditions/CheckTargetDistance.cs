using Pada1.BBCore;
using UnityEngine;

namespace BBUnity.Conditions
{
    /// <summary>
    /// It is a perception condition to check the distance to Ebo and CC.
    /// </summary>
    [Condition("Perception/CheckTargetDistance")]
    [Help("Checks which target is closer based on given distance")]
    public class CheckTargetDistance : GOCondition
    {
        private float eboDistance;
        private float ccDistance;
        private bool checkPositionTargets;

        ///<value>Input Target Ebo to checks it's distance.</value>
        [InParam("Target ebo")]
        [Help("Target Ebo to check the distance")]
        private GameObject targetEbo;

        ///<value>Input Target CC to checks it's distance.</value>
        [InParam("Target CC")]
        [Help("Target CC to check the distance")]
        private GameObject targetCC;

        ///<value>Input Sight distance that the rat can see in front of him.</value>
        [InParam("Sight Distance")]
        [Help("The maximun sight distance to consider that the rat can see the target")]
        private float sightingDistance;

        ///<value>Input the closest target </value>
        [OutParam("Closest Target")]
        [Help("The closest target from the rat")]
        private GameObject closestTarget;

        /// <summary>
        /// Checks whether a target is close depending on a given distance,
        /// calculates the magnitude between the gameobject and the target and then compares with the given distance.
        /// </summary>
        /// <returns>True if the magnitude between the gameobject and de target is lower that the given distance.</returns>
        public override bool Check()
        {
            eboDistance = Vector3.Distance(gameObject.transform.position, targetEbo.transform.position);
            ccDistance = Vector3.Distance(gameObject.transform.position, targetCC.transform.position);
            
            if (eboDistance <= ccDistance)
            {
                closestTarget = targetEbo;
                checkPositionTargets = (gameObject.transform.position - targetEbo.transform.position).sqrMagnitude < sightingDistance * sightingDistance;
            } else
            {
                closestTarget = targetCC;
                checkPositionTargets = (gameObject.transform.position - targetCC.transform.position).sqrMagnitude < sightingDistance * sightingDistance;
            }
            return checkPositionTargets;
        }
    }
}