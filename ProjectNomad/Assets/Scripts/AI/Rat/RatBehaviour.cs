using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RatBehaviour : MonoBehaviour
{
    private NavMeshAgent navAgent;
    private Animator anim;

    [SerializeField] private CharacterStats statsEbo;
    [SerializeField] private CharacterStats statsCC;
    [SerializeField] private RatStats ratStats;
    [SerializeField] private int health;

    private static readonly int Move = Animator.StringToHash("Move");
    private static readonly int AttackTrigger = Animator.StringToHash("AttackTrigger");


    private void Start()
    {
        navAgent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        health = ratStats.MaxHealth;
    }

    private void Update()
    {
        anim.SetFloat(Move, navAgent.velocity.magnitude);
        if (health <= 0) KillRat();
    }

    /// <summary>
    /// Attacking function of the rat where it will play the attack animation and decrease the health of attacked target.
    /// </summary>
    /// <param name="target">Receiving who is the target that the Rat will attack.</param>
    public void Attacking(string target)
    {
        anim.SetTrigger(AttackTrigger);
        if (target == "EBO")
        {
            statsEbo.Health -= ratStats.Damage;
        }
        else
        {
            statsCC.Health -= ratStats.Damage;
        }
    }

    /// <summary>
    /// Receiving damage from the player.
    /// </summary>
    /// <param name="damage">The amount of Damage that the rat receives.</param>
    public void ReceiveDamage(int damage)
    {
        health -= damage;
    }

    private void KillRat()
    {
        Destroy(gameObject);
    }
}
