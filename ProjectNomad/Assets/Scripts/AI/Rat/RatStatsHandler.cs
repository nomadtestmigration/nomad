using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RatStatsHandler : MonoBehaviour
{
    [SerializeField] private RatStats ratStats;
    public delegate void UpdateHealth();
    public UpdateHealth updateHealthDelegate;

    public float Health { get => ratStats.Health; set { ratStats.Health = value; updateHealthDelegate(); } }
    public int MaxHealth { get => ratStats.MaxHealth; set => ratStats.MaxHealth = value; }
    public int Damage => ratStats.Damage;
}
