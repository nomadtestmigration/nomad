using BBUnity.Actions;
using Pada1.BBCore;
using Pada1.BBCore.Tasks;
using UnityEngine;
using UnityEngine.AI;

[Action("RatAction/Attack")]
[Help("Attacking action of the Rat")]
public class Attack : GOAction
{
    ///<value>Input get the closest target to attack.</value>
    [InParam("closestTarget")]
    [Help("Target that is closest to attack")]
    private GameObject attackingTarget;

    ///<value>Input RatBehaviour script to call for attack.</value>
    [InParam("Rat")]
    [Help("RatBehaviour script to call attacking function")]
    private RatBehaviour rat;

    /// <Summary>Initialization Method of Attack.</Summary>
    /// <remarks>Call attacking function in the <see cref="RatBehaviour"/> file with the current attacking target.</remarks>
    public override void OnStart()
    {
        rat.Attacking(attackingTarget.name);
    }
}