using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ratMovement : MonoBehaviour
{
    private NavMeshAgent navAgent;
    private Animator anim;
    private int attackTrigger = 0;

    [SerializeField] private CharacterStats statsEbo;
    [SerializeField] private CharacterStats statsCC;
    [SerializeField] private RatStats ratStats;


    private void Start()
    {
        navAgent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        attackTrigger = Animator.StringToHash("AttackTrigger");
    }

    private void Update()
    {
        anim.SetFloat("Move", navAgent.velocity.magnitude);
    }

    public void Attacking(string target)
    {
        anim.SetTrigger(attackTrigger);
        if (target == "EBO")
        {
            statsEbo.Health -= ratStats.Damage;
        }
        else
        {
            statsCC.Health -= ratStats.Damage;
        }
    }

}
