﻿using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent), typeof(BehaviorExecutor), typeof(MovementController))]
public class AIFollowController : MonoBehaviour
{
    [SerializeField] private bool isFollowing;
    [SerializeField] private AIFollowController target;

    public bool IsFollowing
    {
        get => isFollowing; 
        set => isFollowing = value;
    }

    private MovementController m_MovementController;
    private NavMeshAgent m_NavMeshAgent;

    private void Awake()
    {
        m_NavMeshAgent = GetComponent<NavMeshAgent>();
        m_MovementController = GetComponent<MovementController>();
    }

    private void Start()
    {
        m_NavMeshAgent.updatePosition = false;
        m_NavMeshAgent.updateRotation = false;
    }
    
    private void OnCommand()
    {
        if (m_MovementController.Mode != MovementController.CharacterControlMode.PlayerCharacter)
        {
            return;
        }

        target.IsFollowing = !target.IsFollowing;
    }
}