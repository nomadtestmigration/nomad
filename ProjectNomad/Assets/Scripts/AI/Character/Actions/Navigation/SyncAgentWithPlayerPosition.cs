﻿using BBUnity.Actions;
using Pada1.BBCore;
using Pada1.BBCore.Tasks;
using UnityEngine.AI;

[Action("Nomad/Navigation/SyncAgentWithPlayerPosition")]
[Help("Sets the position of the NavMeshAgent equal to the position of the transform of the character.")]
public class SyncAgentWithPlayerPosition : GOAction
{
    private NavMeshAgent m_NavMeshAgent;

    /// <summary>
    /// Retrieves <see cref="NavMeshAgent"/> on start.
    /// </summary>
    public override void OnStart()
    {
        m_NavMeshAgent = gameObject.GetComponent<NavMeshAgent>();
    }

    /// <summary>
    /// Warps the position of the NavMeshAgent to the position of the player transform.
    /// </summary>
    /// <returns><see cref="TaskStatus.FAILED"/> when member components are null; otherwise <see cref="TaskStatus.COMPLETED"/></returns>
    public override TaskStatus OnUpdate()
    {
        if (m_NavMeshAgent == null) return TaskStatus.FAILED;

        m_NavMeshAgent.Warp(gameObject.transform.position);
        return TaskStatus.COMPLETED;
    }
}