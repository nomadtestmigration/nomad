using BBUnity.Actions;
using Pada1.BBCore;
using Pada1.BBCore.Tasks;
using UnityEngine.AI;

[Action("Nomad/Navigation/ChangeSprintingState")]
[Help("Changes the sprinting state of the AI and adjusts speeds accordingly.")]
public class ChangeSprintingState : GOAction
{
    [InParam("enabled")] 
    private bool enabled;

    private MovementController m_MovementController;
    private NavMeshAgent m_NavMeshAgent;
    private SprintState m_SprintState;
    private WalkState m_WalkState;

    /// <summary>
    /// Retrieves <see cref="MovementController"/>, <see cref="NavMeshAgent"/>, <see cref="SprintState"/> and <see cref="WalkState"/>.
    /// </summary>
    public override void OnStart()
    {
        m_MovementController = gameObject.GetComponent<MovementController>();
        m_NavMeshAgent = gameObject.GetComponent<NavMeshAgent>();
        m_SprintState = gameObject.GetComponent<SprintState>();
        m_WalkState = gameObject.GetComponent<WalkState>();
    }

    /// <summary>
    /// Changes the sprinting state of the <see cref="MovementController"/>, based on the provided <see cref="bool"/>.
    /// </summary>
    /// <returns>
    /// <see cref="TaskStatus.FAILED"/> when member components are null;
    /// otherwise <see cref="TaskStatus.RUNNING"/>.
    /// </returns>
    public override TaskStatus OnUpdate()
    {
        if (m_MovementController == null || m_NavMeshAgent == null || m_SprintState == null || m_WalkState == null)
        {
            return TaskStatus.FAILED;
        }
        
        if (enabled)
        {
            m_MovementController.Sprinting = true;
            m_NavMeshAgent.speed = m_SprintState.SprintSpeed;
        }
        else
        {
            m_MovementController.Sprinting = false;
            m_NavMeshAgent.speed = m_WalkState.WalkSpeed;
        }
        
        return TaskStatus.COMPLETED;
    }
}