﻿using BBUnity.Actions;
using Pada1.BBCore;
using Pada1.BBCore.Tasks;
using UnityEngine;
using UnityEngine.AI;

[Action("Nomad/Navigation/MoveToTarget")]
[Help("Move to target by using the MovementController.")]
public class MoveToTarget : GOAction
{
    [InParam("target")] private GameObject target;

    private MovementController m_MovementController;
    private NavMeshAgent m_NavMeshAgent;

    /// <summary>
    /// Retrieves <see cref="MovementController"/> and <see cref="NavMeshAgent"/> on start.
    /// Also initialises the <see cref="NavMeshAgent"/>.
    /// </summary>
    public override void OnStart()
    {
        m_MovementController = gameObject.GetComponent<MovementController>();
        m_NavMeshAgent = gameObject.GetComponent<NavMeshAgent>();

        if (m_NavMeshAgent != null)
        {
            m_NavMeshAgent.isStopped = false;
            m_NavMeshAgent.SetDestination(target.transform.position);
        }
    }

    /// <summary>
    /// Updates the position and rotation of the <see cref="MovementController"/> in order to follow the <see cref="target"/>.
    /// Also makes the <see cref="MovementController"/> jump when necessary.
    /// </summary>
    /// <returns>
    /// <see cref="TaskStatus.FAILED"/> when member components are null or no valid path can be calculated;
    /// <see cref="TaskStatus.COMPLETED"/> when agent has reached destination;
    /// otherwise <see cref="TaskStatus.RUNNING"/>.
    /// </returns>
    public override TaskStatus OnUpdate()
    {
        if (m_MovementController == null || m_NavMeshAgent == null)
        {
            return TaskStatus.FAILED;
        }

        if (!m_NavMeshAgent.pathPending)
        {
            if (m_NavMeshAgent.remainingDistance <= m_NavMeshAgent.stoppingDistance) return TaskStatus.COMPLETED;

            if (!m_NavMeshAgent.pathPending && m_NavMeshAgent.pathStatus is not NavMeshPathStatus.PathComplete) return TaskStatus.FAILED;
        }

        if (m_NavMeshAgent.destination != target.transform.position)
        {
            m_NavMeshAgent.SetDestination(target.transform.position);
        }

        UpdatePosition();
        TryJump();

        return TaskStatus.RUNNING;
    }

    private void UpdatePosition()
    {
        Vector3 dir = m_NavMeshAgent.steeringTarget - m_NavMeshAgent.nextPosition;
        if (dir == Vector3.zero)
        {
            m_MovementController.MovementVectorValue = Vector2.zero;
            return;
        }

        m_MovementController.RotationTarget = Quaternion.LookRotation(new Vector3(dir.x, 0f, dir.z)).normalized;
        m_MovementController.MovementVectorValue = Vector2.up;

        m_NavMeshAgent.nextPosition = gameObject.transform.position;
    }

    private void TryJump()
    {
        if (!m_NavMeshAgent.isOnOffMeshLink) return;

        if (m_NavMeshAgent.currentOffMeshLinkData.linkType == OffMeshLinkType.LinkTypeJumpAcross)
        {
            m_MovementController.Jumping = true;
        }

        m_NavMeshAgent.CompleteOffMeshLink();
    }

    private void End()
    {
        m_NavMeshAgent.SetDestination(gameObject.transform.position);
        m_NavMeshAgent.isStopped = true;
        m_MovementController.MovementVectorValue = Vector2.zero;
    }

    /// <summary>
    /// Gets called when <see cref="OnUpdate"/> returns <see cref="TaskStatus.FAILED"/>.
    /// </summary>
    public override void OnFailedEnd()
    {
        End();
    }

    /// <summary>
    /// Gets called when <see cref="OnUpdate"/> returns <see cref="TaskStatus.COMPLETED"/>.
    /// </summary>
    public override void OnEnd()
    {
        End();
    }

    /// <summary>
    /// Gets called when the <see cref="BehaviorExecutor"/> terminates/aborts this task.
    /// </summary>
    public override void OnAbort()
    {
        End();
    }
}
