﻿using BBUnity.Conditions;
using Pada1.BBCore;

[Condition("Nomad/CheckAIFollowing")]
[Help("Checks if the AI is in the follow state")]
public class CheckAIFollowing : GOCondition
{
    [InParam("boolean")] 
    private bool boolean;

    /// <summary>
    /// Compares <see cref="AIFollowController.IsFollowing"/> to the provided <see cref="bool"/>.
    /// </summary>
    /// <returns>
    /// True when <see cref="AIFollowController.IsFollowing"/> is equal to the provided <see cref="bool"/>; otherwise False.</returns>
    public override bool Check()
    {
        return gameObject.GetComponent<AIFollowController>().IsFollowing == boolean;
    }
}