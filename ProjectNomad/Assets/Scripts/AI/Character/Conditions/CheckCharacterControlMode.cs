using BBUnity.Conditions;
using Pada1.BBCore;

[Condition("Nomad/CheckCharacterControlMode")]
[Help("Checks character control mode.")]
public class CheckCharacterControlMode : GOCondition
{
    [InParam("characterControlMode")]
    private MovementController.CharacterControlMode characterControlMode;

    /// <summary>
    /// Compares <see cref="MovementController.Mode"/> to the provided <see cref="MovementController.CharacterControlMode"/>.
    /// </summary>
    /// <returns>
    /// True when <see cref="MovementController.Mode"/> is equal to the provided <see cref="MovementController.CharacterControlMode"/>; otherwise False.</returns>
    public override bool Check()
    {
        MovementController.CharacterControlMode currentControlMode = gameObject.GetComponent<MovementController>().Mode;

        if (currentControlMode == MovementController.CharacterControlMode.PlayerCharacter)
        {
            gameObject.GetComponent<AIFollowController>().IsFollowing = false;
        }
        
        return currentControlMode == characterControlMode;
    }
}
