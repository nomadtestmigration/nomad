using UnityEngine;

public class HeldItemHandler : MonoBehaviour
{
    [SerializeField] private Transform hand;
    public BaseItem ItemInHand { get; private set; }
    public GameObject ItemInHandObject { get; private set; }

    /// <summary>
    /// Handles the logic of swapping item in hand.
    /// </summary>
    /// <param name="item">Instance of BaseItem, we use this to get the actual item in hand object.</param>
    public void Swap(BaseItem item) {
        ItemInHand = item;
        PutItemInHand();
    }

    private void PutItemInHand() {
        Destroy(ItemInHandObject);
        if (ItemInHand != null && ItemInHand.ItemInHand != null) {
            ItemInHandObject = Instantiate(ItemInHand.ItemInHand, hand.position, Quaternion.identity);
            ItemInHandObject.transform.SetParent(hand);
        }
    }
}
