using System;
using System.Collections;
using System.Collections.Generic;

public static class LinqExtensions
{
    /// <summary>
    /// Returns the object filtered by a minimum value
    /// </summary>
    /// <typeparam name="TSource">Generic type source parameter</typeparam>
    /// <typeparam name="TKey">Generic type key parameter</typeparam>
    /// <param name="source">The object that will be compared</param>
    /// <param name="selector">The selector that determines whether an object will be returned or not</param>
    /// <returns>The filtered source object by the selector</returns>
    public static TSource MinBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> selector)
    {
        if (source == null) throw new ArgumentNullException(nameof(source));
        if (selector == null) throw new ArgumentNullException(nameof(selector));

        using IEnumerator<TSource> sourceIterator = source.GetEnumerator();
        if (!sourceIterator.MoveNext())
        {
            return default;
        }

        TSource min = sourceIterator.Current;
        TKey minKey = selector(min);
        while (sourceIterator.MoveNext())
        {
            TSource candidate = sourceIterator.Current;
            TKey candidateProjected = selector(candidate);

            if (Comparer<TKey>.Default.Compare(candidateProjected, minKey) >= 0)
            {
                continue;
            }

            min = candidate;
            minKey = candidateProjected;
        }

        return min;
    }
}
