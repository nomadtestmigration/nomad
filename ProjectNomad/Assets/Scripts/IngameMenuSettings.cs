using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IngameMenuSettings : MonoBehaviour
{
    [SerializeField] private string nextSceneName = "";
    [SerializeField] private string mainMenuScene = "";
    [SerializeField] private CanvasManager canvasManager;

    /// <summary>
    /// This function loads the main menu.
    /// </summary
    public void MainMenu()
    {
        if (GameObject.FindGameObjectWithTag("CanvasController") != null)
        {
            canvasManager = GameObject.FindGameObjectWithTag("CanvasController").GetComponent<CanvasManager>();
            canvasManager.SwitchCanvas(CanvasType.MAINMENU);
            SceneManager.LoadScene(mainMenuScene);
        }
    }

    /// <summary>
    /// This function closes the game.
    /// </summary
    public void ExitButton()
    {
        Application.Quit();
    }

    /// <summary>
    /// This function loads the next scene.
    /// </summary
    public void StartGame()
    {
        SceneManager.LoadScene(nextSceneName);
    }

    /// <summary>
    /// This function pauses the game.
    /// </summary
    public void PauseGame()
    {
        Time.timeScale = 0;
    }

    /// <summary>
    /// This function resumes the game where it left off.
    /// </summary
    public void ResumeGame()
    {
        if (GameObject.FindGameObjectWithTag("CanvasController") != null)
        {
            canvasManager = GameObject.FindGameObjectWithTag("CanvasController").GetComponent<CanvasManager>();
            Time.timeScale = 1;
            canvasManager.SwitchCanvas(CanvasType.GAMEHUD);
        }
    }

}
