using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleState : MovementState
{
    /**
    * Set transitions from current state to given state on gameobject.
    */
    protected void Start()
    {
        transitions = new List<Transition>
        {
            new Transition(() => m_controller.MovementVectorValue != Vector2.zero, GetComponent<WalkState>()),
            new Transition(() => m_controller.Sprinting && m_controller.MovementVectorValue.y >= 0, GetComponent<SprintState>()),
            new Transition(() => !m_controller.Grounded && m_rigidbody.velocity.y <= 0, GetComponent<FallState>()),
            new Transition(() => m_controller.Jumping && CheckSlopeAngle(), GetComponent<JumpState>()),
            new Transition(() => m_controller.Crouching, GetComponent<CrouchState>()),
            new Transition(() => m_controller.Dashing, GetComponent<DashState>())
        };
    }

    protected override void FixedUpdate()
    {
        m_controller.LastVelocity = Vector3.zero;
    }
}

