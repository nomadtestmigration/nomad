using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LandState : MovementState
{
    [SerializeField] private float floatSpeed = 10f;

    private PlaySoundPlayer m_playSoundPlayer;

    protected override void Awake()
    {
        base.Awake();
        m_playSoundPlayer = GetComponent<PlaySoundPlayer>();
    }
    
    /**
    * Set transitions from current state to given state on gameobject.
    */
    protected void Start()
    {
        transitions = new List<Transition>
        {
            new Transition(() => m_controller.Grounded && m_controller.Sprinting && m_controller.MovementVectorValue != Vector2.zero, GetComponent<SprintState>()),
            new Transition(() => m_controller.Grounded && m_controller.MovementVectorValue != Vector2.zero, GetComponent<WalkState>()),
            new Transition(() => m_controller.Grounded, GetComponent<IdleState>()),
        };
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        m_rigidbody.velocity = new Vector3(m_rigidbody.velocity.x / 2, m_rigidbody.velocity.y, m_rigidbody.velocity.z / 2);
        m_playSoundPlayer?.LandingPlay();
    }

    /**
     * Call move every physics update to move player in 8 main directions.
     */
    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        MoveUsingForce(floatSpeed);
    }
}
