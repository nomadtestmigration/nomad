using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SprintState : MovementState
{
    [SerializeField] private float sprintSpeed = 4f;
    [SerializeField] private float rotateSpeed = 1f;

    public float SprintSpeed => sprintSpeed;
    
    /**
    * Set transitions from current state to given state on gameobject.
    */
    protected void Start()
    {
        transitions = new List<Transition>
        {
            new Transition(() => !m_controller.Sprinting || m_controller.MovementVectorValue.y < 0, GetComponent<WalkState>()),
            new Transition(() => m_controller.MovementVectorValue == Vector2.zero, GetComponent<IdleState>()),
            new Transition(() => m_controller.Jumping && CheckSlopeAngle(), gameObject.GetComponent<JumpState>()),
            new Transition(() => !m_controller.Grounded && m_rigidbody.velocity.y <= 0, GetComponent<FallState>()),
            new Transition(() => m_controller.Crouching, GetComponent<CrouchState>()),
            new Transition(() => m_controller.Dashing, GetComponent<DashState>())
        };
    }

    /**
     * Call move every physics update to move player in 8 main directions.
     */
    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        Rotate(rotateSpeed);
        Move(sprintSpeed);
    }
}