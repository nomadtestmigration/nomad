using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrouchState : MovementState
{
    [SerializeField] private float crouchSpeed = 1f;
    [SerializeField] private float rotateSpeed = 1f;
    [SerializeField] private float baseHeight;
    [SerializeField] private bool headHit;
    [SerializeField, Range(0.001f, 1)] private float crouchSizeMultiplier = 0.5f;
    
    private void Start()
    {
        transitions = new List<Transition>
        {
            new Transition(() => !m_controller.Crouching && CheckClearOverhead(), GetComponent<WalkState>()),
            new Transition(() => !m_controller.Crouching && m_controller.MovementVectorValue == Vector2.zero && CheckClearOverhead(), GetComponent<IdleState>())
        };
    }

    protected override void Awake()
    {
        base.Awake();
        m_collider = m_controller.GetComponent<CapsuleCollider>();
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        baseHeight = m_collider.bounds.size.y;
        m_collider.height *= crouchSizeMultiplier;
        m_collider.center = new Vector3(0, m_collider.center.y * crouchSizeMultiplier, 0);
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        m_collider.height /= crouchSizeMultiplier;
        m_collider.center = new Vector3(0, m_collider.center.y / crouchSizeMultiplier, 0);
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        Rotate(rotateSpeed);
        Move(crouchSpeed);
    }

    private bool CheckClearOverhead()
    {
        Vector3 minCollider = new Vector3(m_collider.bounds.center.x, m_collider.bounds.center.y, m_collider.bounds.center.z);
        Vector3 maxCollider = new Vector3(minCollider.x, minCollider.y + baseHeight - m_collider.bounds.size.x, minCollider.z);
        return !Physics.CheckCapsule(minCollider, maxCollider, m_collider.radius, m_controller.LayerMask);  
    }
}