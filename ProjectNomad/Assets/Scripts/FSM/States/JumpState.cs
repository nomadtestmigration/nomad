using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class JumpState : MovementState
{
    [SerializeField] private float floatSpeed = 10f;
    [SerializeField] private float jumpSpeed = 10f;
    [SerializeField] private float minimalJumpTime = 0.2f;

    private PlaySoundPlayer m_playSoundPlayer;
    private PlaySoundCC m_playSoundCC;
    private float time;

    protected override void Awake()
    {
        base.Awake();
        m_playSoundPlayer = GetComponent<PlaySoundPlayer>();
        m_playSoundCC = GetComponent<PlaySoundCC>();
    }

    /**
    * Set transitions from current state to given state on gameobject.
    */
    protected void Start()
    {
        transitions = new List<Transition>
        {
            new Transition(() => ((!m_controller.Grounded && m_rigidbody.velocity.y <= 0) || (m_controller.Grounded)) && time + minimalJumpTime < Time.time, GetComponent<FallState>())
        };
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        time = Time.time;
        m_rigidbody.velocity = transform.TransformDirection(m_controller.LastVelocity) + Vector3.up * jumpSpeed;
        m_controller.Jumping = false;
        m_playSoundPlayer?.JumpBeginPlay();
        m_playSoundCC?.CCJumpBeginPlay();
    }

    /**
     * Call move every physics update to move player in 8 main directions.
     */
    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        MoveUsingForce(floatSpeed);
    }
}
