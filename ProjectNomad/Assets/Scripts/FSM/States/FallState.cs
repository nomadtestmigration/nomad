using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallState : MovementState
{
    [SerializeField] private float floatSpeed = 10f;
    [SerializeField] private float landingMultiplier = 1f;

    /**
    * Set transitions from current state to given state on gameobject.
    */
    protected void Start()
    {
        transitions = new List<Transition>
        {
            new Transition(() => Physics.Raycast(transform.position, m_rigidbody.velocity.normalized, m_rigidbody.velocity.magnitude * landingMultiplier, m_controller.LayerMask), GetComponent<LandState>()),
            new Transition(() => m_controller.Grounded && m_controller.Sprinting && m_controller.MovementVectorValue != Vector2.zero, GetComponent<SprintState>()),
            new Transition(() => m_controller.Grounded && m_controller.MovementVectorValue != Vector2.zero, GetComponent<WalkState>()),
            new Transition(() => m_controller.Grounded, GetComponent<IdleState>())
        };
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        m_rigidbody.velocity = transform.TransformDirection(m_controller.LastVelocity);
    }

    /**
     * Call move every physics update to move player in 8 main directions.
     */
    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        MoveUsingForce(floatSpeed);
    }
}
