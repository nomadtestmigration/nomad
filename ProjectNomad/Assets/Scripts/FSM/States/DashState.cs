using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashState : State
{
    [Header("Dash Variables")]
    [SerializeField] private float dashSpeed = 50f;
    [SerializeField] private float dashDurationInSec = 0.3f;
    
    private bool endRoutine;
    private Vector3 currentDashDirection;
    private Rigidbody m_rigidBody;

    protected override void Awake()
    {
        base.Awake();
        m_rigidBody = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        transitions = new List<Transition>
        {
            new Transition(() => endRoutine, gameObject.GetComponent<IdleState>())
        };
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        
        endRoutine = false;

        if (m_controller.MovementVectorValue == Vector2.zero)
        {
            currentDashDirection = transform.TransformDirection(
                (new Vector3(0.0f, 0.0f, 1f)));
        }
        else
        {
            currentDashDirection = transform.TransformDirection(
                (new Vector3(m_controller.MovementVectorValue.x, 0.0f, m_controller.MovementVectorValue.y)
                ));
        }

        StartCoroutine(DashRoutine());
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        
        m_rigidBody.velocity = Vector3.zero;
        m_controller.Dashing = false;
    }
    
    private IEnumerator DashRoutine()
    {
        float time = Time.time;
        
        while (Time.time < time + dashDurationInSec)
        {
            m_rigidBody.AddForce(currentDashDirection * dashSpeed * Time.deltaTime, ForceMode.Impulse);

            yield return null;
        }

        endRoutine = true;
    }
}
