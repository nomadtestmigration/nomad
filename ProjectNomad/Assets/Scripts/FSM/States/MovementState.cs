using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementState : State
{
    protected Rigidbody m_rigidbody;
    protected CapsuleCollider m_collider;
    
    protected override void Awake()
    {
        base.Awake();
        m_rigidbody = m_controller.GetComponent<Rigidbody>();
        m_collider = m_controller.GetComponent<CapsuleCollider>();
    }
    
    protected void Move(float speed)
    {
        Vector3 movement = new Vector3(m_controller.MovementVectorValue.x, 0.0f, m_controller.MovementVectorValue.y);
        movement = transform.TransformDirection(movement);
        m_rigidbody.MovePosition(transform.position + (movement * Time.fixedDeltaTime * speed));
        m_controller.LastVelocity = new Vector3(m_controller.MovementVectorValue.x, 0, m_controller.MovementVectorValue.y) * speed;
        MoveUpOnStepRayCast(movement);
    }

    protected void Rotate(float speed)
    {
        m_rigidbody.MoveRotation(Quaternion.Slerp(m_rigidbody.rotation, new Quaternion(0, m_controller.RotationTarget.y, 0, m_controller.RotationTarget.w), speed * Time.fixedDeltaTime));
    }

    protected void MoveUsingForce(float force)
    {
        Vector3 movement = new Vector3(m_controller.MovementVectorValue.x, 0.0f, m_controller.MovementVectorValue.y);
        movement = transform.TransformDirection(movement);
        m_rigidbody.AddForce(movement.normalized * force);
        m_controller.LastVelocity = transform.InverseTransformDirection(m_rigidbody.velocity);
    }
    
    private void MoveUpOnStepRayCast(Vector3 movement)
    {
        Vector3 lowerPosition = new Vector3(m_collider.bounds.center.x, m_collider.bounds.min.y + m_controller.MinStepHeight, m_collider.bounds.center.z);
        if (StepRayCast(lowerPosition, movement))
        {
            m_rigidbody.MovePosition(m_rigidbody.position + Vector3.up * m_controller.StepSmoothSpeed);
        }
    }

    private bool StepRayCast(Vector3 lowerPosition, Vector3 baseDirection)
    {
        foreach (var angle in m_controller.Angles)
        {
            if(Physics.Raycast(lowerPosition, Quaternion.Euler(0, angle, 0) * baseDirection, m_controller.StepRadius + m_collider.radius, m_controller.LayerMask)) {
                if(!Physics.Raycast(lowerPosition + Vector3.up * m_controller.MaxStepHeight, baseDirection, m_controller.StepRadius + m_collider.radius + m_controller.MaxStepRadiusDifference, m_controller.LayerMask))
                {
                    return true;
                }   
            }
        }
        return false;
    }

    protected bool CheckSlopeAngle()
    {
        Vector3 lowerPosition = new Vector3(m_collider.bounds.center.x, m_collider.bounds.min.y + m_controller.MinStepHeight, m_collider.bounds.center.z);
        RaycastHit hit;
        if (Physics.Raycast(lowerPosition, Vector3.down, out hit, m_controller.GroundedDistance + m_controller.GroundedRadius, m_controller.LayerMask))
        {
            if (Vector3.Angle(Vector3.up, hit.normal) < m_controller.GroundedMaxAngle)
            {
                return true;
            }
        }
        return false;
    }
}
