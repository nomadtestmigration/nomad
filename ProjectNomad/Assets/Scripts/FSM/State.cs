using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class State : MonoBehaviour
{
    protected List<Transition> transitions;
    protected MovementController m_controller;
    protected Animator m_animator;
    [SerializeField] private string animationTriggerName;
    [SerializeField] private bool keepAnimationTriggerActive = true;
    
    protected virtual void Awake()
    {
        m_controller = GetComponent<MovementController>();
        m_animator = GetComponent<Animator>();

        transitions = new List<Transition>();

        //Setup your transitions
    }

    protected virtual void OnEnable()
    {
        //Develop state's initializations
        m_animator.SetTrigger(animationTriggerName);
    }

    protected virtual void OnDisable()
    {
        m_animator.ResetTrigger(animationTriggerName);
        //Develop state's finalization
    }

    protected virtual void Update()
    {
        if (keepAnimationTriggerActive)
        {
            m_animator.SetTrigger(animationTriggerName);
        }
        //Develop behaviour
    }

    protected virtual void FixedUpdate()
    {
        //Develop behaviour
    }

    protected void LateUpdate()
    {
        foreach (Transition transition in transitions)
        {
            if (transition.Condition())
            {
                transition.Target.enabled = true;
                enabled = false;
                return;
            }
        }
    }
}