using System;

public class Transition
{
    private Func<bool> condition;
    private State target;

    public Func<bool> Condition => condition;
    public State Target => target;
    
    public Transition(Func<bool> condition, State target)
    {
        this.condition = condition;
        this.target = target;
    }
}
