using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(HeldItemHandler))]
public class ItemController : MonoBehaviour {
	[SerializeField] private Inventory inventory;
	[SerializeField] private GameObject cc;
	[SerializeField] private AK.Wwise.Event MenuClick;


	private HeldItemHandler heldItemHandler;

	private void Awake() {
		heldItemHandler = GetComponent<HeldItemHandler>();
	}

	/// <summary>
	/// Uses the current item that is held in the players hand
	/// </summary>
	public void OnUse() {
		GameObject item = heldItemHandler.ItemInHandObject;
		
		if (!item) return;

		if (item.TryGetComponent(out Item itemScript))
		{
			if (itemScript.UseItem(gameObject, inventory, heldItemHandler.ItemInHand, cc))
			{
				heldItemHandler.Swap(null);
				MenuClick?.Post(gameObject);
			}
		}
		else 
		{
			Debug.LogWarning($"Item script is missing on {item.GetType().Name}.");
		}
	}
}
