using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Health Item", menuName = "Items/HealthItems")]
public class HealthItem : BaseItem {
	[SerializeField] private int healingAmount;
	public int HealingAmount => healingAmount;
}