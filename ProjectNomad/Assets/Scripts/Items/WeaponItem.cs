using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Weapon", menuName = "Items/Weapons")]
public class WeaponItem : BaseItem {
	[SerializeField] private int attackDamage;
	public int AttackDamage => attackDamage;

	[SerializeField] private float attackSpeed;
	public float AttackSpeed => attackSpeed;
}