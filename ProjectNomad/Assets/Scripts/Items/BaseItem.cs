using UnityEngine;

public class BaseItem : ScriptableObject
{
    [SerializeField] private string itemName;
    public string ItemName { get { return itemName; } set { itemName = value; } }
    [SerializeField] private bool isStackable;
    public bool IsStackable { get { return isStackable; } set { isStackable = value; } }
    [SerializeField] private int maxStackAmount;
    public int MaxStackAmount { get { return maxStackAmount; } set { maxStackAmount = value; } }
    [SerializeField] private Sprite sprite;
    public Sprite Sprite { get { return sprite; } set { sprite = value; } }
    [SerializeField] private GameObject itemInHand;
    public GameObject ItemInHand { get { return itemInHand; } set { itemInHand = value; } }
    
    [SerializeField] private bool isConsumable;
    public bool IsConsumable { get { return isConsumable; } set { isConsumable = value; } }
}
