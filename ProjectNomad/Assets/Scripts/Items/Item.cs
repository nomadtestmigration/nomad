using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Item : MonoBehaviour {
	/// <summary>
	/// Uses the current item that is in the players hand and checks if it's a consumable.
	/// If it is a consumable, it will remove the item from the inventory and return true.
	/// </summary>
	/// <param name="player"> Ebo as GameObject </param>
	/// <param name="inventory"> The Inventory </param>
	/// <param name="item"> The baseItem of the item in hand</param>
	/// <param name="cc"> CC as GameObject</param>
	/// <returns> Returns true when item is a consumable and is available to be used. returns false in all other cases</returns>
	public bool UseItem(GameObject player, Inventory inventory, BaseItem item, GameObject cc)
	{
		if (!OnUseItem(player, cc)) return false;
		
		if (!item.IsConsumable) return false;

		InventorySlot slot = inventory.GetInventorySlot(item);
		if (slot != null)
		{
			inventory.RemoveItem(item);
			return true;
		}

		return false;
	}

	protected abstract bool OnUseItem(GameObject player, GameObject cc);
	
}
