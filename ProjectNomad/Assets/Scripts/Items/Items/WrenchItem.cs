using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WrenchItem : Item
{
    [SerializeField] private HealthItem healthItem;
    [SerializeField] private CharacterStats characterStats;

    protected override bool OnUseItem(GameObject player, GameObject cc)
    {
        if (!(Vector3.Distance(player.transform.position, cc.transform.position) <= 5)) return false;
        
        if (characterStats.Health >= characterStats.MaxHealth)
        {
            // TODO: Logic to prevent usage of medkit due to already being at max lives
            return false;
        }
        characterStats.Health += healthItem.HealingAmount;
        return true;
    }
}

