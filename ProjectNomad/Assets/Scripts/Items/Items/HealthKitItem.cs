using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthKitItem : Item
{
	[SerializeField] private HealthItem healthItem;
	[SerializeField] private CharacterStats characterStats;

	protected override bool OnUseItem(GameObject player, GameObject _)
	{
		if (characterStats.Health >= characterStats.MaxHealth)
		{
			// TODO: Logic to prevent usage of medkit due to already being at max lives
			return false;
		}

		characterStats.Health += healthItem.HealingAmount;
		return true;
	}
}
