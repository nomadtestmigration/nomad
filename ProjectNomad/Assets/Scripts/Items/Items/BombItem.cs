using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombItem : Item
{
    [SerializeField] private WeaponItem weaponItem;
    [SerializeField] private GameObject Bomb;
    
    private Vector3 spawnPoint;
    private Rigidbody m_rigidbody;
    
    protected override bool OnUseItem(GameObject player, GameObject _)
    {
        spawnPoint = player.transform.position + player.GetComponent<CapsuleCollider>().center + player.transform.forward;

        GameObject bombInst = Instantiate(Bomb, spawnPoint, player.transform.rotation);
        
        bombInst.AddComponent<Rigidbody>();
        m_rigidbody = bombInst.GetComponent<Rigidbody>();
        m_rigidbody.constraints = RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezeRotationX;

        return true;
    }
}
