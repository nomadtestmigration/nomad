using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class MovementController : MonoBehaviour
{
    [Header("Control Settings")] 
    [SerializeField] private CharacterControlMode mode = CharacterControlMode.NonPlayerCharacter;
    [SerializeField] private Transform cameraTransform;
    [SerializeField] private LayerMask layerMask;

    [Header("FSM")] 
    [SerializeField] private List<State> states;

    [Header("Animation Parameters")] 
    [SerializeField] protected string animationXParameter = "movementX";
    [SerializeField] protected string animationZParameter = "movementZ";
    [SerializeField] protected string animationYParameter = "movementY";

    [Header("Movement")] 
    [SerializeField] private float groundedDistance = 0.1f;
    [SerializeField] private float groundedRadius = 0.2f;
    [SerializeField] private float groundedMaxAngle = 37.5f;
    [SerializeField] private float minStepHeight = 0.1f;
    [SerializeField] private float maxStepHeight = 0.3f;
    [SerializeField] private float stepRadius = 0.1f;
    [SerializeField] protected float maxStepRadiusDifference = 0.1f;
    [SerializeField] private float stepSmoothSpeed = 0.1f;
    [SerializeField] private List<float> angles = new List<float>() {-45, 0, 45};

    public CharacterControlMode Mode => mode;

    public float GroundedDistance => groundedDistance;

    public float GroundedRadius => groundedRadius;

    public float GroundedMaxAngle => groundedMaxAngle;

    public float MinStepHeight => minStepHeight;

    public float MaxStepHeight => maxStepHeight;

    public float StepRadius => stepRadius;

    public float MaxStepRadiusDifference => maxStepRadiusDifference;

    public float StepSmoothSpeed => stepSmoothSpeed;

    public List<float> Angles => angles;

    protected Vector2 movementVectorValue;
    protected Quaternion rotationTarget;

    public Quaternion RotationTarget
    {
        get => rotationTarget;
        set => rotationTarget = value;
    }

    private Animator m_animator;
    private Collider m_collider;
    private Rigidbody m_rigidbody;

    /**
     * This vector controls the direction of movement of this character. 
     */
    public Vector2 MovementVectorValue
    {
        get => movementVectorValue;
        set => movementVectorValue = value;
    }

    /**
     * The rotational target is the transform used for steering the character. When the character walks it will attempt to rotate into the same direction as this transform's forward.
     */
    public Transform CameraTransform => cameraTransform;

    public LayerMask LayerMask => layerMask;

    /**
     * Bool used to keep track of when the character should sprint.
     */
    [Header("Character Input states")]
    private bool sprinting;

    public bool Sprinting
    {
        get => sprinting;
        set => sprinting = value;
    }

    private bool jumping;

    public bool Jumping
    {
        get => jumping;
        set => jumping = value;
    }

    private bool grounded;

    public bool Grounded
    {
        get => grounded;
        set => grounded = value;
    }

    private bool crouching;

    public bool Crouching
    {
        get => crouching;
        set => crouching = value;
    }

    private bool dashing;

    public bool Dashing
    {
        get => dashing;
        set => dashing = value;
    }

    private bool dashCooldown;

    private Vector3 lastVelocity;

    public Vector3 LastVelocity
    {
        get => lastVelocity;
        set => lastVelocity = value;
    }

    protected virtual void Awake()
    {
        Cursor.visible = false;
        states = new List<State>();
        m_animator = GetComponent<Animator>();
        m_collider = GetComponent<Collider>();
        m_rigidbody = GetComponent<Rigidbody>();
    }

    protected void Update()
    {
        CheckGround();
        if (mode == CharacterControlMode.PlayerCharacter)
        {
            rotationTarget = cameraTransform.rotation;
        }
        m_animator.SetInteger(animationXParameter, Mathf.RoundToInt(movementVectorValue.x));
        m_animator.SetInteger(animationZParameter, Mathf.RoundToInt(movementVectorValue.y));
    }

    protected void FixedUpdate()
    {
        m_animator.SetFloat(animationYParameter, m_rigidbody.velocity.y);
    }

    private void OnMove(InputValue movementValue)
    {
        if (mode != CharacterControlMode.PlayerCharacter)
        {
            return;
        }
        movementVectorValue = movementValue.Get<Vector2>();
    }

    private void OnSprint(InputValue sprintValue)
    {
        if (mode != CharacterControlMode.PlayerCharacter)
        {
            return;
        }
        sprinting = sprintValue.isPressed;
    }

    private void OnJump(InputValue value)
    {
        if (!grounded || mode != CharacterControlMode.PlayerCharacter)
        {
            return;
        }
        jumping = value.isPressed;
    }

    private void OnCrouch(InputValue crouchValue)
    {
        if (mode != CharacterControlMode.PlayerCharacter)
        {
            return;
        }
        crouching = crouchValue.isPressed;
    }
    
    private void OnDash(InputValue dashValue)
    {
        if (mode != CharacterControlMode.PlayerCharacter)
        {
            return;
        }

        if (!dashCooldown)
        {
            StartCoroutine(DashCooldownRoutine());

            dashing = true;
        }
    }
    
    private IEnumerator DashCooldownRoutine()
    {
        dashCooldown = true;
        
        yield return new WaitForSeconds(3f);

        dashCooldown = false;
    }

    /**
     * Method to check if the player is grounded before the character jumps.
     */
    private void CheckGround()
    {
        Grounded = Physics.CheckCapsule(m_collider.bounds.center,
            new Vector3(m_collider.bounds.center.x, m_collider.bounds.min.y - groundedDistance,
                m_collider.bounds.center.z), groundedRadius, layerMask);
    }

    /**
     * Method to set the player mode to CharacterControlMode.PlayerCharacter.
     */
    public void SetModeToPlayerCharacter()
    {
        ResetInputValues();
        mode = CharacterControlMode.PlayerCharacter;
    }

    /**
     * Method to set the player mode to CharacterControlMode.NonPlayerCharacter.
     */
    public void SetModeToNonPlayerCharacter()
    {
        ResetInputValues();
        mode = CharacterControlMode.NonPlayerCharacter;
    }

    private void ResetInputValues()
    {
        movementVectorValue = Vector2.zero;
        sprinting = false;
        jumping = false;
        crouching = false;
        dashing = false;
    }
    
    /**
     * This enum is used to select the behaviour of the movement controller.
     */
    public enum CharacterControlMode
    {
        PlayerCharacter,
        NonPlayerCharacter
    }
}