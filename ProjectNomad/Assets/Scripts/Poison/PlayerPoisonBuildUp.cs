using UnityEngine;

public class PlayerPoisonBuildUp : MonoBehaviour
{
    [SerializeField] private CharacterStats characterStats;
    [SerializeField] private float poisonRemovalSpeed = 1f;
    [SerializeField] private float healthDrainInterval = 2f;
    
    private float elapsedTime = 0f;
    public CharacterStats CharacterStats => characterStats;
    [SerializeField] private bool insidePoisonGas = false;
    public bool InsidePoisonGas
    {
        get => insidePoisonGas;
        set => insidePoisonGas = value;
    }

    private void Update()
    {
        elapsedTime += Time.deltaTime;
        if (insidePoisonGas == false && characterStats.CurrentPoison != 0)
        {
            RemovingPoison();
        }
        if (characterStats.CurrentPoison >= characterStats.MaxPoison && elapsedTime >= healthDrainInterval)
        {
            characterStats.Health--;
            elapsedTime = 0;
        }
    }

    private void RemovingPoison()
    {
        if (elapsedTime > poisonRemovalSpeed)
        {
            characterStats.CurrentPoison--;
            elapsedTime = 0;
        }
    }
}
