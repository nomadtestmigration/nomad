using UnityEngine;
using UnityEngine.Rendering.HighDefinition;

public class PoisonMist : MonoBehaviour
{
    [SerializeField] private Transform endPositionPoison;
    [SerializeField] private LocalVolumetricFog volume;
    [SerializeField] private GameObject playerPosition;
    [SerializeField] private Transform poisonCloudDistanceCheck;
    private Vector3 gasStartPosition;

    [Tooltip("Speed at which the poison mist moves")]
    [SerializeField] private float poisonMistSpeed = 0.1f;
    [Tooltip("Rate at which the size of the poison mist grows")]
    [SerializeField] private float poisonMistGrowthInSize = 1f;
    [SerializeField] private float lerpDistance = 5f;
    private float startTime;
    private float journeyLengthFromStartToFinish;

    private void Start()
    {
        gasStartPosition = transform.position;
        journeyLengthFromStartToFinish = Vector3.Distance(gasStartPosition, endPositionPoison.position);
        startTime = Time.time;
    }

    private void Update()
    {
        if (Vector3.Distance(new Vector3(poisonCloudDistanceCheck.position.x, 0, poisonCloudDistanceCheck.position.z), new Vector3(endPositionPoison.position.x, 0, endPositionPoison.position.z)) >= 5)
        {
            CheckPoisonToPlayerDistance();
        }
    }

    private void CheckPoisonToPlayerDistance()
    {
        Vector3 positionPlayer = new Vector3(playerPosition.transform.position.x + lerpDistance, transform.position.y, transform.position.z);
        Vector3 movePosition = Vector3.Lerp(transform.position, positionPlayer, (poisonMistSpeed * Time.deltaTime));

        MovePoisonCloud(movePosition);
    }

    private void MovePoisonCloud(Vector3 pos)
    {
        transform.position = pos;
        volume.parameters.size.x += poisonMistGrowthInSize * Time.deltaTime;
        transform.localScale = new Vector3(volume.parameters.size.x, transform.localScale.y, transform.localScale.z);
    }
}
