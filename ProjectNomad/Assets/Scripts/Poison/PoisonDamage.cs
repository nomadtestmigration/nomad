using UnityEngine;

public class PoisonDamage : MonoBehaviour
{
    [Tooltip("Speed of the poison buildUp")]
    [SerializeField] private float poisonBuildUpSpeed = 1f;
    [Tooltip("Amount of poison filling the poison bar")]
    [SerializeField] private float poisonAmountIncrease = 5f;
    private float elapsedTime = 0f;
    
    private PlayerPoisonBuildUp playerPoisonBuildUp;

    private void Update()
    {
        elapsedTime += Time.deltaTime;
    }

    /// <summary>
    /// On entry trigger checking collider
    /// </summary>
    /// <param name="other">Collider object that has been hit</param>
    private void OnTriggerEnter(Collider other)
    {
        playerPoisonBuildUp = other.gameObject.GetComponent<PlayerPoisonBuildUp>();
        if(playerPoisonBuildUp != null) playerPoisonBuildUp.InsidePoisonGas = true;
    }

    /// <summary>
    /// On stay trigger checking collider
    /// </summary>
    /// <param name="other">Collider object that has been hit</param>
    private void OnTriggerStay(Collider other)
    {
        playerPoisonBuildUp = other.gameObject.GetComponent<PlayerPoisonBuildUp>();
        if (playerPoisonBuildUp == null) return;

        if (elapsedTime >= poisonBuildUpSpeed && playerPoisonBuildUp.CharacterStats.CurrentPoison <= playerPoisonBuildUp.CharacterStats.MaxPoison)
        {
            playerPoisonBuildUp.CharacterStats.CurrentPoison += poisonAmountIncrease;
            elapsedTime = 0;
        }
    }

    /// <summary>
    /// On exit trigger checking collider
    /// </summary>
    /// <param name="other">Collider object that has been hit</param>
    private void OnTriggerExit(Collider other)
    {
        playerPoisonBuildUp = other.gameObject.GetComponent<PlayerPoisonBuildUp>();
        if (playerPoisonBuildUp != null)
        {
            playerPoisonBuildUp.InsidePoisonGas = false;
        }
    }
}
