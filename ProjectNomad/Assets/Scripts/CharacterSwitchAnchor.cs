using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSwitchAnchor : MonoBehaviour
{
    private MovementController controller;

    private void Awake()
    {
        controller = GetComponentInParent<MovementController>();
    }

    /**
     * Sets the control mode of the parent controller to player
     */
    public void SetPlayerControlled()
    {
        controller!.SetModeToPlayerCharacter();
    }
    
    /**
     * Sets the control mode of the parent controller to non player
     */
    public void SetNonPlayerControlled()
    {
        controller!.SetModeToNonPlayerCharacter();
    }
}
