using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerRespawnHandler : MonoBehaviour
{
    [SerializeField] private CharacterStats eboStats;
    [SerializeField] private CharacterStats ccStats;

    private void Start()
    {
        eboStats.updateHealthDelegate += TryRespawnPlayer;
        ccStats.updateHealthDelegate += TryRespawnPlayer;

        eboStats.Health = eboStats.MaxHealth;
        eboStats.CurrentPoison = 0.0f;
        ccStats.Health = ccStats.MaxHealth;
    }

    private void TryRespawnPlayer()
    {
        if(eboStats.Health > 0 && ccStats.Health > 0)
        {
            return;
        }

        eboStats.updateHealthDelegate -= TryRespawnPlayer;
        ccStats.updateHealthDelegate -= TryRespawnPlayer;

        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }

    private void OnDestroy()
    {
        eboStats.updateHealthDelegate -= TryRespawnPlayer;
        ccStats.updateHealthDelegate -= TryRespawnPlayer;
    }
}
