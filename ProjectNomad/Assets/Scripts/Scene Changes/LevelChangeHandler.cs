using UnityEngine;
using UnityEngine.SceneManagement;
using AK.Wwise;

public class LevelChangeHandler : MonoBehaviour
{
    [SerializeField] private string sceneName;
    [SerializeField] private AK.Wwise.Event ambientMusic;

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<MovementController>() == null)
        {
            return;
        }

        LoadNextLevel();
    }

    private void LoadNextLevel()
    {
        AkSoundEngine.ClearBanks();
        SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
    }
}
