using UnityEngine;

public static class GizmosExtensions {
    /// <summary>
    /// Draw an arrow based on position and direction
    /// </summary>
    /// <param name="position">Origin position where the arrow base starts</param>
    /// <param name="direction">The direction an arrow will go as well as magnitude</param>
    /// <param name="color">Color of the ray</param>
    /// <param name="arrowHeadLength">Length of the arrowhead</param>
    /// <param name="arrowHeadAngle">Angle of the arrowhead</param>
    public static void DrawArrow(Vector3 position, Vector3 direction, Color color, float arrowHeadLength = 0.25f, float arrowHeadAngle = 20.0f) {
        Gizmos.color = color;
        Gizmos.DrawRay(position, direction);

        Vector3 right = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 + arrowHeadAngle, 0) * new Vector3(0, 0, 1);
        Vector3 left = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 - arrowHeadAngle, 0) * new Vector3(0, 0, 1);
        Gizmos.DrawRay(position + direction, right * arrowHeadLength);
        Gizmos.DrawRay(position + direction, left * arrowHeadLength);
    }
}