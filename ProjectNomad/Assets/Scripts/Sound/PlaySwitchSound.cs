using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlaySwitchSound : MonoBehaviour
{
  
    [Header("Wwise Events")]
    [Tooltip("This is the sound CC makes when switched")]
    [SerializeField]
    private AK.Wwise.Event CCSpeak;
    [Header("Timers")]
    [Tooltip("Timer make a character say a switch line")]
    [SerializeField]
    private float timer;
    [Tooltip("Timer reset")]
    [SerializeField]
    private float timerReset;

    private bool CC_Speak = false;
    [SerializeField] private Transform EBO;
    [SerializeField] private Transform CC;
    [SerializeField] private GameObject characterSwitch;

    /**
     * OnSwitch checkes who the character is en activates the speak bool of the other character
     */
    private void OnSwitchCharacter()
    {
        if (characterSwitch.GetComponent<CharacterSwitch>().CurrentCharacter == CharacterSwitch.ActiveCharacter.Ebo)
        {
            
        }
        else
        {
            CC_Speak = true;
        }
    }
    /**
     * checks in update if the character should make a switch sound and starts a timer and if the timer reaches below 0.0f the character that's switched to should make a sound
     */
    private void Update()
    {
        if (CC_Speak)
        {
            timer -= Time.deltaTime;
            if (timer <= 0.0f)
            {
                CCSpeak.Post(CC.gameObject);
                CC_Speak = false;
                timer = timerReset;
            }

        }
    }
}
