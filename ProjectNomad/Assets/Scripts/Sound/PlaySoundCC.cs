using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundCC : MonoBehaviour
{
    [Header("Wwise Events")]
    [Tooltip("This is the sound for CC's footstep.")]
    [SerializeField] private AK.Wwise.Event CCFootstep;
    [Tooltip("This is the sound for a character's landing (after jumping/falling).")]
    [SerializeField] private AK.Wwise.Event CCLanding;
    [SerializeField] private AK.Wwise.Event CCJumpBegin;

    public void PlayCCSteps()
    {
        CCFootstep.Post(gameObject);
    }
    public void CCJumpBeginPlay()
    {
        CCJumpBegin.Post(gameObject);
    }

    public void CCLandingPlay()
    {
        CCLanding.Post(gameObject);
    }
}
