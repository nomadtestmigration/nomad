using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundPlayer : MonoBehaviour
{
    [Header("Wwise Events")]
    [Tooltip("This is the sound for a character's footstep.")]
    [SerializeField] private AK.Wwise.Event myFootstep;

    [Tooltip("This is the sound for a character's landing (after jumping/falling).")]
    [SerializeField] private AK.Wwise.Event myLanding;
    [SerializeField] private AK.Wwise.Event myJumpBegin;

    private const float WALK_FOOTSTEP_TIMER = 0.3f;
    private const float SPRINT_FOOTSTEP_TIMER = 0.1f;
    private float lastWalkSoundTime;
    private float lastSprintSoundTime;

    private void WalkFootstepPlay()
    {
        float timeSinceLastFootstepSound = Time.time - lastWalkSoundTime;

        if (timeSinceLastFootstepSound >= WALK_FOOTSTEP_TIMER) 
        {
            myFootstep.Post(gameObject);
            lastWalkSoundTime = Time.time;
        }
    }

    private void SprintFootstepPlay()
    {
        float timeSinceLastFootstepSound = Time.time - lastSprintSoundTime;

        if (timeSinceLastFootstepSound >= SPRINT_FOOTSTEP_TIMER) 
        {
            myFootstep.Post(gameObject);
            lastSprintSoundTime = Time.time;
        }
    }

    public void JumpBeginPlay()
    {
        myJumpBegin.Post(gameObject);
    }

    public void LandingPlay()
    {
        myLanding.Post(gameObject);
    }
}