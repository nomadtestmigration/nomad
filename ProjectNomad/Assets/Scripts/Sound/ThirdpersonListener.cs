using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[ExecuteInEditMode]
public class ThirdpersonListener : AkGameObj
{
   
    [SerializeField] private Transform EBO;
    [SerializeField] private Transform CC;
    [SerializeField] private GameObject characterSwitch;
    
    /**
     * this overides wat the listener is listening at to the playable character and keeps the spanning of sounds on the camera
     */
    public override Vector3 GetPosition()
    {
        if (characterSwitch.GetComponent<CharacterSwitch>().CurrentCharacter == CharacterSwitch.ActiveCharacter.Ebo)
        {
            return EBO.GetComponent<AkGameObj>().GetPosition();
        }
        else
        {
            return CC.GetComponent<AkGameObj>().GetPosition();
        }
        
    }
}