using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum CanvasType
{
    MAINMENU,
    PAUSEMENU,
    GAMEHUD,
    CC_INVENTORY

}

public class CanvasManager : MonoBehaviour
{
    public CanvasType startScreen = CanvasType.MAINMENU;
    public List<CanvasController> canvasControllerList;
    private CanvasController lastActiveCanvas;
    
    private void Awake()
    {
        canvasControllerList.ForEach(x => x.gameObject.SetActive(false));
        SwitchCanvas(startScreen);
    }
    
    /**
     * SwitchCanvas swaps UI with the parameter you set. It remembers your activeCanvas
     * and turns that off and turns on the Canvas of the parameter.
     *
     * @param aType is the type of Canvas e.g. CanvasType.GAMEHUD
     */
    public void SwitchCanvas(CanvasType aType)
    {
        if (lastActiveCanvas != null)
        {
            lastActiveCanvas.gameObject.SetActive(false);
        }
        
        CanvasController desiredCanvas = canvasControllerList.Find(x => x.CanvasType == aType);
        if (desiredCanvas != null)
        {
            desiredCanvas.gameObject.SetActive(true);
            lastActiveCanvas = desiredCanvas;
        }
        else
        {
            Debug.LogWarning("The desired canvas was not found!");
        }
    }
}

