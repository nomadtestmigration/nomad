using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraManager : MonoBehaviour
{
    
    [SerializeField] private CinemachineFreeLook eboVFreeLookCam;
    [SerializeField] private CinemachineVirtualCameraBase eboPauseCam;
    [Space]
    [SerializeField] private CinemachineFreeLook ccVFreeLookCam;
    [SerializeField] private CinemachineVirtualCameraBase ccPauseCam;
    [Space]
    [SerializeField] private CinemachineBrain brain;

    public CinemachineFreeLook EboVFreeLookCam => eboVFreeLookCam;

    public CinemachineVirtualCameraBase EboPauseCam => eboPauseCam;

    public CinemachineFreeLook CcVFreeLookCam => ccVFreeLookCam;

    public CinemachineVirtualCameraBase CcPauseCam => ccPauseCam;

    public CinemachineBrain Brain => brain;

    /// <summary>
    /// Enables/Disables CameraMovement
    /// </summary>
    public void SetCameraMovementActive(bool active)
    {
        eboVFreeLookCam.GetComponent<CinemachineInputProvider>().enabled = active;
        ccVFreeLookCam.GetComponent<CinemachineInputProvider>().enabled = active;
    }
    
    /// <summary>
    /// Switches the current active camera to the freelook camera provided in the newCam field.
    /// </summary>
    /// <param name="newCam">This is the new Camera to switch to.</param>
    public void SwitchCameraPriorityToVirtualCamera(CinemachineVirtualCameraBase newCam)
    {
        (newCam.Priority, Brain.ActiveVirtualCamera.Priority) = (Brain.ActiveVirtualCamera.Priority, newCam.Priority);
    }
}
