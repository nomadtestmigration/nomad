using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class UISwitcher : MonoBehaviour
{
    [SerializeField] private bool isPaused = false;
    [SerializeField] private CanvasManager canvasManager;
    
    public GameObject pauseCamera;

    private void Awake()
    {
        canvasManager = GameObject.FindGameObjectWithTag("CanvasController").GetComponent<CanvasManager>();
    }

    private void OnIngameMenu(InputValue inputValue)
    {
        if (isPaused)
        {
            pauseCamera.SetActive(false);
            canvasManager.SwitchCanvas(CanvasType.GAMEHUD);
            Cursor.lockState = CursorLockMode.Locked;
            isPaused = false;
            ResumeGame();
        }
        else if (!isPaused)
        {
            pauseCamera.SetActive(true);
            canvasManager.SwitchCanvas(CanvasType.PAUSEMENU);
            Cursor.lockState = CursorLockMode.None;
            isPaused = true;
            PauseGame();
        }
    }

    private void PauseGame()
    {
        Time.timeScale = 0;
    }

    private void ResumeGame()
    {
        Time.timeScale = 1;
    }

}
