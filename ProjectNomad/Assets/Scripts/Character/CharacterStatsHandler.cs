using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CharacterStatsHandler : MonoBehaviour
{
    [SerializeField] private CharacterStats characterStats;
    public delegate void UpdateHealth();
    public UpdateHealth updateHealthDelegate;

    public string Name { get => characterStats.name; set => characterStats.name = value; }
    public float Health { get { return characterStats.Health; } set { characterStats.Health = value; updateHealthDelegate(); } }
    public int MaxHealth { get => characterStats.MaxHealth; set => characterStats.MaxHealth = value; }
}
