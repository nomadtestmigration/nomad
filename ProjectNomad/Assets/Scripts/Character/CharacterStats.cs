using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New CharacterStats", menuName = "CharacterStats")]
public class CharacterStats : ScriptableObject
{
    [Header("Name of the character")]
    [SerializeField] private string name;
    public string Name { get => name; set => name = value; }
    
    [Header("Current health of the character")]
    [SerializeField] private float health;
    public delegate void UpdateHealth();
    public UpdateHealth updateHealthDelegate;
    public float Health { get => health; set { health = Mathf.Clamp(value, 0, maxHealth); if (updateHealthDelegate != null) { updateHealthDelegate(); } } }

    [Header("Even numbers only (This is used to generate sprites as well)")]
    [SerializeField] private int maxHealth;
    public int MaxHealth { get => maxHealth; set => maxHealth = value; }
    
    [SerializeField] private float currentPoison;
    public delegate void UpdateCurrentPoison();
    public UpdateCurrentPoison updateCurrentPoisonDelegate;
    public float CurrentPoison
    {
        get => currentPoison;
        set { currentPoison = value; if (updateCurrentPoisonDelegate != null) { updateCurrentPoisonDelegate(); } }
    }
    
    [SerializeField] private float maxPoison;
    public float MaxPoison
    {
        get => maxPoison;
        set => maxPoison = value;
    }
}
