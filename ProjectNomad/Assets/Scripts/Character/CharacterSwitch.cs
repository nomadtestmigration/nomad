using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.InputSystem;

public class CharacterSwitch : MonoBehaviour
{

    [SerializeField] private MovementController playerCharacter;
    [SerializeField] private MovementController nonPLayerCharacter;
    [SerializeField] private CameraManager cameraManager;

    public enum ActiveCharacter { Ebo, CC }                                
    private ActiveCharacter currentCharacter = ActiveCharacter.Ebo;
    public ActiveCharacter CurrentCharacter => currentCharacter;

    private bool switching;

    private void OnSwitchCharacter(InputValue value)
    {
        if (!switching && value.isPressed)
        {
            StartCoroutine(switchCoroutine());
        }
    }

    private IEnumerator switchCoroutine()
    {
        switching = true;
        cameraManager.SetCameraMovementActive(false);                   
        playerCharacter.SetModeToNonPlayerCharacter();
        currentCharacter = currentCharacter == ActiveCharacter.Ebo ? ActiveCharacter.CC : ActiveCharacter.Ebo;
        cameraManager.SwitchCameraPriorityToVirtualCamera(currentCharacter == ActiveCharacter.Ebo ? cameraManager.EboVFreeLookCam : cameraManager.CcVFreeLookCam);
        yield return null;
        yield return new WaitUntil(() => !cameraManager.Brain.IsBlending);
        (playerCharacter, nonPLayerCharacter) = (nonPLayerCharacter, playerCharacter);
        playerCharacter.SetModeToPlayerCharacter();
        cameraManager.SetCameraMovementActive(true);                    
        switching = false;
    }

}
