using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


[RequireComponent(typeof(TMP_Text))]
public class ItemCountRendering : MonoBehaviour
{
    [SerializeField] private Inventory inventory;
    [SerializeField] private int itemSlotIndex;
    private string defaultValue;

    private void Start()
    {
        FetchDefaultText();
        LoadInfo();
    }

    private void Update()
    {
        LoadInfo();
    }


    private void LoadInfo()
    {
        if (inventory != null)
        {
            TMP_Text m_Text = GetComponent<TMP_Text>();
            if (m_Text != null)
            {
                if (inventory.itemSlots.Count > itemSlotIndex && itemSlotIndex >= 0)
                {
                    if (!inventory.itemSlots[itemSlotIndex].HasItem())
                    {
                        m_Text.text = defaultValue;
                    }
                    else
                    {
                        m_Text.text = inventory.itemSlots[itemSlotIndex].Amount.ToString();
                    }
                }
            }
        }
    }

    private void FetchDefaultText()
    {
        TMP_Text m_Text = GetComponent<TMP_Text>();
        if (m_Text != null)
        {
            defaultValue = m_Text.text;
        }
    }
}
