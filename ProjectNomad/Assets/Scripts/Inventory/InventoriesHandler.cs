using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class InventoriesHandler : MonoBehaviour
{
    [SerializeField] private Image selectedItem;
    
    private const int EMPTY_ITEM = -99;

    private int firstSelectedItemIndex = EMPTY_ITEM;
    private int secondSelectedItemIndex = EMPTY_ITEM;

    private Inventory firstSelectedInv;
    private Inventory secondSelectedInv;

    private Image tempImage;
    /// <summary>
    /// Method to select current clicked Item and assign it to a selectedItem Variable (e.g. first or second)
    /// </summary>
    /// <param name="inv">The inventory of the selected Item</param>
    /// <param name="index">index of the slot</param>
    public void SelectItem(Inventory inv, int index, Image image)
    {
        if (firstSelectedItemIndex == EMPTY_ITEM)
        {
            if (inv.itemSlots[index].Item == null) return;
            firstSelectedItemIndex = index;
            firstSelectedInv = inv;
            
            selectedItem!.gameObject.SetActive(true);
            selectedItem.sprite = inv.itemSlots[index].Item.Sprite;
            tempImage = image;
            tempImage.color = Color.gray;
        }        
        else
        {                
            secondSelectedItemIndex = index;
            secondSelectedInv = inv;
            SwapItems();
        }
    }

    private void SwapItems()
    {
        Inventory.SwapItemSlotsBetweenInventories(firstSelectedInv, firstSelectedInv.itemSlots[firstSelectedItemIndex], secondSelectedInv,secondSelectedInv.itemSlots[secondSelectedItemIndex]);

        firstSelectedItemIndex = EMPTY_ITEM;
        secondSelectedItemIndex = EMPTY_ITEM;
        tempImage.color = Color.white;
        selectedItem!.gameObject.SetActive(false);
    }
    
    private void Update()
    {
        if (firstSelectedItemIndex != EMPTY_ITEM)
        {
            selectedItem.transform.position = Mouse.current.position.ReadValue();
            selectedItem.raycastTarget = false;
        }
    }
}
