using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class ItemSlotRendering : MonoBehaviour, IPointerDownHandler
{
    [SerializeField] private Inventory inventory;
    [SerializeField] private int itemSlotIndex;
    private Sprite defaultValue;
    private InventoriesHandler invHandler;
    private void Awake()
    {
        invHandler = GetComponentInParent<InventoriesHandler>();
    }

    private void Start()
    {
        FetchDefaultSprite();
        LoadInfo();
    }

    private void Update()
    {
        LoadInfo();
    }

    private void LoadInfo()
    {
        if (inventory != null)
        {
            Image m_Image = GetComponent<Image>();
            if (m_Image != null)
            {
                if (inventory.itemSlots.Count > itemSlotIndex && itemSlotIndex >= 0)
                {
                    if (!inventory.itemSlots[itemSlotIndex].HasItem())
                    {
                        m_Image.sprite = defaultValue;
                    }
                    else
                    {
                        m_Image.sprite = inventory.itemSlots[itemSlotIndex].Item.Sprite;
                    }
                }
            }
        }
    }

    private void FetchDefaultSprite()
    {
        Image m_Image = GetComponent<Image>();
        if (m_Image != null)
        {
            defaultValue = m_Image.sprite;
        }
    }

    /// <summary>
    /// Handles when you left-click an item
    /// </summary>
    /// <param name="eventData">eventData of left clicking the gameObject</param>
    public void OnPointerDown(PointerEventData eventData)
    {
        invHandler.SelectItem(inventory, itemSlotIndex, gameObject.GetComponent<Image>());
    }
}
