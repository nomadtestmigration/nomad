using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using UnityEngine;

[CreateAssetMenu(fileName = "New Inventory", menuName = "Inventory")]
public class Inventory : ScriptableObject
{

    [SerializeField] private bool isStackable;
    public bool IsStackable { get { return isStackable; } set { isStackable = value; } }
    [SerializeField] private List<InventorySlot> itemSlotList;
    public ReadOnlyCollection<InventorySlot> itemSlots => itemSlotList.AsReadOnly();

    /// <summary>
    /// This function can be used to pickup an item.
    /// </summary>
    /// <param name="item">The item that gets picked up.</param>
    /// <param name="amount">The amount of items.</param>
    public void PickUp(BaseItem item, int amount)
    {
        if (item.IsStackable && isStackable)
        {
            if (StackItem(item, amount))
            {
                return;
            };
        }
        AddItem(item, amount);
    }

    private bool AddItem(BaseItem item, int amount)
    {
        foreach (InventorySlot slot in itemSlotList)
        {
            if (!slot.HasItem())
            {
                slot.SetItem(item, amount);
                return true;
            }
        }
        return false;
    }

    private bool StackItem(BaseItem item, int amount)
    {
        foreach (InventorySlot slot in itemSlotList)
        {
            if (slot.HasItem())
            {
                if (slot.Item.ItemName.Equals(item.ItemName) && slot.Amount < item.MaxStackAmount)
                {
                    slot.AddAmountToSlot(amount);
                    return true;
                }
            }
        }
        return false;
    }

    /// <summary>
    /// Check if the inventory has free slots.
    /// </summary>
    /// <returns>True if ther is a free slot. False if ther is no free slot.</returns>
    public bool HasFreeSlot()
    {
        foreach (InventorySlot itemSlot in itemSlotList)
        {
            if (!itemSlot.HasItem())
            {
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// Swap the contents of itemslots.
    /// </summary>
    /// <param name="itemSlot1">slot 1</param>
    /// <param name="itemSlot2">slot 2</param>
    public void SwapItemSlots(InventorySlot itemSlot1, InventorySlot itemSlot2)
    {
        int index1 = itemSlotList.IndexOf(itemSlot1);
        int index2 = itemSlotList.IndexOf(itemSlot2);

        itemSlotList[index1] = itemSlot2;
        itemSlotList[index2] = itemSlot1;
    }

    /// <summary>
    /// Swap the contents of itemslots.
    /// </summary>
    /// <param name="inv1">Inventory 1 link to a inventory ScriptableObject</param>
    /// <param name="inv2">Inventory 2 link to a inventory ScriptableObject</param>
    /// <param name="itemSlot1">slot 1</param>
    /// <param name="itemSlot2">slot 2</param>
    public static void SwapItemSlotsBetweenInventories(Inventory inv1, InventorySlot itemSlot1, Inventory inv2, InventorySlot itemSlot2)
    {
        InventorySlot tempSlot1 = new InventorySlot();
        tempSlot1.SetItem(itemSlot1.Item, itemSlot1.Amount);
        InventorySlot tempSlot2 = new InventorySlot();
        tempSlot2.SetItem(itemSlot2.Item, itemSlot2.Amount);
        
        itemSlot1.RemoveItem();
        itemSlot2.RemoveItem();
        
        itemSlot2.SetItem(tempSlot1.Item, tempSlot1.Amount);
        itemSlot1.SetItem(tempSlot2.Item, tempSlot2.Amount);

    }
    
    /// <summary>
    /// Remove all items from the inventory.
    /// </summary>
    public void RemoveAllItems()
    {
        foreach (InventorySlot item in itemSlotList)
        {
            item.RemoveItem();
        }
    }

    /// <summary>
    /// Check if an item is in the inventory.
    /// </summary>
    /// <param name="item"> The item that will be checked if it is in the inventory.</param>
    /// <returns></returns>
    public bool ContainsItem(BaseItem item)
    {
        foreach (InventorySlot itemSlot in itemSlotList)
        {
            if (itemSlot.Item.ItemName == item.ItemName)
            {
                return true;
            }
        }
        return false;
    }
    
    /// <summary>
    /// Remove a single item in the inventory
    /// </summary>
    /// <param name="item">Item that will be deleted from the inventory if it exists</param>
    public void RemoveItem(BaseItem item)
    {
        foreach (InventorySlot slot in itemSlotList.Where(slot => slot.Item == item))
        {
            slot.RemoveItem();
            return;
        }
    }
    
    /// <summary>
    /// Get a single inventory slot by a BaseItem
    /// </summary>
    /// <param name="item">Item that will be used to filter</param>
    /// <returns>Null if the item doesn't exist. The InventorySlot with the BaseItem</returns>
    public InventorySlot GetInventorySlot(BaseItem item)
    {
        return itemSlotList.FirstOrDefault(slot => slot.Item == item);
    }
}
