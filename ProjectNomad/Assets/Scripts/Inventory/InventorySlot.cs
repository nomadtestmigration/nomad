using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class InventorySlot {
    [SerializeField] private BaseItem item;
    public BaseItem Item { get { return item;} }
    [SerializeField] private int amount;
    public int Amount { get { return amount; } }

    /// <summary>
    /// Contstroctur
    /// </summary>
    /// <param name="item">the item in the itemslot default null</param>
    /// <param name="amount">the ammount of items default 0</param>
    public InventorySlot(BaseItem item = null, int amount = 0) {
        this.item = item;
        this.amount = amount;
    }
    
    /// <summary>
    /// Check if the slot has an item in it.
    /// </summary>
    /// <returns>true if it has an item. false if it has no item.</returns>
    public bool HasItem() {
        return (item != null && amount > 0);
    }

    /// <summary>
    /// Remove all itmes in the slot.
    /// </summary>
    public void RemoveItem() {
        item = null;
        amount = 0;
    }

    /// <summary>
    /// Set the item in the slot.
    /// </summary>
    /// <param name="aItem">The item.</param>
    /// <param name="aAmount">The ammount.</param>
    public void SetItem(BaseItem aItem, int aAmount) {
        item = aItem;
        amount = aAmount;
    }

    /// <summary>
    /// Add an ammount to the item count.
    /// </summary>
    /// <param name="amount">how manny items to add</param>
    public void AddAmountToSlot(int amount) {
        this.amount = Mathf.Clamp(this.amount + amount, 0, item.MaxStackAmount);
    }

    /// <summary>
    /// Remove an ammount to the item count.
    /// </summary>
    /// <param name="amount">how manny items to add</param>
    public void RemoveAmountToSlot(int amount)
    {
        this.amount = Mathf.Clamp(this.amount - amount, 0, item.MaxStackAmount);
    }

    public InventorySlot SwapItem(BaseItem item, int amount) {
        InventorySlot current = this;
        SetItem(item, amount);
        return current;
    }
}