/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID PLAY_AIRLOCKDOOR_CLOSE1 = 3974977655U;
        static const AkUniqueID PLAY_BUNKER_3_1 = 3431095223U;
        static const AkUniqueID PLAY_CC_JUMP = 3625097539U;
        static const AkUniqueID PLAY_ENEMYFLESHHIT = 1674911419U;
        static const AkUniqueID PLAY_FLORESENT_LIGHT = 1516254067U;
        static const AkUniqueID PLAY_FOOTSTEPS = 3854155799U;
        static const AkUniqueID PLAY_JUMPBEGIN = 2296124049U;
        static const AkUniqueID PLAY_LANDING = 2323405115U;
        static const AkUniqueID PLAY_LIFT = 3612898093U;
        static const AkUniqueID PLAY_LIGHT_HUM = 3875771565U;
        static const AkUniqueID PLAY_LONELY = 2388497063U;
        static const AkUniqueID PLAY_MENU_CLICK = 1357778998U;
        static const AkUniqueID PLAY_MENU_CLICK_01 = 1115286468U;
        static const AkUniqueID PLAY_NEW = 2557017376U;
        static const AkUniqueID PLAY_NEW_FOOTSTEPS = 21198552U;
        static const AkUniqueID PLAY_NOISE = 1932796752U;
        static const AkUniqueID PLAY_PRESURE_PLATE_COMPRESS = 261900158U;
        static const AkUniqueID PLAY_PRESURE_PLATE_DECOMPRESS = 3330584343U;
        static const AkUniqueID PLAY_ROBOTBOMB_EXPLOSION_V1 = 844699242U;
        static const AkUniqueID PLAY_SOLVING_A_PUZZLE = 3847695549U;
        static const AkUniqueID PLAY_SOLVING_A_PUZZLE__SIEF_ARRANGEMENT__1 = 2039851942U;
        static const AkUniqueID PLAY_SWITCHSOUNDCC = 2481039929U;
        static const AkUniqueID PLAY_TEMPLEBUNKER_2 = 2662844813U;
        static const AkUniqueID PLAY_TENSION = 4114669316U;
        static const AkUniqueID PLAY_TORCH = 2025845440U;
        static const AkUniqueID PLAY_WALKING_CC = 2921490336U;
        static const AkUniqueID PLAY_WEAPON_SWING_STAF = 3766847488U;
        static const AkUniqueID PLAY_WIND = 1020223172U;
        static const AkUniqueID PLAYWATERNOISES = 3419015273U;
        static const AkUniqueID STOP_NEW = 837991554U;
        static const AkUniqueID STOP_NOISE = 3021116958U;
        static const AkUniqueID STOP_TEMPLEBUNKER_2 = 3359251851U;
        static const AkUniqueID STOP_TENSION = 149375838U;
        static const AkUniqueID STOPWATERNOISES = 846923123U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace POISONED
        {
            static const AkUniqueID GROUP = 1467164614U;

            namespace STATE
            {
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID OFF = 930712164U;
                static const AkUniqueID ON = 1651971902U;
            } // namespace STATE
        } // namespace POISONED

    } // namespace STATES

    namespace SWITCHES
    {
        namespace FOOTSTEPS
        {
            static const AkUniqueID GROUP = 2385628198U;

            namespace SWITCH
            {
                static const AkUniqueID DIRT = 2195636714U;
                static const AkUniqueID GRASS = 4248645337U;
                static const AkUniqueID SAND = 803837735U;
                static const AkUniqueID STONE = 1216965916U;
                static const AkUniqueID WATER = 2654748154U;
                static const AkUniqueID WOOD = 2058049674U;
            } // namespace SWITCH
        } // namespace FOOTSTEPS

    } // namespace SWITCHES

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MAIN = 3161908922U;
        static const AkUniqueID SECOND_MAIN_GIT = 890296786U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID AMBIENT = 77978275U;
        static const AkUniqueID FOOTSTEPS = 2385628198U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID REVERB = 348963605U;
    } // namespace BUSSES

    namespace AUX_BUSSES
    {
        static const AkUniqueID BIGCAVE = 2737839590U;
        static const AkUniqueID HUGECAVE = 1939446655U;
        static const AkUniqueID INBTWEEN = 3070299775U;
        static const AkUniqueID OUTSIDE = 438105790U;
        static const AkUniqueID SMALLCAVE = 1382725209U;
        static const AkUniqueID TINYCAVE = 588112636U;
    } // namespace AUX_BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
